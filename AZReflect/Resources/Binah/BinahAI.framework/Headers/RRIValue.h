//
//  RRIValue.h
//  BinahSDK
//
//  Created by Erez Bursztyn on 09/06/2021.
//  Copyright © 2021 binah.ai. All rights reserved.
//

#ifndef RRIValue_h
#define RRIValue_h

#import <Foundation/Foundation.h>

@interface RRIValue : NSObject

@property (nonatomic, readonly) double timestamp;
@property (nonatomic, readonly) double interval;

- (instancetype)initWithTimestamp:(double)timestamp
                      andInterval:(double)interval;

@end


#endif /* RRIValue_h */
