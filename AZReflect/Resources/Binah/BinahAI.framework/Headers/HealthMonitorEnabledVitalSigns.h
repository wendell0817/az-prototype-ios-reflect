#ifndef HealthMonitorEnabledVitalSigns_h
#define HealthMonitorEnabledVitalSigns_h

#import <Foundation/Foundation.h>

@interface HealthMonitorEnabledVitalSigns : NSObject

@property (nonatomic, readonly) bool heartRateEnabled;
@property (nonatomic, readonly) bool breathingRateEnabled;
@property (nonatomic, readonly) bool oxygenSaturationEnabled;
@property (nonatomic, readonly) bool sdnnEnabled;
@property (nonatomic, readonly) bool stressLevelEnabled;
@property (nonatomic, readonly) bool rrIntervalEnabled;
@property (nonatomic, readonly) bool stressIndexEnabled;

- (instancetype)initWithEnabledVitalSigns:(bool)isHeartRateEnabled
                   andBreatingRateEnabled:(bool)isBreathingRateEnabled
               andOxygenSaturationEnabled:(bool)isOxygenSaturationEnabled
                           andSdnnEnabled:(bool)isSdnnEnabled
                    andStressLevelEnabled:(bool)isStressLevelEnabled
                     andRRIntervalEnabled:(bool)isRRIntervalEnabled
                   andStressIndexEnabled:(bool)isStressIndexEnabled;

@end

#endif /* HealthMonitorEnabledVitalSigns_h */
