//
//  HealthMonitorReport.h
//  health_monitor_sdk
//
//  Created by Tal Lerman on 26/04/2020.
//  Copyright © 2020 binah.ai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RRIValue.h"


#pragma mark -

typedef enum {
    kStressLevelType_Unknown = 0,
    kStressLevelType_Low = 1,
    kStressLevelType_Normal = 2,
    kStressLevelType_Mild = 3,
    kStressLevelType_High = 4,
    kStressLevelType_Extreme = 5
} StressLevelType;



@interface HealthMonitorReport : NSObject

@property (nonatomic, strong) NSNumber *_Nullable HeartRate;               // HR
@property (nonatomic, strong) NSNumber *_Nullable RespirationRate;         // RR
@property (nonatomic, strong) NSNumber *_Nullable OxygenSaturation;        // SpO2
@property (nonatomic, strong) NSNumber *_Nullable StandardDeviationNN;     // SDNN
@property (nonatomic) StressLevelType StressLevel;                         // SL
@property (nonatomic, strong) NSArray<RRIValue*> *_Nullable RRInterval;    // RRi
@property (nonatomic, strong) NSNumber *_Nullable StressIndex;             // SI

- (BOOL)isEmpty;

@end

