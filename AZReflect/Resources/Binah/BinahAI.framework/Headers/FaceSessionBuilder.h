//
//  FaceSessionBuilder.h
//  BinahAI
//
//  Created by Tal Lerman on 06/10/2020.
//  Copyright © 2020 binah.ai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HealthMonitorSession.h"
#import "HealthMonitorFaceSessionListener.h"
#import "HealthMonitorSessionStateListener.h"


NS_ASSUME_NONNULL_BEGIN


@protocol FaceSessionBuilder <NSObject>

/*!
 Use to create and activate HealthMonitorSession object.
 @param error health monitor error
 */
- (id<HealthMonitorSession> _Nullable)buildWithError:(NSError *__autoreleasing  _Nullable * _Nullable)error;



/*!
 Set listener
 @param listener Default value is @b nil
 */
- (instancetype)listener:(id<HealthMonitorFaceSessionListener>)listener;

/*!
 Set stateListener
 @param stateListener Default value is @b nil
 */
- (instancetype)stateListener:(id<HealthMonitorSessionStateListener>)stateListener;

/*!
 Set detectionAlwaysOn
 @param detectionAlwaysOn Default value is @b false
 */
- (instancetype)detectionAlwaysOn:(BOOL)detectionAlwaysOn NS_SWIFT_NAME(detectionAlwaysOn(_:));


@end


NS_ASSUME_NONNULL_END

