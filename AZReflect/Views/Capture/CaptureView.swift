//
//  CaptureView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/9/21.
//

import SwiftUI

// MARK: RecordingView

struct CaptureView: View {
    @EnvironmentObject private var reflectionsStore: ReflectionsStore
    @StateObject private var store: CaptureStore
    
    var body: some View {
        views(for: store.stage)
            .animation(.easeInOut, value: store.stage)
            .background(CaptureView.BackgroundView().ignoresSafeArea())
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .onAppear {
                store.setFlow(.metricsVideo)
                reflectionsStore.subscribeToCaptureState(store.statePublisher)
            }
    }
    
    init(store: CaptureStore = .init()) {
        self._store = .init(wrappedValue: store)
    }
    
    private func views(for stage: CaptureState.CaptureStage) -> some View {
        return ZStack {
            visibleView(for: [.ready]) {
                CaptureView.ReadyView(startTapped: store.nextStage)
            }
            
            visibleView(for: [.metricsCountdown, .videoCountdown]) {
                CaptureView.CountdownView(
                    text: store.stage.countdownText,
                    textEmphasized: store.stage.countdownTextEmphasized,
                    countdown: Int(store.state.countdown)
                )
            }
            
            visibleView(for: [.metricsCapture]) {
                CaptureView.MetricsCaptureView(state: store.state)
            }
            
            visibleView(for: [.videoCapture]) {
                CaptureView.VideoCaptureView(
                    session: store.videoSession,
                    state: store.state,
                    finishedTapped: store.nextStage
                )
            }
            
            visibleView(for: [.metricsCapture, .videoCapture]) {
                CaptureView.ProgressView(
                    state: store.state,
                    flow: .metricsVideo
                )
            }
            
            visibleView(for: [.completed]) {
                CaptureView.CompletedView(
                    reflectStore: reflectionsStore,
                    captureID: store.captureId
                )
            }
        }
    }
    
    private func visibleView<Content: View>(
        for stages: [CaptureState.CaptureStage],
        _ content: () -> Content
    ) -> some View {
        content()
            .opacity(stages.contains(store.stage) ? 1.0 : 0.0)
    }
}

// MARK: BackroundView

extension CaptureView {
    struct BackgroundView: View {
        var body: some View {
            LinearGradient(
                gradient: .init(
                    colors: [
                        Theme.color(.accentLight),
                        Theme.color(.accentDark),
                    ]
                ),
                startPoint: .topLeading,
                endPoint: .bottomTrailing
            )
            .overlay(
                Image("onboarding.intro.background")
                    .resizable()
                    .scaledToFill()
                    .blendMode(.multiply)
            )
            .clipped()
        }
    }
}

// MARK: ReadyView

extension CaptureView {
    struct ReadyView: View {
        let startTapped: () -> Void
        
        var body: some View {
            VStack(spacing: 0.0) {
                BackNavigationView()
                    .padding(.top, 20.0)
                
                VStack(spacing: 0.0) {
                    Text("Let’s get rolling!")
                        .themeFont(.titleBold)
                    Text("We’ll be using your front camera to record health metrics and a video message.")
                        .themeFont(.action)
                        .padding(.top, 16.0)
                    Text("Be aware of your surroundings!")
                        .themeFont(.subtitle)
                        .padding(.top, 40.0)
                }
                .multilineTextAlignment(.center)
                .padding(.top, 40.0)
                
                Spacer()
                
                Button("Start Reflection", action: startTapped)
                    .themeButtonStyle(.captureStandardGradient)
            }
            .foregroundColor(.white)
            .padding(.horizontal, 20.0)
            .padding(.bottom, 48.0)
        }
    }
}

// MARK: CountdownView

extension CaptureView {
    struct CountdownView: View {
        let text: String
        let textEmphasized: String
        let countdown: Int
        
        var body: some View {
            VStack(spacing: 0.0) {
                GeometryReader { proxy in
                    VStack {
                        Text(text)
                            .themeFont(.captureCountdownBody)
                        +
                        Text(textEmphasized)
                            .themeFont(.captureCountdownBodyBold)
                    }
                    .animation(.none, value: text)
                    .padding(.bottom, 40.0)
                    .frame(size: proxy.size, alignment: .bottom)
                }
                
                Text("\(countdown)")
                    .themeFont(.countdown)
                    .animation(.none)
                
                Spacer().layoutPriority(0.0)
                
            }
            .multilineTextAlignment(.center)
            .foregroundColor(.white)
            .padding(.horizontal, 36.0)
        }
    }
}

// MARK: ProgressView

extension CaptureView {
    struct ProgressView: View {
        let state: CaptureState
        let flow: Settings.CaptureFlow
        private let iconWidth: CGFloat = 20.0
        
        var body: some View {
            VStack {
                progressView()
                Spacer()
            }
            .padding(.horizontal, 24.0)
        }
        
        @ViewBuilder private func progressView() -> some View {
            switch state.stage {
            case .metricsCapture:
                CaptureProgressView(
                    style: .scan,
                    elapsedTime: state.metricCaptureTime,
                    progress: state.metricCapturePercent
                )
                .animation(
                    .linear(duration: 1.0),
                    value: state.metricCapturePercent
                )
            case .videoCapture:
                CaptureProgressView(
                    style: .record,
                    elapsedTime: state.videoCaptureTime,
                    progress: state.videoCapturePercent
                )
                .animation(
                    .linear(duration: 1.0),
                    value: state.videoCapturePercent
                )
            default:
                EmptyView()
            }
        }
    }
}

// MARK: MetricsCatpureView

extension CaptureView {
    struct MetricsCaptureView: View {
        let state: CaptureState
        
        var body: some View {
            ZStack {
                GeometryReader { proxy in
                    if let metricsImage = state.metricsImage {
                        ImageView(uiImage: metricsImage.image)
                            .scaledToFill()
                            .frame(size: proxy.size)
                            .clipped()
                        
                        if let image = metricsImage.image {
                            faceScannerView(
                                for: image.size,
                                faceRect: metricsImage.faceRect,
                                maxSize: proxy.size
                            )
                            .accentColor(Theme.color(.warning))
                            .animation(.linear, value: metricsImage.faceRect)
                        }
                    }
                }
                .ignoresSafeArea()
                
                VStack {
                    Spacer()
                    
                    if state.stage == .metricsCapture {
                        CaptureMetricsView(metrics: state.metricsDisplay)
                    }
                }
                .padding(.bottom, 40.0)
                .padding(.horizontal, 20.0)
            }
        }
        
        func faceScannerView(for imageSize: CGSize, faceRect: CGRect, maxSize: CGSize) -> some View {
            let scaledFaceRect = scaledFaceRect(for: imageSize, faceRect: faceRect, in: maxSize)
            
            return FaceScannerView()
                .offset(x: scaledFaceRect.minX, y: scaledFaceRect.minY)
                .frame(width: scaledFaceRect.width, height: scaledFaceRect.height)
        }
        
        func scaledFaceRect(
            for imageSize: CGSize,
            faceRect: CGRect,
            in maxSize: CGSize
        ) -> CGRect {
            let ratio = SizeRatio(size: imageSize)
            let newImageSize = CGSize(
                width: ratio.width(for: maxSize.height),
                height: maxSize.height
            )
            
            let scale = maxSize.height / imageSize.height
            
            let scaledSize = CGSize(
                width: faceRect.width * scale,
                height: faceRect.height * scale
            )
            
            let scaledOrigin = CGPoint(
                x: (faceRect.origin.x * scale) - ((newImageSize.width - maxSize.width) * 0.5),
                y: (faceRect.origin.y * scale) - ((newImageSize.height - maxSize.height) * 0.5)
            )
            
            return CGRect(
                origin: scaledOrigin,
                size: scaledSize
            )
        }
        
        func metricView(
            for symbol: String,
            metricKeyPath: KeyPath<CaptureState.MetricsDisplay, Double?>,
            unitText: String
        ) -> some View {
            let value = state.metricsDisplay[keyPath: metricKeyPath]
            let accentColor = value == .none ? Theme.color(.captureText) : Theme.color(.accept)
            
            return MetricView(
                symbol: symbol,
                value: value,
                unitText: unitText,
                foregroundColor: Theme.color(.captureText)
            )
            .accentColor(accentColor)
        }
    }
}

// MARK: VideoCaptureView

extension CaptureView {
    struct VideoCaptureView: View {
        struct Instruction: Identifiable {
            let id: Int
            let title: String
            let bullets: [String]
        }
        
        let session: AVCaptureSession
        let state: CaptureState
        let finishedTapped: () -> Void
        
        @State private var isCardExpanded = true
        @State private var selectedInstuctionId = 0
        
        private let instructionCards: [Instruction] = [
            .init(
                id: 0,
                title: "How are you feeling physically?",
                bullets: [
                    "Include even mild symptoms",
                    "Remember things like sleep and appetite"
                ]
            ),
            .init(
                id: 1,
                title: "How are you emotionally?",
                bullets: [
                    "Include good or bad feelings",
                    "Remember things like work and family"
                ]
            ),
            .init(
                id: 2,
                title: "Include a personal message",
                bullets: ["Anything else you’d like to say!"]
            ),
        ]
        
        var body: some View {
            ZStack {
                videoView()
                cardView()
            }
            .foregroundColor(Theme.color(.captureText))
        }
        
        private func videoView() -> some View {
            GeometryReader { proxy in
                VideoPreviewView(session: session)
                    .frame(size: proxy.size)
            }
            .ignoresSafeArea()
        }
        
        private func cardView() -> some View {
            ExpandingCardView(
                heightRange: 100.0 ... 280.0,
                content: {
                    VStack(spacing: 0.0) {
                        HStack {
                            selectorButton(for: -1, symbol: "arrow.left")
                            
                            Spacer()
                            
                            ForEach(instructionCards) { instruction in
                                Circle()
                                    .stroke(Theme.color(.backgroundRecording), lineWidth: 2.0)
                                    .overlay(
                                        Circle()
                                            .fill(Theme.color(.backgroundRecording))
                                            .opacity(instruction.id == selectedInstuctionId ? 1.0 : 0.0)
                                    )
                                    .frame(width: 8.0, height: 8.0)
                            }
                            
                            Spacer()
                            
                            selectorButton(for: 1, symbol: "arrow.right")
                        }
                        .padding(.horizontal, 20.0)
                        .frame(height: 40.0, alignment: .bottom)
                        
                        TabView(selection: $selectedInstuctionId) {
                            ForEach(instructionCards) { instruction in
                                VStack(spacing: 4.0) {
                                    Text(instruction.title)
                                        .themeFont(.captureCountdownBodyBold)
                                    ForEach(instruction.bullets, id: \.self) { bullet in
                                        HStack {
                                            Image(systemName: "checkmark.circle.fill")
                                                .themeModifier(.reflectGradient)
                                            Text(bullet)
                                        }
                                        .themeFont(.subtitle)
                                    }
                                }
                                .multilineTextAlignment(.center)
                                .padding(.horizontal, 20.0)
                                .tag(instruction.id)
                            }
                        }
                        .frame(height: 140.0)
                        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
                        
                        Button("Finish Reflection", action: finishedTapped)
                            .themeButtonStyle(.captureStandardDestructive)
                            .frame(height: 100.0)
                            .padding(.horizontal, 20.0)
                    }
                },
                background: {
                    CardShape(cornerRadius: 28.0)
                        .fill(Theme.color(.captureBackgroundCard))
                        .edgesIgnoringSafeArea(.bottom)
                },
                accessory: {
                    symptomsView()
                }
            )
        }
        
        private func symptomsView() -> some View {
            ScrollView(.horizontal) {
                ScrollViewReader { scrollProxy in
                    HStack(spacing: 0.0) {
                        ForEach(state.symptoms) { symptom in
                            Text(symptom.title)
                                .themeFont(.action)
                                .padding(.horizontal, 16.0)
                                .frame(height: 32.0)
                                .foregroundColor(.white)
                                .themeModifier(.symptomPill)
                                .padding(.horizontal, 4.0)
                                .id(symptom)
                            
                        }
                    }
                    .onChange(of: state.symptoms) { symptoms in
                        switch symptoms.last {
                        case .some(let lastSymptom):
                            withAnimation(.easeInOut) {
                                scrollProxy.scrollTo(lastSymptom, anchor: .trailing)
                            }
                        default:
                            break
                        }
                    }
                    .padding(.horizontal, 16.0)
                    .frame(height: 32.0)
                    
                }
            }
            .animation(.easeInOut, value: state.symptoms)
        }
        
        func selectorButton(for increment: Int, symbol: String) -> some View {
            Button(
                action: { advanceInstructions(by: increment) },
                label: { Image(systemName: symbol) }
            )
            .themeFont(.captureCountdownBodyBold)
            .buttonStyle(PlainButtonStyle())
        }
        
        func advanceInstructions(by increment: Int) {
            guard let currentIndex = instructionCards
                    .firstIndex(where: { $0.id == selectedInstuctionId })
            else { return }
            
            let targetIndex = currentIndex + increment
            
            guard instructionCards.indices
                    .contains(targetIndex)
            else { return }
            
            withAnimation {
                selectedInstuctionId = instructionCards[targetIndex].id
            }
        }
    }
}

// MARK: CompletedView

extension CaptureView {
    struct CompletedView: View {
        @ObservedObject var reflectStore: ReflectionsStore
        let captureID: String
        let videoSizeRatio = SizeRatio(width: 16.0, height: 9.0)
        
        var body: some View {
            GeometryReader { proxy in
                VStack(spacing: 0.0) {
                    if let reflection = reflectStore.reflection(for: captureID) {
                        titleView()
                            .padding(.top, 100.0)
                        
                        videoCardView(for: reflection, width: proxy.size.width)
                            .padding(.top, 52.0)
                        
                        metricsView(for: reflection)
                            .padding(20.0)
                        
                        Spacer()
                        
                        footerView()
                            .padding(.bottom, 40.0)
                    } else {
                        EmptyView()
                    }
                }
            }
            .padding(.horizontal, 20.0)
            .background(Theme.color(.backgroundInstruction).ignoresSafeArea())
        }
        
        private func titleView() -> some View {
            VStack(spacing: 8.0) {
                Text("Looking good!")
                    .themeFont(.titleBold)
                Text("We collected and averaged your health markers for your recording. ")
                    .themeFont(.subtitle)
            }
            .multilineTextAlignment(.center)
        }
        
        private func videoCardView(for reflection: Reflection, width: CGFloat) -> some View {
            VStack(alignment: .leading, spacing: 0.0) {
                HStack {
                    Button(
                        action: {},
                        label: { Image(systemName: "play") }
                    )
                    .themeButtonStyle(.capturePlay)
                    
                    Spacer()
                }
                
                Spacer()
                
                VStack(alignment: .leading) {
                    Text("New Reflection")
                        .themeFont(.bodyBold)
                    Text(reflection.symptoms.map(\.title).joined(separator: ", "))
                        .themeFont(.subtitle)
                }
            }
            .foregroundColor(.white)
            .padding(16.0)
            .frame(
                width: width,
                height: videoSizeRatio.height(for: width)
            )
            .background(
                VideoThumbnailView(url: reflectStore.videoURL(for: reflection))
                    .overlay(Color.black.opacity(0.5))
                    .mask(RoundedRectangle(cornerRadius: 16.0))
            )
        }
        
        private func metricsView(for reflection: Reflection) -> some View {
            VStack(alignment: .leading, spacing: 16.0) {
                HStack {
                    metricView(
                        for: reflection,
                           symbol: "heart",
                           metricKeyPath: \.heartRate,
                           unitText: "bpm"
                    )
                    
                    Spacer()
                    
                    metricView(
                        for: reflection,
                           symbol: "drop",
                           metricKeyPath: \.oxygenSaturation,
                           unitText: "sp02"
                    )
                    
                    Spacer()
                    
                    metricView(
                        for: reflection,
                           symbol: "wind",
                           metricKeyPath: \.breathingRate,
                           unitText: "rpm"
                    )
                }
                .accentColor(Theme.color(.captureText))
                
                VStack(alignment: .leading, spacing: 4.0) {
                    reportValueTemplate(
                        for: "Heart Rate Variability",
                        label: {
                            Text(value: reflection.heartRateVariability, format: .metric)
                                .themeFont(.bodyBold)
                        }
                    )
                    reportValueTemplate(
                        for: "Stress Level",
                        label: {
                            Text(reflection.stressLevel.displayText)
                                .themeFont(.bodyBold)
                                .foregroundColor(reflection.stressLevel.foregroundColor)
                        }
                    )
                }
                .themeFont(.body)
            }
        }
        
        private func footerView() -> some View {
            HStack {
                VStack(alignment: .leading) {
                    Text("Next:")
                        .themeFont(.subtitle)
                    Text("Review and Share")
                        .themeFont(.action)
                }
                
                Spacer()
                
                if let reflection = reflectStore.reflection(for: captureID) {
                    NavigationLink(
                        destination: { ReflectionDetailsView(reflection: reflection, canDelete: false) },
                        label: { Image(systemName: "arrow.right") }
                    )
                    .themeButtonStyle(.ctaCircle)
                }
            }
        }
        
        private func metricView(
            for reflection: Reflection,
            symbol: String,
            metricKeyPath: KeyPath<Reflection.AverageMetrics, Double?>,
            unitText: String
        ) -> some View {
            let value = reflection.metrics[keyPath: metricKeyPath]
            let accentColor = value == .none ? Theme.color(.textBody) : Theme.color(.accept)
            
            return MetricView(
                symbol: symbol,
                value: value,
                unitText: unitText,
                animated: false
            )
            .accentColor(accentColor)
        }
        
        private func reportValueTemplate<Label: View>(
            for title: String,
            label: () -> Label
        ) -> some View {
            HStack {
                Text(title)
                    .themeFont(.body)
                Spacer()
                label()
            }
        }
    }
}

// MARK: Extensions

extension CaptureState.CaptureStage {
    var countdownText: String {
        switch self {
        case .metricsCountdown:
            return "We’ll now collect "
        case .videoCountdown:
            return "Go ahead and "
        default:
            return ""
        }
    }
    
    var countdownTextEmphasized: String {
        switch self {
        case .metricsCountdown:
            return "your health metrics."
        case .videoCountdown:
            return "tell us how you’re feeling."
        default:
            return ""
        }
    }
}

extension Reflection.StressLevel {
    var displayText: String {
        switch self {
        case .unknown: return ""
        case .low: return "Low"
        case .normal: return "Normal"
        case .mild: return "Mild"
        case .high: return "High"
        case .extreme: return "Extreme"
        }
    }
    
    var foregroundColor: Color {
        switch self {
        case .unknown:
            return Theme.color(.textBody)
        case .low, .normal, .mild:
            return Theme.color(.accept)
        case .high, .extreme:
            return Theme.color(.alert)
        }
    }
}

// MARK: Previews

#if DEBUG

struct RecordingView_Previews: PreviewProvider {
    static let previewState: CaptureState = .init(
        stage: .metricsCapture,
        captureId: "100",
        countdown: 3,
        metricsImage: .init(
            image: UIImage(named: "capture.image.test"),
            faceRect: CGRect(x: 300.0, y: 100.0, width: 200.0, height: 300.0)
        ),
        metricCapturePercent: 0.5,
        videoCapturePercent: 0.5,
        symptoms: [.headache, .nausea]
    )
    
    static let reflectState = ReflectionsState(
        userID: "",
        selectedFilter: .reflections,
        sections: [],
        reflections: [
            "100": .init(
                id: "100",
                date: Date(),
                metrics: .init(
                    heartRate: 65.3,
                    oxygenSaturation: 98.4,
                    breathingRate: 12.5
                ),
                symptoms: [.headache, .nausea],
                heartRateVariability: 56.5,
                stressLevel: .low
            )
        ]
    )
    
    static var previews: some View {
        CaptureView(store: .init(previewState))
            .environmentObject(ReflectionsStore(reflectState))
            .environment(\.colorScheme, .light)
    }
}

#endif
