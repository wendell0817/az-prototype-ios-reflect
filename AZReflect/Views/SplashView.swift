//
//  SplashView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import SwiftUI

struct SplashView: View {
    @State private var showLogo = false
    
    var body: some View {
        GeometryReader { proxy in
            VStack(spacing: 0.0) {
                ReflectLogoView()
                    .frame(height: 100.0)
                    .frame(height: showLogo ? 100.0 : 0.0, alignment: .top)
                    .frame(height: 100.0, alignment: .bottom)
                    .clipped()
                
                RoundedRectangle(cornerRadius: 1.0)
                    .themeModifier(.reflectGradient)
                    .frame(height: 2.0)
                
                ReflectLogoView()
                    .rotation3DEffect(
                        .degrees(180.0),
                        axis: (1.0, 0.0, 0.0),
                        anchor: .center
                    )
                    .frame(height: 100.0)
                    .frame(height: showLogo ? 100.0 : 0.0, alignment: .bottom)
                    .frame(height: 100.0, alignment: .top)
                    .mask(
                        LinearGradient(
                            gradient: .init(
                                colors: [
                                    Color.white.opacity(0.5),
                                    Color.clear
                                ]
                            ),
                            startPoint: .top,
                            endPoint: .bottom
                        )
                    )
                    .clipped()
                
                AZLogoView()
                    .foregroundColor(Theme.color(.textBody))
                    .opacity(showLogo ? 1.0 : 0.0)
                    .animation(.easeIn(duration: 0.75).delay(0.25), value: showLogo)
            }
            .frame(size: proxy.size)
            .animation(Animation.easeOut(duration: 1.0), value: showLogo)
        }
        .padding(32.0)
        .background(Theme.color(.background).ignoresSafeArea())
        .onAppear { showLogo = true }
    }
}

#if DEBUG

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}

#endif
