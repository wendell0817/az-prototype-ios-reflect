//
//  AppView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import SwiftUI

struct AppView: View {
    @EnvironmentObject private var userStore: UserStore
    @State private var isAppReady = false
    
    var body: some View {
        rootView()
            .animation(.easeInOut, value: userStore.state)
            .background(
                Theme.color(.background).ignoresSafeArea()
            )
            .overlay(
                GeometryReader { proxy in
                    SplashView()
                        .offset(y: isAppReady ? proxy.size.height : 0.0)
                        .frame(size: proxy.size)
                        .animation(.easeOut.delay(1.5), value: isAppReady)
                }
                .ignoresSafeArea()
            )
            .onChange(of: userStore.state) { _ in
                isAppReady = true
            }
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                    userStore.loadUser()
                }
            }
    }
    
    @ViewBuilder private func rootView() -> some View {
        switch userStore.state {
        case .none:
            Theme.color(.background).ignoresSafeArea()
        case .loggedIn:
            HomeView()
        default:
            WelcomeView()
        }
    }
}

#if DEBUG

extension AppView {
    init(isAppReady: Bool = false) {
        self._isAppReady = State(initialValue: isAppReady)
    }
}

struct AppView_Previews: PreviewProvider {
    static let user = User(
        id: "100",
        name: "Testerson",
        email: "test@test.com",
        isOnboarded: true
    )
    
    static let userState = UserState.loggedIn(user)
    static let reflectionstate = ReflectionsState(
        selectedFilter: .reflections,
        sections: [
            .init(type: .checkIn, items: [.checkIn]),
            .init(
                type: .reflections,
                items: [
                    .reflectionFilter,
                    .reflection(.init(id: "100", date: Date()))
                ]
            )
        ]
    )
    
    static var previews: some View {
        AppView(isAppReady: true)
            .environmentObject(UserStore(userState))
            .environmentObject(ReflectionsStore(reflectionstate))
    }
}

#endif
