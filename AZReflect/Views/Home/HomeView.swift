//
//  HomeView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import SwiftUI

struct HomeView: View {
    @EnvironmentObject private var reflectionsStore: ReflectionsStore
    @State private var homeNavItem: HomeNavigationItem = .none
    
    var body: some View {
        NavigationView {
            VStack(spacing: 8.0) {
                HomeView.StatusView()
                    .padding(.horizontal, 24.0)
                    .padding(.top, 32.0)
                
                
                GeometryReader { proxy in
                    ScrollView {
                        ScrollViewReader { scrollProxy in
                            VStack(spacing: 54.0) {
                                ForEach(reflectionsStore.sections) { section in
                                    view(for: section)
                                }
                            }
                            .padding(.horizontal, 24.0)
                            .frame(width: proxy.size.width)
                        }
                    }
                    .frame(size: proxy.size)
                    .animation(.default, value: reflectionsStore.sections)
                }
            }
            .background(Theme.color(.backgroundInstruction).ignoresSafeArea())
            .navigationBarHidden(true)
            .navigationTitle("Home")
        }
        .onChange(of: homeNavItem) { navItem in
            switch navItem {
            case .none:
                reflectionsStore.unsubscribeFromCaptureState()
            default:
                break
            }
        }
        .homeNavigationRoot($homeNavItem)
        .navigationViewStyle(.stack)
        .onAppear {
            reflectionsStore.loadReflections()
        }
    }
    
    private func view(for section: ReflectionsState.Section) -> some View {
        SectionView(sectionType: section.type, header: { headerView(for: section) }) {
            VStack {
                ForEach(section.items) { item in
                    VStack(spacing: 0.0) {
                        rowView(for: item)
                        
                        if item.showDivider && item != section.items.last {
                            Divider().padding(.vertical, 16.0)
                        }
                    }
                }
            }
            .padding(section.contentPadding)
            .background(Theme.color(.backgroundHomeCard))
        }
    }
    
    @ViewBuilder private func headerView(for section: ReflectionsState.Section) -> some View {
        switch section.type {
        case .checkIn:
            EmptyView()
        case .reflections:
            Label("Search Reflections", systemImage: "magnifyingglass")
        case .trends:
            Label("Trends", systemImage: "chart.pie")
        }
    }
    
    @ViewBuilder private func rowView(for item: ReflectionsState.Item) -> some View {
        switch item {
        case .checkIn:
            HomeView.CheckInView()
        case .reflectionFilter:
            HomeView.FilterView()
                .padding(.bottom, 20.0)
        case .reflection(let reflection):
            NavigationLink(
                isActive: $homeNavItem.isActive(for: .review(reflection.id)),
                destination: { ReflectionDetailsView(reflection: reflection, canDelete: true) },
                label: { HomeView.ReflectionView(reflection: reflection) }
            )
        case .trend, .highlight:
            HStack {
                Text("Coming Soon")
                Spacer()
            }
        }
    }
}

// MARK: StatusView

extension HomeView {
    struct StatusView: View {
        @EnvironmentObject private var userStore: UserStore
        @Environment(\.homeNavigationRoot) private var homeNavigationRoot
        
        var body: some View {
            HStack(spacing: 16.0) {
                ReflectLogoView(Theme.color(.backgroundInverse), style: .small)
                Spacer()
                
                Button(
                    action: {},
                    label: { Image(systemName: "bell") }
                )
                
                NavigationLink(
                    isActive: homeNavigationRoot.isActive(for: .settings),
                    destination: { SettingsView() },
                    label: { Image("profile") }
                )
            }
            .foregroundColor(Theme.color(.backgroundInverse))
            .themeFont(.title)
        }
    }
}

// MARK: SectionView

extension HomeView {
    struct SectionView<Header: View, Content: View>: View {
        let sectionType: ReflectionsState.SectionType
        let header: () -> Header
        let content: () -> Content
        
        var body: some View {
            VStack(alignment: .leading, spacing: 20.0) {
                HStack {
                    header()
                        .themeFont(.subtitle)
                        .foregroundColor(Theme.color(.textCaption))
                    
                    Spacer()
                }
                
                content()
                    .overlay(overlayView())
                    .mask(RoundedRectangle(cornerRadius: 20.0))
                    .shadow(color: Color.black.opacity(0.1), radius: 4.0, x: 0.0, y: 0.0)
                    
            }
        }
        
        @ViewBuilder private func overlayView() -> some View {
            switch sectionType {
            case .checkIn:
                RoundedRectangle(cornerRadius: 20.0)
                    .stroke(Color.white, lineWidth: 2.0)
                    .padding(1.0)
            default:
                EmptyView()
            }
        }
    }
}

// MARK: Row Views

extension HomeView {
    struct CheckInView: View {
        @EnvironmentObject private var userStore: UserStore
        @Environment(\.homeNavigationRoot) private var homeNavigationRoot
        
        var greetingText: String {
            let text = "Good morning"
            switch userStore.state.user {
            case .some(let user):
                return text + ", \(user.name)"
            default:
                return text
            }
        }
        
        var body: some View {
            VStack(spacing: 0.0) {
                Text(greetingText)
                    .themeFont(.titleBold)
                    .padding(.bottom, 8.0)
                
                Text("We’re ready for your weekly\rcheck-in when you are.")
                    .themeFont(.subtitle)
                    .foregroundColor(Theme.color(.textCaption))
                    .padding(.bottom, 60.0)
                
                NavigationLink(
                    isActive: homeNavigationRoot.isActive(for: .capture),
                    destination: { CaptureView() },
                    label: { Text("Start New Reflection") }
                )
                .themeButtonStyle(.cta)
                .padding(.horizontal, 32.0)
            }
            .multilineTextAlignment(.center)
            .padding(.horizontal, 32.0)
            .frame(height: 320.0)
            .background(
                Image("reflection.card.background")
                    .resizable()
                    .scaledToFill()
            )
        }
    }
    
    struct FilterView: View {
        @EnvironmentObject private var reflectionsStore: ReflectionsStore
        @State private var filter: ReflectionsState.ReflectionFilter = .reflections
        
        var body: some View {
            VStack {
                ReflectSegmentedView(
                    data: ReflectionsState.ReflectionFilter.allCases,
                    id: \.self,
                    selection: $filter,
                    content: { Text($0.title) }
                )
            }
            .onChange(of: filter) {
                reflectionsStore.loadSections(for: $0)
            }
            .pickerStyle(SegmentedPickerStyle())
        }
    }
    
    struct ReflectionView: View {
        let reflection: Reflection
        
        var body: some View {
            VStack(alignment: .leading, spacing: 0.0) {
                Text(date: reflection.date, format: .short)
                    .foregroundColor(Theme.color(.textCaption))
                    .themeFont(.caption)
                    .padding(.bottom, 8.0)
                
                HStack(spacing: 2.0) {
                    Text(reflection.title)
                    Image(systemName: "chevron.right")
                }
                .themeFont(.action)
                .themeModifier(.reflectGradient)
                .padding(.bottom, 8.0)
                
                HStack {
                    Text(reflection.symptoms.map(\.title).joined(separator: ", "))
                }
                .foregroundColor(Theme.color(.textCaption))
                .themeFont(.caption)
                .padding(.bottom, 32.0)
                
                HStack {
                    metricView(for: "heart", keyPath: \.heartRate, unitText: "bpm")
                    Spacer().layoutPriority(-1.0)
                    metricView(for: "drop", keyPath: \.oxygenSaturation, unitText: "sp02")
                    Spacer().layoutPriority(-1.0)
                    metricView(for: "wind", keyPath: \.breathingRate, unitText: "rpm")
                }
                .accentColor(Theme.color(.textBody))
                .foregroundColor(Theme.color(.textBody))
            }
        }
        
        func metricView(
            for symbol: String,
            keyPath: KeyPath<Reflection.AverageMetrics, Double?>,
            unitText: String
        ) -> some View {
            let value = reflection.metrics[keyPath: keyPath]
            
            return MetricView(
                symbol: symbol,
                value: value,
                unitText: unitText,
                animated: false
            )
        }
    }
}

// MARK: Extensions

extension ReflectionsState.Section {
    var contentPadding: CGFloat {
        switch self.type {
        case .checkIn:
            return 0.0
        default:
            return 20.0
        }
    }
}

extension ReflectionsState.Item {
    var showDivider: Bool {
        switch self {
        case .reflection, .trend:
            return true
        default:
            return false
        }
    }
}

extension ReflectionsState.ReflectionFilter {
    var title: String {
        switch self {
        case .reflections: return "Reflections"
        case .highlights: return "Highlights"
        }
    }
}

// MARK: Previews

#if DEBUG

struct HomeView_Previews: PreviewProvider {
    static let reflectionsState = ReflectionsState(
        userID: "100",
        selectedFilter: .reflections,
        sections: [
            .init(type: .checkIn, items: [.checkIn]),
            .init(
                type: .reflections,
                items: [
                    .reflectionFilter,
                    .reflection(
                        .init(id: "100",
                              date: Date(),
                              metrics: .init(
                                heartRate: 98.1,
                                oxygenSaturation: 99.2,
                                breathingRate: 12.3
                              ),
                              symptoms: [.headache, .nausea]
                        )
                    )
                ]
            )
        ],
        reflections: [:]
    )
    
    static var previews: some View {
        Group {
            HomeView()
                .background(Theme.color(.backgroundInstruction).ignoresSafeArea())
            
            HomeView.FilterView()
                .padding()
                .background(Theme.color(.backgroundInstruction))
                .previewLayout(.sizeThatFits)
            
            HomeView.ReflectionView(
                reflection: .init(
                    id: "100",
                    date: Date(),
                    metrics: .init(
                        heartRate: .none,
                        oxygenSaturation: 99.9,
                        breathingRate: 14.2
                    ),
                    symptoms: [.headache, .nausea, .acne]
                )
            )
            .padding()
            .background(Theme.color(.backgroundInstruction))
            .previewLayout(.sizeThatFits)
        }
        .environmentObject(UserStore())
        .environmentObject(ReflectionsStore(reflectionsState))
        .environment(\.colorScheme, .light)
    }
}

#endif
