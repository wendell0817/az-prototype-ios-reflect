//
//  ReflectionDetailsView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 10/20/21.
//

import SwiftUI

struct ReflectionDetailsView: View {
    @EnvironmentObject private var reflectionStore: ReflectionsStore
    let reflection: Reflection
    let canDelete: Bool
    private let contentPadding: CGFloat = 20.0
    
    var body: some View {
        GeometryReader { proxy in
            VStack(spacing: 0.0) {
                ReflectionDetailsView.HeaderView(reflection: reflection)
                    .padding(.horizontal, contentPadding)
                    .padding(.top, 40.0)
                
                ScrollView {
                    VStack(spacing: 32.0) {
                        ReflectionDetailsView.VideoView(
                            store: reflectionStore,
                            reflection: reflection,
                            width: proxy.size.width - (contentPadding * 2.0)
                        )
                        
                        ReflectionDetailsView.SymptomsView(
                            reflectStore: reflectionStore,
                            reflection: reflection
                        )
                        
                        ReflectionDetailsView.ActionsView(
                            reflection: reflection,
                            canDelete: canDelete
                        )
                    }
                    .padding(.horizontal, contentPadding)
                    .padding(.vertical, 32.0)
                    .frame(width: proxy.size.width)
                }
            }
        }
        .navigationBarHidden(true)
    }
}

// MARK: HeaderView

extension ReflectionDetailsView {
    struct HeaderView: View {
        let reflection: Reflection
        
        static var dateFormatter: DateFormatter {
            DateFormatterUtility.formatter(for: .custom("MMM d"))
        }
        
        static var timeFormatter: DateFormatter {
            DateFormatterUtility.formatter(for: .custom("h:mm a"))
        }
        
        var body: some View {
            VStack {
                HStack {
                    BackNavigationView()
                        .frame(width: 100.0)

                    Spacer()

                    Text(reflection.title)
                        .themeFont(.action)

                    Spacer()

                    Spacer()
                        .frame(width: 100.0)
                }
                
                Text(dateText(for: reflection.date))
                    .themeFont(.caption)
            }
        }
        
        private func dateText(for date: Date) -> String {
            let dateText = Self.dateFormatter.string(from: date)
            let timeText = Self.timeFormatter.string(from: date)
            return "\(dateText) at \(timeText)"
        }
    }
}

// MARK: VideoView

extension ReflectionDetailsView {
    struct VideoView: View {
        @ObservedObject var store: ReflectionsStore
        let reflection: Reflection
        let width: CGFloat
        
        private let sizeRatio = SizeRatio(width: 16.0, height: 9.0)
        
        var body: some View {
            VStack {
                HStack {
                    Button(
                        action: {},
                        label: { Image(systemName: "play") }
                    )
                    .themeButtonStyle(.capturePlay)
                    
                    Spacer()
                }
                
                Spacer()
                
                HStack {
                    metricView(for: \.heartRate)
                    Spacer()
                    metricView(for: \.oxygenSaturation)
                    Spacer()
                    metricView(for: \.breathingRate)
                }
                .accentColor(.white)
            }
            .padding(16.0)
            .frame(height: sizeRatio.height(for: width))
            .background(
                VideoThumbnailView(url: store.videoURL(for: reflection))
                    .overlay(Color.black.opacity(0.5))
            )
            .mask(RoundedRectangle(cornerRadius: 8.0))
        }
        
        private func metricView(
            for keyPath: KeyPath<Reflection.AverageMetrics, Double?>
        ) -> some View {
            let symbol: String
            let unitText: String
            
            switch keyPath {
            case \.heartRate:
                symbol = "heart"
                unitText = "BPM"
            case \.oxygenSaturation:
                symbol = "drop"
                unitText = "sp02"
            case \.breathingRate:
                symbol = "wind"
                unitText = "RPM"
            default:
                symbol = ""
                unitText = ""
            }
            
            return MetricView(
                symbol: symbol,
                value: reflection.metrics[keyPath: keyPath],
                unitText: unitText,
                foregroundColor: .white,
                animated: false
            )
        }
    }
}

// MARK: SymptomsView

extension ReflectionDetailsView {
    struct SymptomsView: View {
        struct ViewState {
            var edit: ReflectionsState.ReflectionEdit
            var isSelectorPresented = false
        }
        
        @ObservedObject var reflectStore: ReflectionsStore
        @State private var viewState: ViewState
        
        init(reflectStore: ReflectionsStore, reflection: Reflection) {
            self._reflectStore = .init(wrappedValue: reflectStore)
            
            let edit = ReflectionsState.ReflectionEdit(reflection)
            let viewState = ViewState(edit: edit)
            self._viewState = .init(wrappedValue: viewState)
        }
        
        var body: some View {
            VStack(alignment: .leading, spacing: 0.0) {
                Text("Here are the symptoms we detected")
                    .themeFont(.body)
                    .frame(height: 32.0, alignment: .top)
                
                WrappingHStack(
                    data: viewState.edit.symptoms,
                    id: \.self, spacing: 16.0) { symptom in
                        Button(
                            action: {
                                viewState.edit.remove(symptom: symptom)
                                reflectStore.apply(viewState.edit)
                            },
                            label: { view(for: symptom) }
                        )
                    }
                    .animation(.easeInOut, value: viewState.edit.symptoms)
                
                Text("Add any symptoms we missed.")
                    .themeFont(.action)
                    .padding(.top, 32.0)
                
                Button(
                    "Add a symptom",
                    action: { viewState.isSelectorPresented = true }
                )
                .themeButtonStyle(.shareAdd)
                .padding(.top, 16.0)
                .sheet(
                    isPresented: $viewState.isSelectorPresented,
                    onDismiss: { },
                    content: {
                        SymptomSelectorView(
                            excludedSymptoms: viewState.edit.symptoms,
                            symptomSelected: { symptom in
                                viewState.edit.symptoms.append(symptom)
                                reflectStore.apply(viewState.edit)
                                viewState.isSelectorPresented = false
                            }
                        )
                    }
                )
            }
        }
        
        private func view(for symptom: Symptom) -> some View {
            HStack(spacing: 8.0) {
                Text(symptom.title)
                    .foregroundColor(Theme.color(.accentDark))
                
                Image(systemName: "xmark")
                    .foregroundColor(Theme.color(.alert))
            }
            .themeFont(.action)
            
            .padding(.horizontal, 8.0)
            .frame(height: 32.0)
            .background(
                RoundedRectangle(cornerRadius: 8.0)
                    .stroke(Theme.color(.accentDark), lineWidth: 2.0)
            )
        }
    }
}

// MARK: ActionsView

extension ReflectionDetailsView {
    struct ActionsView: View {
        let reflection: Reflection
        let canDelete: Bool
        @EnvironmentObject private var store: ReflectionsStore
        @Environment(\.homeNavigationRoot) private var homeNavigationRoot
        @State private var isDeletePresented = false
        
        var body: some View {
            VStack(spacing: 16.0) {
                NavigationLink(
                    "Share Reflection",
                    destination: {
                        ReflectionShareView(reflection: reflection)
                            .homeNavigationRoot(homeNavigationRoot)
                    }
                )
                .themeButtonStyle(.cta)
                
                if canDelete {
                    Button("Delete", action: { isDeletePresented = true })
                        .themeButtonStyle(.destructive)
                }
            }
            .alert(
                isPresented: $isDeletePresented,
                content: {
                    Alert(
                        title: Text(reflection.title),
                        message: Text("Are you sure you want to delete this reflection?"),
                        primaryButton: .cancel(),
                        secondaryButton: .destructive(
                            Text("Delete"),
                            action: {
                                homeNavigationRoot.wrappedValue.dismiss()
                                store.delete(by: reflection.id)
                            }
                        )
                    )
                }
            )
        }
    }
}

#if DEBUG

struct ReflectionDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ReflectionDetailsView(reflection: Reflection.mock, canDelete: true)
        }
        .environmentObject(ReflectionsStore())
    }
}

#endif
