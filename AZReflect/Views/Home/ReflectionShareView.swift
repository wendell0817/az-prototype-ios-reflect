//
//  ReflectionShareView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 10/21/21.
//

import SwiftUI

struct ReflectionShareView: View {
    @EnvironmentObject private var reflectStore: ReflectionsStore
    @Environment(\.homeNavigationRoot) private var homeNavigationRoot
    let reflection: Reflection
    
    var body: some View {
        GeometryReader { proxy in
            VStack(spacing: 0.0) {
                ReflectionShareView.HeaderView(reflection: reflection)
                    .padding(.horizontal, 20.0)
                    .padding(.top, 40.0)
                
                ScrollView {
                    VStack(spacing: 32.0) {
                        ReflectionShareView.VideoView(
                            store: reflectStore,
                            width: proxy.size.width - 20.0,
                            reflection: reflection
                        )
                        
                        ReflectionShareView.ShareView(store: reflectStore)
                            .onAppear { reflectStore.loadGroup() }
                        
                        ReflectionShareView.ActionsView(
                            finishedTapped: {
                                homeNavigationRoot.wrappedValue.dismiss()
                            }
                        )
                    }
                    .padding(.vertical, 32.0)
                    .padding(.horizontal, 20.0)
                }
            }
        }
        .navigationBarHidden(true)
    }
}

// MARK: HeaderView

extension ReflectionShareView {
    struct HeaderView: View {
        let reflection: Reflection
        
        static var dateFormater = DateFormatterUtility.formatter(for: .custom("MMM d, yyyy"))
        
        var body: some View {
            VStack {
                HStack {
                    BackNavigationView()
                        .frame(width: 40.0)
                    
                    Spacer()
                    
                    Text(reflection.title)
                        .themeFont(.body)
                    
                    Spacer()
                    
                    Spacer()
                        .frame(width: 40.0)
                }
                
                Text(Self.dateFormater.string(from: reflection.date))
                    .themeFont(.caption)
            }
        }
    }
}

// MARK: VideoView

extension ReflectionShareView {
    struct VideoView: View {
        @ObservedObject var store: ReflectionsStore
        let width: CGFloat
        let reflection: Reflection
        private let sizeRatio = SizeRatio(width: 16.0, height: 9.0)
        
        var body: some View {
            VStack(alignment: .leading) {
                HStack {
                    Button(action: {}, label: { Image(systemName: "play") })
                        .themeButtonStyle(.capturePlay)
                    
                    Spacer()
                }
                
                Spacer()
                
                symptomTextView()
                    .foregroundColor(.white)
            }
            .padding(16.0)
            .frame(height: sizeRatio.height(for: width))
            .background(
                VideoThumbnailView(url: store.videoURL(for: reflection))
                    .overlay(Color.black.opacity(0.5))
            )
            .mask(RoundedRectangle(cornerRadius: 8.0))
        }
        
        private func symptomTextView() -> Text {
            let text = reflection.symptoms.map(\.title).joined(separator: ", ")
            return Text(text)
        }
    }
}

// MARK: ShareView

extension ReflectionShareView {
    struct ShareView: View {
        @ObservedObject var store: ReflectionsStore
        @State private var edits: [ReflectionsState.ReflectionShareEdit] = []
        @State private var isContactsPresented = false
        
        var body: some View {
            VStack {
                HStack {
                    Text("Share With:")
                    Spacer()
                    Button("Unselect All", action: deselectAll)
                }
                
                VStack(spacing: 32.0) {
                    VStack(spacing: 0.0) {
                        ForEach($edits) { edit in
                            VStack(spacing: 0.0) {
                                HStack {
                                    ProfileImageView(member: edit.wrappedValue.member)
                                        .frame(width: 32.0, height: 32.0)
                                        .foregroundColor(.white)
                                    
                                    Text(edit.wrappedValue.member.name)
                                        .lineLimit(1)
                                        .themeFont(.action)
                                    
                                    Spacer()
                                    
                                    Toggle("", isOn: edit.isOn)
                                        .frame(width: 80.0)
                                }
                                .padding(.horizontal, 16.0)
                                .frame(height: 50.0)
                                .toggleStyle(SwitchToggleStyle(tint: Theme.color(.accentDark)))
                                
                                if edit.wrappedValue.member.id != edits.last?.id {
                                    Rectangle()
                                        .fill(Theme.color(.backgroundBorder))
                                        .frame(height: 1.0)
                                }
                            }
                        }
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 8.0)
                            .fill(Theme.color(.backgroundInstruction))
                            .overlay(
                                RoundedRectangle(cornerRadius: 8.0)
                                    .stroke(Theme.color(.backgroundBorder), lineWidth: 1.0)
                            )
                    )
                    
                    Button(
                        "Add from my contacts",
                        action: { isContactsPresented = true }
                    )
                    .themeButtonStyle(.shareAdd)
                    .sheet(
                        isPresented: $isContactsPresented,
                        onDismiss: {},
                        content: {
                            ContactSearchView(
                                contactSelected: { contact in
                                    isContactsPresented = false
                                    guard let email = contact.email else { return }
                                    store.addShareMember(with: email)
                                }
                            )
                        }
                    )
                }
                .onAppear {
                    loadEdits(for: store.state.shareGroup)
                }
                .onChange(of: store.state.shareGroup) { group in
                    loadEdits(for: group)
                }
            }
        }
        
        func deselectAll() {
            withAnimation { setEdits(to: false) }
        }
        
        func setEdits(to isOn: Bool) {
            edits = edits.map { edit -> ReflectionsState.ReflectionShareEdit in
                return .init(member: edit.member, isOn: isOn)
            }
        }
        
        func loadEdits(for group: ShareGroup) {
            let group = store.state.shareGroup
            let allMembers = group.members + group.outgoingInvites
            edits = allMembers.map { member -> ReflectionsState.ReflectionShareEdit in
                let existingEdit = edits.first(where: { $0.member.id == member.id })
                switch existingEdit {
                case .some(let edit):
                    return edit
                default:
                    return .init(member: member)
                }
            }
        }
    }
}

// MARK: ActionsView

extension ReflectionShareView {
    struct ActionsView: View {
        let finishedTapped: () -> Void
        @State private var isFinishPresented = false
        
        var body: some View {
            VStack {
                Button(
                    "Share",
                    action: { isFinishPresented = true }
                )
                .themeButtonStyle(.cta)
            }
            .sheet(
                isPresented: $isFinishPresented,
                onDismiss: finishedTapped,
                content: {
                    ShareCompletedView(
                        finishTapped: { isFinishPresented = false }
                    )
                }
            )
        }
    }
}

#if DEBUG

struct ReflectionShareView_Previews: PreviewProvider {
    static let reflectionState = ReflectionsState(
        userID: "100",
        selectedFilter: .reflections,
        sections: [],
        reflections: [:],
        shareGroup: .mock
    )
    
    static var previews: some View {
        NavigationView {
            ReflectionShareView(reflection: Reflection.mock)
        }
        .environmentObject(ReflectionsStore(reflectionState))
    }
}

#endif
