//
//  SettingsView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 10/19/21.
//

import SwiftUI

struct SettingsView: View {
    let sections: [SettingsView.Section] = [
        .init(id: .privacy, rows: [.privacy]),
        .init(id: .account, rows: [.account])
    ]
    
    var body: some View {
        GeometryReader { proxy in
            ScrollView {
                VStack(spacing: 32.0) {
                    ForEach(sections) { section in
                        SectionView(section: section)
                    }
                }
                .frame(width: proxy.size.width)
            }
        }
        .listStyle(.plain)
        .navigationTitle("Settings")
    }
}

extension SettingsView {
    struct SectionView: View {
        let section: Section
        var body: some View {
            VStack(alignment: .leading, spacing: 0.0) {
                VStack(alignment: .leading, spacing: 0.0) {
                    Divider()
                    
                    ForEach(section.rows, id: \.self) { row in
                        VStack(spacing: 0.0) {
                            NavigationLink(
                                destination: { destView(for: row) },
                                label: {
                                    SettingsView.RowView(row: row)
                                        .contentShape(Rectangle())
                                }
                            )
                            .buttonStyle(.plain)
                            
                            if row != section.rows.last {
                                Divider()
                            }
                        }
                    }
                    
                    Divider()
                }
                .background(Theme.color(.backgroundInstruction))
                
                Text(section.footer)
                    .foregroundColor(Theme.color(.textCaption))
                    .themeFont(.subtitle)
                    .padding(.horizontal, 20.0)
                    .padding(.top, 8.0)
            }
        }
        
        @ViewBuilder private func destView(for row: SettingsView.Row) -> some View {
            switch row {
            case .privacy:
                SettingsPrivacyView()
            case .account:
                SettingsAccountView()
            }
        }
    }
}

extension SettingsView {
    struct RowView: View {
        let row: SettingsView.Row
        
        var body: some View {
            HStack {
                Image(systemName: row.symbol)
                    .font(.system(size: 20.0).weight(.semibold))
                    .frame(width: 32.0, height: 32.0)
                    .foregroundColor(Color.white)
                    .background(
                        RoundedRectangle(cornerRadius: 4.0)
                            .fill(row.symbolColor)
                    )
                
                Text(row.title)
                    .themeFont(.body)
                
                Spacer()
                
                Image(systemName: "chevron.right")
                    .font(.system(size: 14.0))
            }
            .padding(.horizontal, 20.0)
            .frame(height: 44.0)
        }
    }
}

extension SettingsView {
    enum SectionType: Int {
        case privacy
        case account
    }
    
    enum Row: Hashable {
        case privacy
        case account
        
        var symbol: String {
            switch self {
            case .privacy: return "lock"
            case .account: return "person"
            }
        }
        
        var symbolColor: Color {
            switch self {
            case .privacy: return Theme.color(.settingsPrivacy)
            case .account: return Theme.color(.settingsGroup)
            }
        }
        
        var title: String {
            switch self {
            case .privacy: return "Privacy"
            case .account: return "Account"
            }
        }
    }
    
    struct Section: Identifiable {
        let id: SectionType
        let rows: [Row]
        
        var footer: String {
            switch id {
            case .privacy: return "Control what people see when you share"
            case .account: return "Change details associated with this account"
            }
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            SettingsView()
        }
    }
}
