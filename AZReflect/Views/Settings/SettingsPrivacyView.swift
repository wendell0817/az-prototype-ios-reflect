//
//  SettingsPrivacyView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 10/21/21.
//

import SwiftUI

struct SettingsPrivacyView: View {
    struct ViewState {
        var edits: [ReflectionsState.MemberPrivacyEdit] = []
        var animation: Animation? = .none
    }
    
    @EnvironmentObject private var reflectStore: ReflectionsStore
    @State private var viewState = ViewState()
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 32.0) {
                Text("For each contact you’re sharing with, you can control what they see.")
                    .themeFont(.subtitle)
                    .foregroundColor(Theme.color(.textCaption))
                    .padding(.top, 32.0)
                
                ForEach($viewState.edits) { edit in
                    SettingsPrivacyView.MemberView(
                        group: reflectStore.shareGroup,
                        edit: edit,
                        onDelete: { reflectStore.removeShareMember($0) }
                    )
                }
            }
            .padding(.horizontal, 20.0)
        }
        .animation(viewState.animation, value: viewState.edits)
        .navigationTitle("Privacy")
        .onAppear {
            reflectStore.loadGroup()
            buildEdits(for: reflectStore.shareGroup)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                viewState.animation = .default
            }
        }
        .onChange(
            of: reflectStore.shareGroup,
            perform: buildEdits(for:)
        )
    }
    
    func buildEdits(for group: ShareGroup) {
        viewState.edits = [group.members, group.outgoingInvites]
            .flatMap { $0 }
            .map { member -> ReflectionsState.MemberPrivacyEdit in
                let existingEdit = viewState.edits
                    .first(where: { $0.member.email.lowercased() == member.email.lowercased() })
                switch existingEdit {
                case .some(let edit):
                    return edit
                default:
                    return .init(member: member)
                }
            }
    }
}

extension SettingsPrivacyView {
    struct MemberView: View {
        let group: ShareGroup
        @Binding var edit: ReflectionsState.MemberPrivacyEdit
        @State private var isDeletePresented = false
        let onDelete: (ShareGroup.Member) -> Void
        
        var body: some View {
            VStack(alignment: .leading) {
                HStack {
                    ProfileImageView(member: edit.member)
                        .foregroundColor(.white)
                        .frame(width: 32.0, height: 32.0)
                    
                    VStack(alignment: .leading, spacing: 0.0) {
                        Text(edit.member.name)
                            .foregroundColor(Theme.color(.headerText))
                            .themeFont(.action)
                        
                        if group.isOutgoing(edit.member) {
                            Text("Pending")
                                .themeFont(.caption)
                                .foregroundColor(Theme.color(.alert))
                        }
                    }
                    
                    Spacer()
                    
                    Button(
                        action: { isDeletePresented = true },
                        label: {
                            Image(systemName: "trash")
                                .foregroundColor(Theme.color(.textCaption))
                        }
                    )
                    .alert(
                        isPresented: $isDeletePresented,
                        content: {
                            deleteAlert(
                                for: edit.member,
                                onDelete: { onDelete(edit.member) }
                            )
                        }
                    )
                }
                .padding(.horizontal, 16.0)
                .frame(height: 48.0)
                .background(Theme.color(.headerBackground))
                
                VStack {
                    Toggle("Can see video", isOn: $edit.shareVideo)
                    Divider()
                    Toggle("Can see metrics", isOn: $edit.shareMetrics)
                    Divider()
                    Toggle("Can see transcripts", isOn: $edit.shareTrancript)
                }
                .toggleStyle(SwitchToggleStyle(tint: Theme.color(.accentDark)))
                .padding(.horizontal, 16.0)
                .padding(.bottom, 16.0)
            }
            .mask(RoundedRectangle(cornerRadius: 8.0))
            .background(
                RoundedRectangle(cornerRadius: 8.0)
                    .fill(Theme.color(.background))
            )
            .overlay(
                RoundedRectangle(cornerRadius: 8.0)
                    .stroke(Theme.color(.backgroundBorder), lineWidth: 1.0)
            )
        }
        
        func deleteAlert(
            for member: ShareGroup.Member,
            onDelete: @escaping () -> Void
        ) -> Alert {
            Alert(
                title: Text("\(edit.member.name)"),
                message: Text("Are you sure you want to delete this user?"),
                primaryButton: .cancel(),
                secondaryButton: .destructive(Text("Delete"), action: onDelete)
            )
        }
    }
}

extension ShareGroup {
    func isOutgoing(_ member: Member) -> Bool {
        outgoingInvites
            .contains(where: { $0.email.lowercased() == member.email.lowercased() })
    }
}

#if DEBUG

struct SettingsPrivacyView_Previews: PreviewProvider {
    static let reflectionState = ReflectionsState(
        userID: "100",
        selectedFilter: .reflections,
        sections: [],
        reflections: [:],
        shareGroup: .mock
    )
    
    static var previews: some View {
        NavigationView {
            SettingsPrivacyView()
                .background(Theme.color(.background))
        }
        .background(Theme.color(.background))
        .environmentObject(ReflectionsStore(reflectionState))
        .environment(\.colorScheme, .light)
    }
}

#endif
