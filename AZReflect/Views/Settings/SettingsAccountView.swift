//
//  SettingsAccountView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 12/8/21.
//

import SwiftUI

struct SettingsAccountView: View {
    @EnvironmentObject private var userStore: UserStore
    @Environment(\.homeNavigationRoot) private var homeNavigationRoot
    @State private var isLogOutPresented = false
    
    var body: some View {
        List {
            VStack {
                if userStore.user != .none {
                    SignOutButton(action: { isLogOutPresented = true })
                }
            }
        }
        .alert(
            isPresented: $isLogOutPresented,
            content: {
                Alert(
                    title: Text("Sign Out"),
                    message: Text("Are you sure you want to sign out?"),
                    primaryButton: .cancel(),
                    secondaryButton: .destructive(Text("Sign Out"), action: logOut)
                )
            }
        )
        .navigationTitle("Account")
    }
    
    private func logOut() {
        userStore.logout()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
            homeNavigationRoot.wrappedValue.dismiss()
        }
    }
}

extension SettingsAccountView {
    struct SignOutButton: View {
        let action: () -> Void
        
        var body: some View {
            Button(
                action: action,
                label: {
                    Label("Sign Out", systemImage: "rectangle.portrait.and.arrow.right")
                        .foregroundColor(Theme.color(.alert))
                }
            )
        }
    }
}

#if DEBUG

struct SettingsAccountView_Previews: PreviewProvider {
    static let previewUser = User(
        id: "100",
        name: "Wendell",
        email: "wendell@az.com",
        isOnboarded: true
    )
    
    static let previewState: UserState = .loggedIn(previewUser)
    
    static var previews: some View {
        NavigationView {
            SettingsAccountView()
                .environmentObject(UserStore(previewState))
        }
    }
}

#endif
