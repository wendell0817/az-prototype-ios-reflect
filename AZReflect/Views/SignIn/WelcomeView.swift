//
//  WelcomeView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import SwiftUI

struct WelcomeView: View {
    var body: some View {
        NavigationView {
            VStack {
                GeometryReader { proxy in
                    VStack(spacing: 0.0) {
                        ReflectLogoView(.white)
                            .frame(height: 60.0)
                        
                        AZLogoView()
                            .accentColor(.white)
                            .padding(.top, 20.0)
                        
                        Text("We’re here to take away the stress of talking about your health.")
                            .themeFont(.body)
                            .foregroundColor(.white)
                            .multilineTextAlignment(.center)
                            .padding(.top, 56.0)
                    }
                    .frame(size: proxy.size)
                }
                
                GeometryReader { proxy in
                    HStack(spacing: 16.0) {
                        NavigationLink("Create Profile", destination: NewProfileView())
                            .themeButtonStyle(.standardLight)
                            .shadow(color: Color.black.opacity(0.25), radius: 4.0)
                        
                        NavigationLink("Sign In", destination: SignInView())
                            .themeButtonStyle(.cta)
                            .shadow(color: Color.black.opacity(0.25), radius: 4.0)
                    }
                    .padding(.bottom, 32.0)
                    .frame(size: proxy.size, alignment: .bottom)
                }
            }
            .padding(.horizontal, 32.0)
            .background(
                Rectangle()
                    .themeModifier(.reflectGradient)
                    .overlay(
                        Image("onboarding.intro.background")
                            .resizable()
                            .scaledToFill()
                            .blendMode(.multiply)
                    )
                    .ignoresSafeArea()
            )
            .navigationBarHidden(true)
        }
    }
}

#if DEBUG

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
            .environment(\.colorScheme, .light)
    }
}

#endif
