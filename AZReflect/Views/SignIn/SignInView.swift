//
//  SignInView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/5/21.
//

import SwiftUI

struct SignInView: View {
    @EnvironmentObject private var userStore: UserStore
    @Environment(\.presentationMode) private var presentationMode
    @State private var formValues = FormValues()
    
    private var showError: Bool {
        switch userStore.state {
        case .logInFailed:
            return true
        default:
            return false
        }
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0.0) {
            BackNavigationView()
                .padding(.top, 20.0)
            
            VStack(alignment: .leading, spacing: 8.0) {
                Text("Great to see you!")
                    .themeFont(.titleBold)
                
                Text("Let’s get you signed in.")
                    .foregroundColor(Theme.color(.textCaption))
                    .themeFont(.body)
            }
            .padding(.top, 32.0)
            
            VStack(alignment: .leading, spacing: 28.0) {
                FieldView(
                    prompt: "User Email",
                    title: "Add email",
                    text: $formValues.email,
                    isValid: formValues.isEmailValid
                )
                .textContentType(.emailAddress)
                .keyboardType(.emailAddress)
                .autocapitalization(.none)
                
                FieldView(
                    isSecure: true,
                    prompt: "Password",
                    title: "Add password",
                    text: $formValues.password,
                    isValid: formValues.isPasswordValid
                )
                .textContentType(.password)
                .keyboardType(.default)
                .autocapitalization(.none)
            }
            .padding(.top, 40.0)
            
            VStack(alignment: .leading, spacing: 8.0) {
                Text("Either the email address or password is incorrect.  Please try again")
                    .themeFont(.body)
                    .foregroundColor(Theme.color(.alert))
                
                Button("Need Help?", action: {})
                    .buttonStyle(PlainButtonStyle())
            }
            .fixedSize(horizontal: false, vertical: true)
            .padding(.top, 24.0)
            .opacity(showError ? 1.0 : 0.0)
            .frame(height: showError ? .none : 0.0)
            .animation(.easeInOut, value: userStore.state)
            
            Spacer()
            
            HStack {
                Spacer()
                
                Button(
                    action: { userStore.signIn(formValues.email, password: formValues.password) },
                    label: { Image(systemName: "checkmark") }
                )
                .themeButtonStyle(.ctaCircle)
                .scaleEffect(formValues.isValid ? 1.0 : 0.01, anchor: .center)
                .animation(.spring(), value: formValues)
            }
            .padding(.bottom, 40.0)
        }
        .onChange(of: userStore.state) { userState in
            switch userState {
            case .loggedIn:
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                    presentationMode.wrappedValue.dismiss()
                }
            default:
                break
            }
        }
        .navigationBarHidden(true)
        .padding(.horizontal, 32.00)
        .background(Theme.color(.background).ignoresSafeArea())
    }
}

extension SignInView {
    struct FormValues: Equatable {
        var email = ""
        var password = ""
        
        var isEmailValid: Bool {
            email.isEmail
        }
        
        var isPasswordValid: Bool {
            password.count >= 3
        }
        
        var isValid: Bool {
            isEmailValid && isPasswordValid
        }
    }
}

#if DEBUG

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
            .environmentObject(UserStore())
    }
}

#endif
