//
//  OnboardingView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/3/21.
//

import SwiftUI

struct OnboardingView: View {
    @EnvironmentObject private var userStore: UserStore
    @Environment(\.presentationMode) private var presentationMode
    
    let steps: [OnboardingView.Step] = [
        .init(
            id: 1,
            title: "First, we’ll check your health metrics.",
            prompt: "Simply look at your phone, and your health data will start to appear.",
            imageName: "onboarding.metrics"
        ),
        .init(
            id: 2,
            title: "Then, you’ll tell us how you’re doing.",
            prompt: "You can just talk normally. We’ll highlight any symptoms you share.",
            imageName: "onboarding.reflections"
        ),
        .init(
            id: 3,
            title: "You can then share with those who care.",
            prompt: "You have full control over who sees what, if anything.",
            imageName: "onboarding.sharing"
        )
    ]
    
    @State private var selectedStepId: Int = 1
    private let navIconTopPadding: CGFloat = 175.0
    
    private var selectedIndex: Int? {
        steps.firstIndex { $0.id == selectedStepId }
    }
    
    var canGoBack: Bool {
        switch selectedIndex {
        case .some(let index):
            return index > 0
        default:
            return false
        }
    }
    
    var isCompleted: Bool {
        switch selectedIndex {
        case .some(let index):
            return index == (steps.count - 1)
        default:
            return false
        }
    }
    
    var body: some View {
        VStack(spacing: 0.0) {
            ReflectLogoView(Theme.color(.backgroundInverse), style: .small)
                .frame(height: 52.0, alignment: .bottom)
                .padding(.bottom)
            
            HStack {
                Button(
                    action: { withAnimation { navigateBack() } },
                    label: {
                        Image(systemName: "arrow.left")
                            .foregroundColor(Theme.color(.backgroundInverse))
                            .themeFont(.title)
                    }
                )
                .opacity(canGoBack ? 1.0 : 0.01)
                .padding(.top, navIconTopPadding)
                
                TabView(selection: $selectedStepId) {
                    ForEach(steps) { step in
                        OnboardingView.StepContentView(
                            step: step
                        )
                        .tag(step.id)
                        .padding(.top, 16.0)
                    }
                }
                .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
                
                Button(
                    action: { withAnimation { navigateForward() } },
                    label: {
                        Image(systemName: isCompleted ? "checkmark" : "arrow.right")
                            .foregroundColor(isCompleted ? .white : Theme.color(.backgroundInverse))
                            .font(.system(size: 24.0))
                    }
                )
                .padding(.top, navIconTopPadding)
            }
            
            Text("Tap to Pause")
                .foregroundColor(Theme.color(.textCaption))
                .themeFont(.subtitle)
                .multilineTextAlignment(.center)
                .padding(.bottom, 40.0)
        }
        .onChange(of: userStore.state) { userState in
            switch userState {
            case .loggedIn:
                presentationMode.wrappedValue.dismiss()
            default:
                break
            }
        }
        .padding(.horizontal, 40.0)
        .background(
            Image("onboarding.confirm.background")
                .scaledToFit()
                .padding(.leading, 190.0)
                .padding(.top, 180.0)
                .opacity(isCompleted ? 1.0 : 0.0)
        )
        .background(Theme.color(.backgroundInstruction).ignoresSafeArea())
    }
    
    private func navigateBack() {
        switch selectedIndex {
        case .some(let index):
            let targetIndex = index - 1
            guard steps.indices.contains(targetIndex) else { return }
            selectedStepId = steps[targetIndex].id
        default:
            break
        }
    }

    private func navigateForward() {
        switch selectedIndex {
        case .some(let index):
            let targetIndex = index + 1
            guard steps.indices.contains(targetIndex) else {
                if isCompleted {
                    userStore.onBoardingCompleted()
                }
                return
            }
            selectedStepId = steps[targetIndex].id
        default:
            break
        }
    }

}

extension OnboardingView {
    // MARK: StepContentView
    struct StepContentView: View {
        let step: OnboardingView.Step
        
        var body: some View {
            VStack(spacing: 0.0) {
                Text(step.title)
                    .themeFont(.titleBold)
                    .multilineTextAlignment(.center)
                    .padding(.bottom, 12.0)
                
                Text(step.prompt)
                    .foregroundColor(Theme.color(.textCaption))
                    .themeFont(.subtitle)
                    .multilineTextAlignment(.center)
                    .padding(.bottom, 40.0)
            
                Image(step.imageName)
                    .resizable()
                    .scaledToFit()
            }
        }
    }
    
    // MARK: PagingView
    struct PagingView: View {
        let selectedIndex: Int?
        let count: Int
        
        var body: some View {
            HStack {
                Spacer()
                
                HStack {
                    ForEach(0 ..< count, id: \.self) { index in
                        Circle()
                            .stroke(Theme.color(.textCaption), lineWidth: 1.0)
                            .frame(width: 8.0, height: 8.0)
                            .overlay(
                                Circle().fill(Theme.color(.textCaption))
                                    .opacity(selectedIndex == index ? 1.0 : 0.0)
                            )
                    }
                }
                
                Spacer()
            }
            .animation(.spring(), value: selectedIndex)
        }
    }
}

extension OnboardingView {
    struct Step: Identifiable {
        let id: Int
        let title: String
        let prompt: String
        let imageName: String
    }
}

// MARK: Previews

#if DEBUG

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
            .environmentObject(UserStore())
    }
}

#endif
