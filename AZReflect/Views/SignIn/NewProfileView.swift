//
//  NewProfileView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import SwiftUI

struct NewProfileView: View {
    @EnvironmentObject private var userStore: UserStore
    @Environment(\.presentationMode) private var presentationMode
    @State private var formValues = FormValues()
    @State private var isOnboardingPresented = false
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0.0) {
            BackNavigationView()
                .padding(.top, 20.0)
            
            VStack(alignment: .leading) {
                Text("Lets get started!")
                    .themeFont(.titleBold)
                Text("First we’ll just need some supporting info:")
                    .themeFont(.body)
                    .foregroundColor(Theme.color(.textCaption))
            }
            .padding(.vertical, 32.0)
            
            VStack(alignment: .leading, spacing: 28.0) {
                FieldView(
                    prompt: "What should we call you?",
                    title: "Add your preferred name",
                    text: $formValues.name,
                    isValid: !formValues.name.isEmpty
                )
                .textContentType(.givenName)
                .keyboardType(.default)
                
                FieldView(
                    prompt: "What is your email?",
                    title: "Add your email",
                    text: $formValues.email,
                    isValid: formValues.email.isEmail
                )
                .textContentType(.emailAddress)
                .keyboardType(.emailAddress)
                .autocapitalization(.none)
                
                FieldView(
                    prompt: "What is your patient portal?",
                    title: "Add your patient portal link",
                    text: $formValues.url,
                    isValid: formValues.url.isURL
                )
                .textContentType(.URL)
                .keyboardType(.URL)
                .autocapitalization(.none)
                
                Text("Need Help?")
                    .themeFont(.caption)
                    .foregroundColor(Theme.color(.textCaption))
            }
            .navigationLink(
                destination: OnboardingView(),
                isActive: $isOnboardingPresented
            )
            
            Spacer()
            
            HStack {
                Spacer()
                
                Button(
                    action: {
                        userStore.createUser(for: formValues.name, email: formValues.email)
                        isOnboardingPresented = true
                    },
                    label: { Image(systemName: "checkmark") }
                )
                .themeButtonStyle(.ctaCircle)
                .opacity(formValues.isValid ? 1.0 : 0.0)
                .animation(.spring(), value: formValues)
            }
            .padding(.bottom, 40.0)
        }
        .onChange(of: userStore.state) { userState in
            switch userState {
            case .loggedIn:
                presentationMode.wrappedValue.dismiss()
            default:
                break
            }
        }
        .navigationBarHidden(true)
        .padding(.horizontal, 32.00)
        .background(Theme.color(.background).ignoresSafeArea())
    }
}

extension NewProfileView {
    struct FormValues: Equatable {
        var name = ""
        var email = ""
        var url = ""
        
        var isValid: Bool {
            !name.isEmpty && email.isEmail
        }
    }
}

#if DEBUG

struct NewProfileView_Previews: PreviewProvider {
    static var previews: some View {
        NewProfileView().environment(\.colorScheme, .light)
            .environmentObject(UserStore())
    }
}

#endif
