//
//  AZReflectApp.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//  Test

import SwiftUI

@main
struct AZReflectApp: App {
    @StateObject private var userStore = UserStore()
    @StateObject private var reflectionsStore = ReflectionsStore()
    
    var body: some Scene {
        WindowGroup {
            AppView()
                .environmentObject(userStore)
                .environmentObject(reflectionsStore)
                .onAppear {
                    setLogLevel()
                    reflectionsStore.subscribe(to: userStore)
                }
        }
    }
    
    private func setLogLevel() {
        #if DEBUG
        Log.level = .verbose
        #else
        Log.level = .debug
        #endif
    }
}

