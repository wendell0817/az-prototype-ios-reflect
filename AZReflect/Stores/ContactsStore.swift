//
//  ContactsStore.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 12/7/21.
//

import Foundation
import Contacts

// MARK: State

struct ContactsState {
    var contacts: [ContactsState.Contact] = []
}

extension ContactsState {
    struct Contact: Identifiable, Equatable {
        let id: String
        let image: UIImage?
        let givenName: String
        let familyName: String
        let email: String?
        
        init(
            id: String,
            imageData: Data?,
            givenName: String,
            familyName: String,
            email: String
        ) {
            self.id = id
            self.givenName = givenName
            self.familyName = familyName
            self.email = email
            
            switch imageData {
            case .some(let data):
                self.image = UIImage(data: data)
            default:
                self.image = .none
            }
        }
        
        init(_ cnContact: CNContact, emailLabel: String? = .none) {
            self.id = cnContact.identifier
            self.givenName = cnContact.givenName
            self.familyName = cnContact.familyName
            
            let emailLabel = cnContact.emailAddresses
                .first(where: { $0.label == emailLabel })
                    ?? cnContact.emailAddresses.first
            
            switch emailLabel {
            case .some(let email):
                self.email = String(email.value)
            default:
                self.email = .none
            }
            
            switch cnContact.imageData {
            case .some(let data):
                self.image = UIImage(data: data)
            default:
                self.image = .none
            }
        }
    }
}

// MARK: Environment

struct ContactsEnvironment {
    let cnContactStore = CNContactStore()
    let contactsService = ContactsService()
}

// MARK: Store

typealias ContactsStore = Store<ContactsState, ContactsEnvironment>

extension ContactsStore {
    convenience init(_ state: ContactsState = .init()) {
        self.init(
            state,
            env: .init(),
            mapError: { error in
                return .none
            }
        )
    }
    
    func loadContacts(for searchString: String) {
        receive(.dataTask(loadContactsTask, searchString, 0.5, "search"))
    }
    
    private func loadContactsTask(for searchString: String) async throws -> Effect {
        let contacts = try await env.contactsService.searchContacts(for: searchString)
            .compactMap { ContactsState.Contact($0) }
        return .set { $0.contacts = contacts }
    }
}
