//
//  ReflectionsStore.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 12/2/21.
//

import Foundation
import Combine

// MARK: State

struct ReflectionsState {
    var userID = ""
    var selectedFilter: ReflectionFilter = .reflections
    var sections: [Section] = []
    var reflections: [String: Reflection] = [:]
    var shareGroup: ShareGroup = .empty
}

extension ReflectionsState {
    enum ReflectionFilter: CaseIterable {
        case reflections
        case highlights
    }
    
    enum SectionType: Int {
        case checkIn
        case reflections
        case trends
    }
    
    enum Item: Identifiable, Equatable {
        case checkIn
        case reflectionFilter
        case reflection(Reflection)
        case highlight(String)
        case trend(String)
        
        var id: String {
            switch self {
            case .checkIn: return "checkIn"
            case .reflectionFilter: return "reflectionFilter"
            case .reflection(let reflection): return reflection.id
            case .highlight(let value): return value
            case .trend(let value): return value
            }
        }
    }
    
    struct Section: Identifiable, Equatable {
        let type: SectionType
        let items: [Item]
        
        var id: Int { type.rawValue }
    }
    
    struct ReflectionEdit: Equatable {
        let id: String
        var title: String
        var symptoms: [Symptom]
        
        init(_ reflection: Reflection) {
            self.id = reflection.id
            self.title = reflection.title
            self.symptoms = reflection.symptoms
        }
        
        mutating func remove(symptom: Symptom) {
            guard let index = symptoms.firstIndex(of: symptom) else { return }
            symptoms.remove(at: index)
        }
    }
    
    struct ReflectionShareEdit: Identifiable {
        let member: ShareGroup.Member
        var isOn: Bool
        var id: String { member.id }
        
        init(member: ShareGroup.Member) {
            self.member = member
            self.isOn = true
        }
        
        init(member: ShareGroup.Member, isOn: Bool) {
            self.member = member
            self.isOn = isOn
        }
    }
    
    struct MemberPrivacyEdit: Identifiable, Equatable {
        var id: String { member.id }
        let member: ShareGroup.Member
        var shareVideo: Bool
        var shareMetrics: Bool
        var shareTrancript: Bool
        
        init(member: ShareGroup.Member) {
            self.member = member
            self.shareVideo = true
            self.shareMetrics = true
            self.shareTrancript = true
        }
    }
    
    struct UploadData: Equatable {
        let id: String
        let videoURL: URL
        let audioURL: URL
    }
}

// MARK: Environment

struct ReflectionsEnvironment {
    enum CancelIds: CaseIterable, Hashable {
        case captureCompleted
        case assetsReady
    }
    
    let networkService = NetworkService()
}

// MARK: Store

typealias ReflectionsStore = Store<ReflectionsState, ReflectionsEnvironment>

extension ReflectionsStore {
    
    // Init
    
    convenience init(_ state: ReflectionsState = .init()) {
        self.init(
            state,
            env: .init(),
            mapError: { error in
                Log.error(error)
                return .none
            }
        )
    }
    
    // Public API
    
    func subscribe(to userStore: UserStore) {
        subscribe(
            to: userStore,
            on: \.self,
            id: "UserState",
            mapEffect: mapUserState
        )
    }
    
    func unsubscribe(from userStore: UserStore) {
        receive(.cancel("UserState"))
    }
    
    func subscribeToCaptureState(_ publisher: AnyPublisher<CaptureState, Never>) {
        subscribe(
            to: publisher
                .filter { $0.stage == .completed }
                .removeDuplicates(by: { $0.captureId == $1.captureId })
                .log(.debug, prefix: "[ReflectionsStore] creating reflection")
                .eraseToAnyPublisher(),
            id: ReflectionsEnvironment.CancelIds.captureCompleted,
            mapEffect: mapCaptureState
        )
        
        subscribe(
            to: publisher
                .compactMap(\.uploadData)
                .removeDuplicates()
                .log(.debug, prefix: "[ReflectionsStore] moving assets")
                .eraseToAnyPublisher(),
            id: ReflectionsEnvironment.CancelIds.assetsReady,
            mapEffect: mapUploadData
        )
    }
    
    func unsubscribeFromCaptureState() {
        receive(
            .merge(
                ReflectionsEnvironment.CancelIds.allCases
                    .map(Effect.cancel)
            )
        )
    }
    
    func loadReflections() {
        receive(.task(loadReflectionsTask))
    }
    
    func loadSections(for filter: ReflectionsState.ReflectionFilter) {
        receive(
            .concatenate(
                .set { $0.selectedFilter = filter },
                .task(loadSectionsTask)
            )
        )
    }
    
    func delete(by id: String) {
        receive(.dataTask(deleteReflection, id, 0.5))
    }
    
    func apply(_ edit: ReflectionsState.ReflectionEdit) {
        receive(.dataTask(applyEditTask, edit))
    }
    
    func loadGroup() {
        receive(.task(loadGroupTask))
    }
    
    func addShareMember(with email: String) {
        receive(.dataTask(addShareMemberTask, email))
    }
    
    func removeShareMember(_ member: ShareGroup.Member) {
        receive(.dataTask(removeShareMemberTask, member))
    }
        
    // Internal API
    
    private func loadReflectionsTask() async throws -> Effect {
        guard !state.userID.isEmpty else {
            return .set { $0.reflections = [:] }
        }
        
        let feed: Feed = try env.networkService.get(.feed(userID: state.userID))
        return .concatenate(
            .set { $0.reflections = feed.reflections },
            .task(loadSectionsTask)
        )
    }
    
    private func loadSectionsTask() async throws -> Effect {
        var sections: [ReflectionsState.Section] = [.init(type: .checkIn, items: [.checkIn]) ]
        
        switch state.selectedFilter {
        case .reflections:
            let sortedReflectionItems = Array(state.reflections.values)
                .sorted(by: \.date, >)
                .map(ReflectionsState.Item.reflection)
            
            sections.append(
                .init(
                    type: .reflections,
                    items: [.reflectionFilter] + sortedReflectionItems
                )
            )
        default:
            sections.append(
                .init(
                    type: .reflections,
                    items: [.reflectionFilter, .trend("Coming Soon")]
                )
            )
        }
        
        sections.append(.init(type: .trends, items: [.trend("Coming Soon")]))
        
        return .set { $0.sections = sections }
    }
    
    private func createReflectionTask(_ captureState: CaptureState) async throws -> Effect {
        let newReflection = Reflection(
            id: captureState.captureId,
            date: Date(),
            metrics: .init(
                heartRate: captureState.metrics.heartRates.average,
                oxygenSaturation: captureState.metrics.oxygenSaturations.average,
                breathingRate: captureState.metrics.breathingRates.average
            ),
            transcription: captureState.transcript,
            symptoms: captureState.symptoms,
            heartRateVariability: captureState.metrics.report?.standardDeviationNN,
            stressLevel: Reflection.StressLevel(value: captureState.metrics.report?.stressLevel ?? 0)
        )
        
        let status = try env.networkService
            .status(.create(reflection: newReflection, userID: state.userID))
        
        switch status {
        case 200 ... 299:
            return .concatenate(
                .set { $0.reflections[newReflection.id] = newReflection },
                .task(loadSectionsTask)
            )
        default:
            Log.error("[ReflectionsStore] failed to updload reflection. status:\(status)")
            return .none
        }
    }
    
    private func deleteReflection(for id: String) async throws -> Effect {
        let status = try env.networkService.status(.delete(reflectionID: id, userID: state.userID))
        switch status {
        case 200 ... 299:
            return .concatenate(
                .set { $0.reflections[id] = .none },
                .task(loadSectionsTask)
            )
        default:
            return .none
        }
    }
    
    private func applyEditTask(_ edit: ReflectionsState.ReflectionEdit) async throws -> Effect {
        guard let existingReflection = state.reflections[edit.id] else { return .none }
        let updatedReflection = existingReflection.with(title: edit.title, symptoms: edit.symptoms)
        let updateQuery = NetworkService.StatusQuery.update(reflection: updatedReflection, userID: state.userID)
        let status = try env.networkService.status(updateQuery)
        switch status {
        case 200 ... 299:
            return .concatenate(
                .set { $0.reflections[updatedReflection.id] = updatedReflection },
                .task(loadSectionsTask)
            )
        default:
            Log.error("[ReflectionsStore] failed to delete reflection. status:\(status)")
            return .none
        }
    }
    
    private func loadGroupTask() async throws -> Effect {
        let group: ShareGroup = try env.networkService.get(.group(userID: state.userID))
        return .set { $0.shareGroup = group }
    }
    
    private func addShareMemberTask(for email: String) async throws -> Effect {
        let statusQuery = NetworkService.StatusQuery.invite(newMemberEmail: email, userID: state.userID)
        let status = try env.networkService.status(statusQuery)
        switch status {
        case 200 ... 299:
            return .task(loadGroupTask)
        default:
            return .none
        }
    }
    
    private func removeShareMemberTask(_ member: ShareGroup.Member) async throws -> Effect {
        let statusQuery = NetworkService.StatusQuery.removeGroupMember(id: member.id, userID: state.userID)
        let status = try env.networkService.status(statusQuery)
        switch status {
        case 200 ... 299:
            return .task(loadGroupTask)
        default:
            return .none
        }
    }
    
    private func uploadTask(_ mergeData: ReflectionsState.UploadData) async throws -> Effect {
        let uploadQuery = NetworkService.GetQuery.upload(
            videoURL: mergeData.videoURL,
            audioURL: mergeData.audioURL,
            reflectionID: mergeData.id,
            userID: state.userID
        )
        let updatedReflection: Reflection = try env.networkService.get(uploadQuery)
        return .concatenate(
            .set({ $0.reflections[updatedReflection.id] = updatedReflection }),
            .task(loadSectionsTask)
        )
    }
}

// MARK: Utility Funcs

extension ReflectionsStore {
    func reflection(for id: String) -> Reflection? {
        state.reflections[id]
    }
    
    func videoURL(for reflection: Reflection) -> URL? {
        env.networkService.videoURL(for: reflection)
    }
    
    private func mapUserState(_ userState: UserState) -> Effect {
        .concatenate(
            .set { $0.userID = userState.user?.id ?? "" },
            .task(loadReflectionsTask)
        )
    }
    
    private func mapCaptureState(_ captureState: CaptureState) -> Effect {
        return .dataTask(createReflectionTask, captureState)
    }
    
    private func mapUploadData(_ uploadData: ReflectionsState.UploadData) -> Effect {
        return .dataTask(uploadTask, uploadData)
    }
}

extension CaptureState {
    var uploadData: ReflectionsState.UploadData? {
        switch (videoCaptureVideoURL, videoCaptureAudioURL) {
        case (.some(let videoURL), .some(let audioURL)):
            return .init(id: captureId, videoURL: videoURL, audioURL: audioURL)
        default:
            return .none
        }
    }
}
