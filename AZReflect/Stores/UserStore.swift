//
//  UserStore.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 12/2/21.
//

import Foundation

enum UserState: Equatable {
    case none
    case notLoggedIn
    case onBoarding
    case logginIn
    case logInFailed
    case loggedIn(User)
    
    var user: User? {
        switch self {
        case .loggedIn(let user):
            return user
        default:
            return .none
        }
    }
}

struct UserEnvironment {
    let fileService = FileService()
    let networkService = NetworkService()
    let userFileName = "com.AstraZeneca.AZReflect.user.json"
}

typealias UserStore = Store<UserState, UserEnvironment>

extension UserStore {
    convenience init(_ state: UserState = .none) {
        self.init(
            state,
            env: .init(),
            mapError: { error in
                Log.error(error)
                return .none
            }
        )
    }
    
    // MARK: Public API
    
    func loadUser() {
        receive(.task(loadUserTask))
    }
    
    func signIn(_ emailAddress: String, password: String) {
        receive(.dataTask(signInTask, (emailAddress, password)))
    }
    
    func createUser(for name: String, email: String) {
        receive(.dataTask(createUserTask, (name, email)))
    }
    
    func onBoardingCompleted() {
        receive(.task(onBoardingCompletedTask))
    }
    
    func logout() {
        receive(.task(logoutTask))
    }
    
    // MARK: Internal API
    
    private func loadUserTask() async throws -> Effect {
        do {
            let user: User = try env.fileService.read(from: env.userFileName)
            return .set { $0 = .loggedIn(user) }
        } catch let error {
            Log.warning("[UserStore] \(error)")
            return .set { $0 = .notLoggedIn }
        }
    }
    
    private func signInTask(_ credentials: (email: String, password: String)) async throws -> Effect {
        do {
            let query = NetworkService.GetQuery.login(
                email: credentials.email,
                password: credentials.password
            )
            
            let user: User = try env.networkService.get(query)
            
            return .merge(
                .set { $0 = .loggedIn(user) },
                .dataTask(storeTask, user)
            )
        } catch let error {
            Log.warning("[UserStore] \(error)")
            return .set { $0 = .logInFailed }
        }
    }
    
    private func createUserTask(_ credentials: (name: String, email: String)) async throws -> Effect {
        let newUser = User(name: credentials.name, email: credentials.email)
        return .merge(
            .set { $0 = .onBoarding },
            .dataTask(storeTask, newUser)
        )
    }
    
    private func onBoardingCompletedTask() async throws -> Effect {
        let user: User = try env.fileService.read(from: env.userFileName)
        let onboardedUser = user.onboarded()
        return .set { $0 = .loggedIn(onboardedUser) }
    }
    
    private func logoutTask() async throws -> Effect {
        return .merge(
            .set { $0 = .notLoggedIn },
            .task(deleteUserTask)
        )
    }
    
    private func storeTask(_ user: User) async throws -> Effect {
        try env.fileService.write(user, to: env.userFileName)
        return .none
    }
    
    private func deleteUserTask() async throws -> Effect {
        try env.fileService.delete(env.userFileName)
        return .none
    }
}
