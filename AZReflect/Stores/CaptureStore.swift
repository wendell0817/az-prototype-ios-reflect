//
//  CaptureStore.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 12/2/21.
//

import Foundation

// MARK: State

struct CaptureState {
    var stage: CaptureStage = .ready
    var captureStages: [CaptureStage] = []
    var captureId = ""
    var countdown: UInt = 0
    var metricsImage: MetricsService.Image? = .none
    var metrics: MetricsService.Metrics = .init()
    var metricsDisplay: MetricsDisplay = .init()
    var metricStartDate = Date()
    var metricCaptureTime: TimeInterval = 0.0
    var metricCapturePercent = 0.0
    var metricsCaptureCompleted = false
    var videoStartDate = Date()
    var videoCaptureTime: TimeInterval = 0.0
    var videoCapturePercent = 0.0
    var videoCaptureCompleted = false
    var videoCaptureVideoURL: URL? = .none
    var videoCaptureAudioURL: URL? = .none
    var transcript: String = ""
    var symptoms: [Symptom] = []
}

extension CaptureState {
    enum CaptureStage: Int, Equatable {
        case ready
        case metricsCountdown
        case metricsCapture
        case videoCountdown
        case videoCapture
        case completed
    }
    
    struct MetricsDisplay: Equatable {
        var heartRate: Double? = .none
        var oxygenSaturation: Double? = .none
        var breathingRate: Double? = .none
    }
}

// MARK: Environment

struct CaptureEnvironment {
    let maxMetricCaptureTime: TimeInterval = 60.0
    let maxVideoCaptureTime: TimeInterval = 60.0
    let metricsService = MetricsService()
    let videoService = VideoCaptureService()
    let transcriptionService = TranscriptionService()
    let symptomDetector = SymptomDetectionService()
    let timerTaskId = "capture.timer"
}

// MARK: Store

typealias CaptureStore = Store<CaptureState, CaptureEnvironment>

extension CaptureStore {
    
    // MARK: Public API
    
    convenience init(_ state: CaptureState = .init()) {
        self.init(
            state,
            env: .init(),
            mapError: { error in
                return .none
            }
        )
        
        subscribe(
            to: env.metricsService.imagePublisher,
            id: "metricsService.imagePublisher",
            mapEffect: { image in .set { $0.metricsImage = image } }
        )
        
        subscribe(
            to: env.metricsService.metricsPublisher,
            id: "metricsService.metricsPublisher",
            mapEffect: mapMetrics
        )
        
        subscribe(
            to: env.transcriptionService.speechPublisher,
            id: "transcriptionService.speechPublisher",
            mapEffect: mapTranscript
        )
        
        subscribe(
            to: env.videoService.outputPublisher,
            id: "videoService.outputPublisher",
            mapEffect: { url in .set { $0.videoCaptureVideoURL = url } }
        )
        
        subscribe(
            to: env.transcriptionService.outputPublisher,
            id: "transcriptionService.outputPublisher",
            mapEffect: { url in .set { $0.videoCaptureAudioURL = url } }
        )
    }
    
    // MARK: Public API
    
    func setFlow(_ flow: Settings.CaptureFlow) {
        receive(.set({ $0.captureStages = flow.stages }))
    }
    
    func nextStage() {
        receive(.task(advanceToNextStageTask))
    }
    
    func reset() {
        receive(.task(resetTask))
    }

    // MARK: Internal API
    
    private func buildMetricsDisplayTask() async throws -> Effect {
        let metricsDisplay = CaptureState.MetricsDisplay(
            heartRate: state.metrics.heartRates.average,
            oxygenSaturation: state.metrics.oxygenSaturations.average,
            breathingRate: state.metrics.breathingRates.average
        )

        return .set { $0.metricsDisplay = metricsDisplay }
    }
    
    private func detectSymptomsTask() async throws -> Effect {
        let symptoms = env.symptomDetector.detectSymptoms(in: state.transcript)
        return .set { $0.symptoms = symptoms }
    }
    
    private func advanceToNextStageTask() async throws -> Effect {
        guard let index = state.captureStages.firstIndex(of: state.stage) else { return .none }
        let targetIndex = index + 1
        guard state.captureStages.indices.contains(targetIndex) else { return .none }
        let nextStage = state.captureStages[targetIndex]
        
        let previousStage = state.stage
        var updatedState = state
        updatedState.stage = nextStage
        var sideEffect: Effect = .none
        
        switch previousStage {
        case .ready:
            updatedState.captureId = UUID().uuidString
        case .metricsCapture:
            env.metricsService.stopRecording()
        case .videoCapture:
            env.videoService.stopRecording()
            env.transcriptionService.stopRecording()
            updatedState.videoCaptureCompleted = true
            sideEffect = .cancel(env.timerTaskId)
        default:
            break
        }
        
        switch nextStage {
        case .metricsCountdown:
            updatedState.countdown = 3
            sideEffect = .timer(env.timerTaskId, interval: 1.0, mapEffect: timerTick)
        case .metricsCapture:
            updatedState.metricStartDate = Date()
            updatedState.metricCaptureTime = 0.0
            env.metricsService.startRecording(for: env.maxMetricCaptureTime)
            sideEffect = .timer(env.timerTaskId, interval: 1.0, mapEffect: timerTick)
        case .videoCountdown:
            updatedState.countdown = 3
            sideEffect = .timer(env.timerTaskId, interval: 1.0, mapEffect: timerTick)
        case .videoCapture:
            updatedState.videoStartDate = Date()
            updatedState.videoCaptureTime = 0.0
            env.videoService.startRecording(to: state.captureId)
            env.transcriptionService.startRecording(to: state.captureId)
            sideEffect = .timer(env.timerTaskId, interval: 1.0, mapEffect: timerTick)
        default:
            break
        }
        
        return .merge(
            .set { state in
                state.stage = updatedState.stage
                state.captureId = updatedState.captureId
                state.videoCaptureCompleted = updatedState.videoCaptureCompleted
                state.countdown = updatedState.countdown
                state.metricStartDate = updatedState.metricStartDate
                state.metricCaptureTime = updatedState.metricCaptureTime
                state.videoStartDate = updatedState.videoStartDate
                state.videoCaptureTime = updatedState.videoCaptureTime
            },
            sideEffect
        )
    }
    
    private func resetTask() async throws -> Effect {
        let stages = state.captureStages
        var newState = CaptureState()
        newState.captureStages = stages
        newState.stage = .ready
        return .set { $0 = newState }
    }
}

extension CaptureStore {
    var videoSession: AVCaptureSession {
        env.videoService.session
    }
    
    private func mapMetrics(_ metrics: MetricsService.Metrics) -> Effect {
        return .concatenate(
            .set { $0.metrics = metrics },
            .task(buildMetricsDisplayTask)
        )
    }
    
    private func mapTranscript(_ transcript: String) -> Effect {
        return .concatenate(
            .set { $0.transcript = transcript },
            .task(detectSymptomsTask)
        )
    }
    
    private func timerTick(_ date: Date) -> Effect {
        switch state.stage {
        case .metricsCountdown,
             .videoCountdown:
            let nextCount = state.countdown - 1
            switch nextCount {
            case 0:
                return .concatenate(
                    .cancel(env.timerTaskId),
                    .task(advanceToNextStageTask)
                )
            default:
                return .set { $0.countdown = nextCount }
            }
        case .metricsCapture:
            let time = date.timeIntervalSince(state.metricStartDate)
            switch time {
            case (0 ..< env.maxMetricCaptureTime):
                let maxTime = env.maxMetricCaptureTime
                return .set {
                    $0.metricCaptureTime = time
                    $0.metricCapturePercent = time / maxTime
                }
                
            default:
                return .concatenate(
                    .set { $0.metricsCaptureCompleted = true },
                    .cancel(env.timerTaskId),
                    .task(advanceToNextStageTask)
                )
            }
        case .videoCapture:
            let time = date.timeIntervalSince(state.videoStartDate)
            let maxTime = env.maxVideoCaptureTime
            switch time {
            case (0 ..< maxTime):
                return .set {
                    $0.videoCaptureTime = time
                    $0.videoCapturePercent = time / maxTime
                }
            default:
                return .concatenate(
                    .set { $0.videoCaptureCompleted = true },
                    .cancel(env.timerTaskId),
                    .task(advanceToNextStageTask)
                )
            }
        default:
            Log.warning("[CaptureStore] capture timer ticked for invalid stage:\(state.stage)")
            break
        }
        
        return .none
    }
}

extension Settings.CaptureFlow {
    var stages: [CaptureState.CaptureStage] {
        switch self {
        case .metricsVideo:
            return [
                .ready,
                .metricsCountdown,
                .metricsCapture,
                .videoCountdown,
                .videoCapture,
                .completed
            ]
        case .videoMetrics:
            return [
                .ready,
                .videoCountdown,
                .videoCapture,
                .metricsCountdown,
                .metricsCapture,
                .completed
            ]
        }
    }
}
