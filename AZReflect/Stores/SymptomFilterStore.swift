//
//  SymptomFilterStore.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 12/7/21.
//

import Foundation

struct SymptomFilterState {
    var symptoms: [Symptom] = Symptom.allCases.filter(\.isDisplay)
}

struct SymptomFilterEnvironment {
    let excludedSymptoms: [Symptom]
}

typealias SymptomFilterStore = Store<SymptomFilterState, SymptomFilterEnvironment>

extension SymptomFilterStore {
    convenience init(
        _ state: SymptomFilterState = .init(),
        excludedSymptoms: [Symptom] = []
    ) {
        self.init(
            state,
            env: .init(excludedSymptoms: excludedSymptoms),
            mapError: { error in
                return .none
            }
        )
    }
    
    func search(for text: String) {
        receive(.dataTask(loadSymptomsTask, text, 0.5, "search"))
    }
    
    private func loadSymptomsTask(for searchText: String) async throws -> Effect {
        let displayableSymptoms = Symptom.allCases.filter { symptom in
            return symptom.isDisplay && !env.excludedSymptoms.contains(symptom)
        }
        
        guard !searchText.isEmpty else {
            return .set { $0.symptoms = displayableSymptoms }
        }
        
        let filteredSymptoms = displayableSymptoms
            .compactMap { symptom -> (symptom: Symptom, range:Range<String.Index>)? in
                let range = symptom.title.range(of: searchText, options: .caseInsensitive)
                switch range {
                case .some(let range):
                    return (symptom, range)
                default:
                    return .none
                }
            }
            .sorted(by: { $0.range.lowerBound < $1.range.lowerBound })
            .map(\.symptom)
        
        return .set { $0.symptoms = filteredSymptoms }
    }
}

extension Symptom {
    var isDisplay: Bool {
        switch self {
        case .none:
            return false
        default:
            return true
        }
    }
}
