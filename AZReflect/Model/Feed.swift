//
//  Feed.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 9/21/21.
//

import Foundation

struct Feed {
    let reflections: [String: Reflection]
    
    init(reflections: [String: Reflection] = [:]) {
        self.reflections = reflections
    }
}

extension Feed: Codable {}
