//
//  ShareGroup.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 10/5/21.
//

import Foundation

struct ShareGroup {
    let members: [Member]
    let outgoingInvites: [Member]
    let incomingInvites: [Member]
}

extension ShareGroup {
    struct Member {
        let id: String
        let name: String
        let email: String
    }
}

extension ShareGroup: Equatable {
    var isEmpty: Bool {
        self == Self.empty
    }
    
    static var empty: ShareGroup {
        return .init(members: [], outgoingInvites: [], incomingInvites: [])
    }
}

extension ShareGroup.Member: Identifiable {}
extension ShareGroup.Member: Equatable {}
extension ShareGroup.Member: Codable {}
extension ShareGroup: Codable {}
