//
//  Reflection.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/4/21.
//

import Foundation

struct Reflection {
    let id: String
    let date: Date
    let title: String
    let metrics: AverageMetrics
    let transcription: String
    let symptoms: [Symptom]
    let videoFileName: String?
    let audioFileName: String?
    let heartRateVariability: Double?
    let stressLevel: StressLevel
    
    init(
        id: String,
        date: Date,
        title: String = "Weekly Check-In",
        metrics: AverageMetrics = .init(),
        transcription: String = "",
        symptoms: [Symptom] = [],
        videoFileName: String? = .none,
        audioFileName: String? = .none,
        heartRateVariability: Double? = .none,
        stressLevel: StressLevel = .unknown
    ) {
        self.id = id
        self.date = date
        self.title = title
        self.metrics = metrics
        self.transcription = transcription
        self.symptoms = symptoms
        self.videoFileName = videoFileName
        self.audioFileName = audioFileName
        self.heartRateVariability = heartRateVariability
        self.stressLevel = stressLevel
    }
}

extension Reflection {
    struct AverageMetrics {
        let heartRate: Double?
        let oxygenSaturation: Double?
        let breathingRate: Double?
        
        init(
            heartRate: Double? = .none,
            oxygenSaturation: Double? = .none,
            breathingRate: Double? = .none
        ) {
            self.heartRate = heartRate
            self.oxygenSaturation = oxygenSaturation
            self.breathingRate = breathingRate
        }
    }
    
    enum StressLevel: UInt32, Codable {
        case unknown = 0
        case low = 1
        case normal = 2
        case mild = 3
        case high = 4
        case extreme = 5
        
        init(value: UInt32) {
            switch StressLevel.init(rawValue: value) {
            case .some(let level):
                self = level
            default:
                self = .unknown
            }
        }
    }
}

extension Reflection {
    func with(videoFileName: String, audioFileName: String) -> Reflection {
        .init(
            id: id,
            date: date,
            title: title,
            metrics: metrics,
            transcription: transcription,
            symptoms: symptoms,
            videoFileName: videoFileName,
            audioFileName: audioFileName,
            heartRateVariability: heartRateVariability,
            stressLevel: stressLevel
        )
    }
    
    func with(title: String, symptoms: [Symptom]) -> Reflection {
        .init(
            id: id,
            date: date,
            title: title,
            metrics: metrics,
            transcription: transcription,
            symptoms: symptoms,
            videoFileName: videoFileName,
            audioFileName: audioFileName,
            heartRateVariability: heartRateVariability,
            stressLevel: stressLevel
        )
    }
}

extension Reflection.AverageMetrics: Codable {}
extension Reflection: Codable {}

extension Reflection.AverageMetrics: Equatable {}
extension Reflection: Equatable {}

extension Reflection: Identifiable {}
