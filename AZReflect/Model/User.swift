//
//  User.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import Foundation

struct User {
    let id: String
    let name: String
    let email: String
    let isOnboarded: Bool
    
    init(
        id: String = UUID().uuidString,
        name: String,
        email: String,
        isOnboarded: Bool = false
    ) {
        self.id = id
        self.name = name
        self.email = email
        self.isOnboarded = isOnboarded
    }
    
    func onboarded() -> User {
        return .init(id: id, name: name, email: email, isOnboarded: true)
    }
}

extension User: Equatable {}
extension User: Codable {}
