//
//  Symptom.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/10/21.
//

import Foundation

enum Symptom: String, CaseIterable {
    case none
    case abdominalPain
    case achieveAndMaintainErection
    case acne
    case anorexia
    case anxiety
    case bedPressureSores
    case bloating
    case blurredVision
    case bodyOdor
    case breastSwellingAndTenderness
    case bruising
    case changeInUsualUrineColor
    case cheilosis
    case chills
    case cognitive
    case concentration
    case constipation
    case cough
    case decreasedLibido
    case decreasedSweating
    case delayedOrgasm
    case depression
    case diarrhea
    case difficultySwallowing
    case discouraged
    case dizziness
    case dryMouth
    case earsRinging
    case ejaculation
    case fatigue
    case fecalIncontinence
    case fever
    case flashingLights
    case gas
    case generalPain
    case hairLoss
    case handFootSyndrome
    case headache
    case heartburn
    case heartPalpitations
    case hiccups
    case hives
    case hoarseness
    case hotFlashes
    case increasedSweating
    case insomnia
    case irregularPeriodsVaginalBleeding
    case itching
    case jointPain
    case missedExpectedMenstrualPeriod
    case mouthThroatSores
    case musclePain
    case nailDiscoloration
    case nailLoss
    case nailRidging
    case nausea
    case neuropathy
    case nosebleed
    case painAndSwellingAtInjectionSite
    case painfulUrination
    case painWithSexualIntercourse
    case radiationSkinReaction
    case rapidHeartRate
    case rash
    case sensitivityToSunlight
    case shortnessOfBreath
    case skinDarkening
    case skinDryness
    case stretchMarks
    case swelling
    case tasteChanges
    case unableToHaveOrgasm
    case urinaryFrequency
    case urinaryIncontinence
    case urinaryUrgency
    case vaginalDischarge
    case vaginalDryness
    case visualFloaters
    case voiceQualityChanges
    case vomiting
    case wateryEyes
    case wheezing
}

extension Symptom {
    var title: String {
        switch self {
        case .abdominalPain: return "Abdominal Pain"
        case .achieveAndMaintainErection: return "Achieve And Maintain Erection"
        case .acne: return "Acne"
        case .anorexia: return "Anorexia"
        case .anxiety: return "Anxiety"
        case .bedPressureSores: return "Bed Pressure Sores"
        case .bloating: return "Bloating"
        case .blurredVision: return "Blurred Vision"
        case .bodyOdor: return "Body Odor"
        case .breastSwellingAndTenderness: return "Breast Swelling And Tenderness"
        case .bruising: return "Bruising"
        case .changeInUsualUrineColor: return "Change In Usual Urine Color"
        case .cheilosis: return "Cheilosis"
        case .chills: return "Chills"
        case .cognitive: return "Cognitive"
        case .concentration: return "Concentration"
        case .constipation: return "Constipation"
        case .cough: return "Cough"
        case .decreasedLibido: return "Decreased Libido"
        case .decreasedSweating: return "Decreased Sweating"
        case .delayedOrgasm: return "Delayed Orgasm"
        case .depression: return "Depression"
        case .diarrhea: return "Diarrhea"
        case .difficultySwallowing: return "Difficulty Swallowing"
        case .discouraged: return "Discouraged"
        case .dizziness: return "Dizziness"
        case .dryMouth: return "Dry Mouth"
        case .earsRinging: return "Ears Ringing"
        case .ejaculation: return "Ejaculation"
        case .fatigue: return "Fatigue"
        case .fecalIncontinence: return "Fecal Incontinence"
        case .fever: return "Fever"
        case .flashingLights: return "Flashing Lights"
        case .gas: return "Gas"
        case .generalPain: return "General Pain"
        case .hairLoss: return "HairLoss"
        case .handFootSyndrome: return "Hand Foot Syndrome"
        case .headache: return "Headache"
        case .heartburn: return "Heartburn"
        case .heartPalpitations: return "Heart Palpitations"
        case .hiccups: return "Hiccups"
        case .hives: return "Hives"
        case .hoarseness: return "Hoarseness"
        case .hotFlashes: return "Hot Flashes"
        case .increasedSweating: return "Increased Sweating"
        case .insomnia: return "Insomnia"
        case .irregularPeriodsVaginalBleeding: return "Irregular Periods Vaginal Bleeding"
        case .itching: return "Itching"
        case .jointPain: return "Joint Pain"
        case .missedExpectedMenstrualPeriod: return "Missed Expected Menstrual Period"
        case .mouthThroatSores: return "Mouth Throat Sores"
        case .musclePain: return "Muscle Pain"
        case .nailDiscoloration: return "Nail Discoloration"
        case .nailLoss: return "Nail Loss"
        case .nailRidging: return "Nail Ridging"
        case .nausea: return "Nausea"
        case .neuropathy: return "Neuropathy"
        case .nosebleed: return "Nosebleed"
        case .painAndSwellingAtInjectionSite: return "Pain And Swelling At Injection Site"
        case .painfulUrination: return "Painful Urination"
        case .painWithSexualIntercourse: return "Pain With Sexual Intercourse"
        case .radiationSkinReaction: return "Radiation Skin Reaction"
        case .rapidHeartRate: return "Rapid Heart Rate"
        case .rash: return "Rash"
        case .sensitivityToSunlight: return "Sensitivity To Sunlight"
        case .shortnessOfBreath: return "Shortness Of Breath"
        case .skinDarkening: return "Skin Darkening"
        case .skinDryness: return "Skin Dryness"
        case .stretchMarks: return "Stretch Marks"
        case .swelling: return "Swelling"
        case .tasteChanges: return "Taste Changes"
        case .unableToHaveOrgasm: return "Unable To Have Orgasm"
        case .urinaryFrequency: return "Urinary Frequency"
        case .urinaryIncontinence: return "Urinary Incontinence"
        case .urinaryUrgency: return "Urinary Urgency"
        case .vaginalDischarge: return "Vaginal Discharge"
        case .vaginalDryness: return "Vaginal Dryness"
        case .visualFloaters: return "Visual Floaters"
        case .voiceQualityChanges: return "Voice Quality Changes"
        case .vomiting: return "Vomiting"
        case .wateryEyes: return "Watery Eyes"
        case .wheezing: return "Wheezing"
        default: return "\(self)"
        }
    }
        
    var descriptors: [String] {
        switch self {
        case .acne:
            return ["acne", "pimples", "pimple"]
        case .anxiety:
            return ["anxiety", "anxious"]
        case .bloating:
            return ["bloat", "bloated", "bloating"]
        case .blurredVision:
            return ["vision was blurred", "vision is blurred", "vision was blurry", "vision is blurry", "had blurry vision"]
        case .bruising:
            return ["bruise", "bruised", "bruising"]
        case .constipation:
            return ["constipated", "constipation"]
        case .cough:
            return ["cough", "coughing"]
        case .diarrhea:
            return ["diarrhea"]
        case .dizziness:
            return ["dizzy", "dizziness"]
        case .fatigue:
            return ["tired"]
        case .fever:
            return ["had a high fever", "felt feverish", "had a high temperature"]
        case .headache:
            return ["headache", "headaches", "head aches", "head ache"]
        case .nausea:
            return ["nausea", "nauseous"]
        case .rapidHeartRate:
            return ["heart was racing", "heart is racing"]
        case .rash:
            return ["rash", "rashes"]
        case .swelling:
            return ["swelling", "swollen"]
        case .vomiting:
            return ["vomit", "vomiting", "threw up", "throw up", "throwing up"]
        default:
            return []
        }
    }
}

extension Symptom: Identifiable {
    var id: String { self.rawValue }
}

extension Symptom: Equatable {}
extension Symptom: Codable {}
