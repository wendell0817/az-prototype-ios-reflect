//
//  Settings.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/17/21.
//

import Foundation

struct Settings {
    var onboardFlow: OnboardingFlow
    var captureFlow: CaptureFlow
    
    init(
        onboardFlow: OnboardingFlow = .photos,
        captureFlow: CaptureFlow = .metricsVideo
    ) {
        self.onboardFlow = onboardFlow
        self.captureFlow = captureFlow
    }
}

extension Settings {
    enum OnboardingFlow: Int, CaseIterable {
        case photos
        case capture
    }
    
    enum CaptureFlow: Int, CaseIterable {
        case metricsVideo
        case videoMetrics
    }
}

extension Settings.OnboardingFlow: Codable {}
extension Settings.CaptureFlow: Codable {}
extension Settings: Codable {}
extension Settings: Equatable {}
