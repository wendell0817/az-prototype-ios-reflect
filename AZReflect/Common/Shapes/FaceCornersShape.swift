//
//  FaceCornersShape.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/26/21.
//

import SwiftUI

struct FaceCornersShape: Shape {
    func path(in rect: CGRect) -> Path {
        let minDimension = min(rect.width, rect.height)
        let cornerLength = minDimension * 0.1
        
        return Path { path in
            path.move(to: .init(x: 0.0, y: cornerLength))
            path.addLine(to: .zero)
            path.addLine(to: .init(x: cornerLength, y: 0.0))
            
            path.move(to: .init(x: rect.width - cornerLength, y: 0.0))
            path.addLine(to: .init(x: rect.width, y: 0.0))
            path.addLine(to: .init(x: rect.width, y: cornerLength))
            
            path.move(to: .init(x: rect.width, y: rect.height - cornerLength))
            path.addLine(to: .init(x: rect.width, y: rect.height))
            path.addLine(to: .init(x: rect.width - cornerLength, y: rect.height))
            
            path.move(to: .init(x: cornerLength, y: rect.height))
            path.addLine(to: .init(x: 0.0, y: rect.height))
            path.addLine(to: .init(x: 0.0, y: rect.height - cornerLength))
        }
    }
}

struct FaceCornersShape_Previews: PreviewProvider {
    static var previews: some View {
        FaceCornersShape()
            .stroke(Color.blue, lineWidth: 8.0)
            .padding()
    }
}
