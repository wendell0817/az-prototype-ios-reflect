//
//  Line.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/11/21.
//

import SwiftUI

struct Line: Shape {
    var axis: Axis
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        switch axis {
        case .horizontal:
            path.move(to: .init(x: 0.0, y: rect.midY))
            path.addLine(to: .init(x: rect.width, y: rect.midY))
        case .vertical:
            path.move(to: .init(x: rect.midX, y: 0.0))
            path.addLine(to: .init(x: rect.midX, y: rect.height))
        }
        
        return path
    }
}

struct Line_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Line(axis: .horizontal).stroke(Color.blue, lineWidth: 4.0)
                .frame(height: 20.0)
            Line(axis: .vertical).stroke(Color.blue, lineWidth: 4.0)
                .frame(width: 20.0)
        }
        .padding()
        .previewLayout(.sizeThatFits)
    }
}
