//
//  CardShape.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/9/21.
//

import SwiftUI

struct CardShape: Shape {
    let cornerRadius: CGFloat
    
    func path(in rect: CGRect) -> Path {
        /*
                  270
            180          0
                  90
         */
        
        var path = Path()
        path.move(to: .init(x: 0.0, y: cornerRadius))
        path.addArc(
            center: .init(x: cornerRadius, y: cornerRadius),
            radius: cornerRadius,
            startAngle: .degrees(180.0),
            endAngle: .degrees(270.0),
            clockwise: false
        )
        
        path.addLine(to: .init(x: rect.width - (cornerRadius), y: 0.0))
        path.addArc(
            center: .init(
                x: rect.width - cornerRadius,
                y: cornerRadius
            ),
            radius: cornerRadius,
            startAngle: .degrees(270.0),
            endAngle: .degrees(0.0),
            clockwise: false
        )
        
        path.addLine(to: .init(x: rect.width, y: rect.height))
        path.addLine(to: .init(x: 0.0, y: rect.height))
        path.addLine(to: .init(x: 0.0, y: cornerRadius))
        
        return path
    }
}

struct CardShape_Previews: PreviewProvider {
    static var previews: some View {
        CardShape(cornerRadius: 44.0)
            .fill(Color.black)
    }
}
