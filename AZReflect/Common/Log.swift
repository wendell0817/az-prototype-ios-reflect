//
//  Log.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import Foundation
import Combine

struct Log {
    static var level: Level = .debug
    
    private static var logDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm-ss:SSS"
        return formatter
    }()
    
    static func error(_ error: Error) {
        log(.error, msg: "\(error)")
    }
    
    static func error(_ msg: String) {
        log(.error, msg: msg)
    }
    
    static func debug(_ msg: String) {
        log(.debug, msg: msg)
    }
    
    static func warning(_ msg: String) {
        log(.warning, msg: msg)
    }
    
    static func verbose(_ msg: String) {
        log(.verbose, msg: msg)
    }
    
    private static func log(_ level: Log.Level, msg: String) {
        guard level.rawValue <= Log.level.rawValue else { return }
        let log = [
            "[\(logDateFormatter.string(from: Date()))]",
            level.tag,
            msg
        ]
        .joined(separator: " - ")
        
        print(log)
    }
}

extension Log {
    struct Publisher<Upstream: Combine.Publisher>: Combine.Publisher {
        typealias Output = Upstream.Output
        typealias Failure = Upstream.Failure
        
        let upstream: Upstream
        let logLevel: Log.Level
        let prefix: String?
        
        func receive<S>(subscriber: S) where S : Subscriber, Upstream.Failure == S.Failure, Upstream.Output == S.Input {
            upstream
                .handleEvents(
                    receiveOutput: { output in
                        let log = [prefix, "\(output)"].compactMap { $0 }.joined(separator: " ")
                        Log.log(logLevel, msg: log)
                    }
                )
                .subscribe(subscriber)
        }
    }
    
    struct ErrorPublisher<Upstream: Combine.Publisher>: Combine.Publisher {
        typealias Output = Upstream.Output
        typealias Failure = Upstream.Failure
        
        let upstream: Upstream
        
        func receive<S>(subscriber: S) where S : Subscriber, Upstream.Failure == S.Failure, Upstream.Output == S.Input {
            upstream
                .mapError { error in
                    Log.error("\(error)")
                    return error
                }
                .subscribe(subscriber)
        }
    }
}

extension Combine.Publisher {
    func log(_ level: Log.Level, prefix: String? = .none) -> Log.Publisher<Self> {
        Log.Publisher(upstream: self, logLevel: level, prefix: prefix)
    }
}

extension Combine.Publisher {
    func logError() -> Log.ErrorPublisher<Self> {
        Log.ErrorPublisher(upstream: self)
    }
}

extension Log {
    enum Level: UInt {
        case error = 0
        case debug = 1
        case warning = 2
        case verbose = 3
        
        var tag: String {
            switch self {
            case .error: return "[ERROR🚨]"
            case .debug: return "[DEBUG🐛]"
            case .warning: return "[WARNING⚠️]"
            case .verbose: return "[VERBOSE💬]"
            }
        }
    }
}
