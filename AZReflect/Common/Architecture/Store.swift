//
//  Store.swift
//  Architecture
//
//  Created by Wendell Thompson on 12/3/21.
//

import Foundation
import Combine

// MARK: Store

@dynamicMemberLookup
final class Store<State, Environment>: ObservableObject {
    private let stateSubject: CurrentValueSubject<State, Never>
    let env: Environment
    private let mapError: (Error) -> Effect
    private var streamContinuation: AsyncStream<Effect>.Continuation? = .none
    private var reduceTask: Task<Void, Never>? = .none
    private var cancellables: [AnyHashable: CancelItem] = [:]
    
    private var timerQueue: DispatchQueue {
        return DispatchQueue(label: "\(type(of: self)).timer.queue")
    }
    
    var state: State {
        get { stateSubject.value }
    }
    
    var statePublisher: AnyPublisher<State, Never> {
        get { stateSubject.eraseToAnyPublisher() }
    }
    
    subscript <Value>(dynamicMember dynamicMember: KeyPath<State, Value>) -> Value {
        get { stateSubject.value[keyPath: dynamicMember] }
    }
    
    init(
        _ state: State,
        env: Environment,
        mapError: @escaping (Error) -> Effect
    ) {
        self.stateSubject = .init(state)
        self.env = env
        self.mapError = mapError
        
        let stream = AsyncStream<Effect> { cont in
            self.streamContinuation = cont
        }
        
        self.reduceTask = Task {
            for await effect in stream {
                await reduce(effect)
            }
        }
    }
    
    deinit {
        cancellables.values.forEach { $0.cancel() }
        streamContinuation?.finish()
        reduceTask?.cancel()
    }
    
    func receive(_ effect: Effect) {
        cancel(effect.operation.cancelId)
        let task = Task { await reduce(effect) }
        store(effect.operation.cancelId, task: task)
    }
    
    private func reduce(_ effect: Effect) async {
        switch effect.operation {
        case .none:
            return
        case .set(let setter):
            await setOnMain(setter)
        case .task(let operation, _, _):
            let effect = await execute(operation, delay: effect.operation.delay)
            await reduce(effect)
        case .timer(let id, let interval, let mapEffect):
            timerQueue.async { [weak self] in
                guard let self = self else { return }
                let timer = Timer(
                    timeInterval: interval,
                    repeats: true,
                    block: { _ in self.streamContinuation?.yield(mapEffect(Date())) }
                )
                self.cancellables[id] = .timer(timer)
                RunLoop.current.add(timer, forMode: .default)
                RunLoop.current.run()
            }
        case .cancel(let id):
            cancel(id)
        case .merge(let effects):
            for effect in effects {
                Task { await reduce(effect) }
            }
        case .concatenate(let effects):
            for effect in effects {
                await reduce(effect)
            }
        }
    }
    
    private func cancel(_ id: AnyHashable?) {
        guard let id = id else { return }
        cancellables[id]?.cancel()
        cancellables[id] = .none
    }
    
    private func store(_ id: AnyHashable?, task: Task<Void, Never>) {
        guard let id = id else { return }
        cancellables[id] = .task(task)
    }
    
    @MainActor private func setOnMain(_ setter: @escaping (inout State) -> Void) {
        objectWillChange.send()
        setter(&stateSubject.value)
    }
    
    private func execute(
        _ operation: () async throws -> Effect,
        delay interval: TimeInterval?
    ) async -> Effect {
        do {
            try await delay(by: interval)
            return try await operation()
        } catch let error {
            return mapError(error)
        }
    }
    
    private func delay(by interal: TimeInterval?) async throws {
        switch interal {
        case .some(let interval):
            try await Task.trySleep(timeInterval: interval)
        default:
            break
        }
    }
}

// MARK: Effects

extension Store {
    struct Effect {
        enum Operation {
            case none
            case task(operation: () async throws -> Effect, delay: TimeInterval?, cancelId: AnyHashable?)
            case set((inout State) -> Void)
            case timer(id: AnyHashable, interval: TimeInterval, effect: (Date) -> Effect)
            case cancel(id: AnyHashable)
            case merge([Effect])
            case concatenate([Effect])
            
            var delay: TimeInterval? {
                switch self {
                case .task(_, let delay, _):
                    return delay
                default:
                    return .none
                }
            }
            
            var cancelId: AnyHashable? {
                switch self {
                case .task(_, _, let cancelId):
                    return cancelId
                default:
                    return .none
                }
            }
        }
        
        let operation: Operation
        
        init(operation: Operation) {
            self.operation = operation
        }
        
        static var none: Effect {
            return .init(operation: .none)
        }
        
        static func set(_ setter: @escaping (inout State) -> Void) -> Effect {
            return .init(operation: .set(setter))
        }
        
        static func task(
            _ operation: @escaping () async throws -> Effect,
            delay: TimeInterval? = .none,
            cancelId: AnyHashable? = .none
        ) -> Effect {
            return .init(operation: .task(operation: operation, delay: delay, cancelId: cancelId))
        }
        
        static func dataTask<Data>(
            _ operation: @escaping (Data) async throws -> Effect,
            _ data: Data,
            _ delay: TimeInterval? = .none,
            _ cancelId: AnyHashable? = .none
        ) -> Effect {
            return .init(
                operation: .task(
                    operation: { try await operation(data) },
                    delay: delay,
                    cancelId: cancelId
                )
            )
        }
        
        static func timer(
            _ id: AnyHashable,
            interval: TimeInterval,
            mapEffect: @escaping (Date) -> Effect
        ) -> Effect {
            return .init(operation: .timer(id: id, interval: interval, effect: mapEffect))
        }
        
        static func cancel(_ id: AnyHashable) -> Effect {
            return .init(operation: .cancel(id: id))
        }
        
        static func merge(_ effects: Effect ...) -> Effect {
            return .init(operation: .merge(effects))
        }
        
        static func merge(_ effects: [Effect]) -> Effect {
            return .init(operation: .merge(effects))
        }
        
        static func concatenate(_ effects: Effect ...) -> Effect {
            return .init(operation: .concatenate(effects))
        }
        
        static func concatenate(_ effects: [Effect]) -> Effect {
            return .init(operation: .concatenate(effects))
        }
    }
}

// MARK: Cancellation

extension Store {
    enum CancelItem {
        case task(Task<Void, Never>)
        case timer(Timer)
        case publisher(AnyCancellable)
        
        func cancel() {
            switch self {
            case .task(let task):
                task.cancel()
            case .timer(let timer):
                timer.invalidate()
            case .publisher(let cancellable):
                cancellable.cancel()
            }
        }
        
        func wait() async {
            switch self {
            case .task(let task):
                await task.value
            default:
                return
            }
        }
    }
}

// MARK: Combine

extension Store {
    func subscribe<OtherState, OtherEnviroment, Value>(
        to otherStore: Store<OtherState, OtherEnviroment>,
        on keyPath: KeyPath<OtherState, Value>,
        id: AnyHashable,
        mapEffect: @escaping (Value) -> Effect
    ) {
        let cancellable = otherStore.stateSubject
            .map(keyPath)
            .map(mapEffect)
            .sink { [weak self] effect in
                guard let self = self else { return }
                self.receive(effect)
            }
        cancellables[id] = .publisher(cancellable)
    }
    
    func subscribe<OtherState, OtherEnviroment, Value>(
        to otherStore: Store<OtherState, OtherEnviroment>,
        on keyPath: KeyPath<OtherState, Value>,
        id: AnyHashable,
        mapEffect: @escaping (Value) -> Effect
    ) where Value: Equatable {
        let cancellable = otherStore.stateSubject
            .map(keyPath)
            .removeDuplicates()
            .map(mapEffect)
            .sink { [weak self] effect in
                guard let self = self else { return }
                self.receive(effect)
            }
        cancellables[id] = .publisher(cancellable)
    }
    
    func subscribe<Value, SomeError>(
        to publisher: AnyPublisher<Value, SomeError>,
        id: AnyHashable,
        mapEffect: @escaping (Value) -> Effect
    ) {
        let cancellable = publisher
            .map(mapEffect)
            .catch { [weak self] error -> AnyPublisher<Effect, Never> in
                guard let self = self else { return Just(.none).eraseToAnyPublisher() }
                return Just(error).map(self.mapError).eraseToAnyPublisher()
            }
            .sink { [weak self] effect in
                guard let self = self else { return }
                self.receive(effect)
            }
        
        cancellables[id] = .publisher(cancellable)
    }
    
    func subscribe<Value, SomeError>(
        to publisher: AnyPublisher<Value, SomeError>,
        id: AnyHashable,
        mapEffect: @escaping (Value) -> Effect
    ) where Value: Equatable {
        let cancellable = publisher
            .removeDuplicates()
            .map(mapEffect)
            .catch { [weak self] error -> AnyPublisher<Effect, Never> in
                guard let self = self else { return Just(.none).eraseToAnyPublisher() }
                return Just(error).map(self.mapError).eraseToAnyPublisher()
            }
            .sink { [weak self] effect in
                guard let self = self else { return }
                self.receive(effect)
            }
        
        cancellables[id] = .publisher(cancellable)
    }
    
    func subscribe<Value>(
        to publisher: AnyPublisher<Value, Never>,
        id: AnyHashable,
        mapEffect: @escaping (Value) -> Effect
    ) {
        let cancellable = publisher
            .map(mapEffect)
            .sink { [weak self] effect in
                guard let self = self else { return }
                self.receive(effect)
            }
        
        cancellables[id] = .publisher(cancellable)
    }
    
    func subscribe<Value>(
        to publisher: AnyPublisher<Value, Never>,
        id: AnyHashable,
        mapEffect: @escaping (Value) -> Effect
    ) where Value: Equatable {
        let cancellable = publisher
            .removeDuplicates()
            .map(mapEffect)
            .sink { [weak self] effect in
                guard let self = self else { return }
                self.receive(effect)
            }
        
        cancellables[id] = .publisher(cancellable)
    }
}

extension Task where Success == Never, Failure == Never {
    static func trySleep(timeInterval: TimeInterval) async throws {
        try await Task.sleep(nanoseconds: UInt64(timeInterval * 1_000_000_000))
    }
}
