//
//  EnvironmentKeys.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 11/3/21.
//

import Foundation
import SwiftUI

enum HomeNavigationItem: Equatable {
    case none
    case settings
    case review(String)
    case capture
    
    mutating func dismiss() {
        self = .none
    }
    
    static func ==(lhs: HomeNavigationItem, rhs: HomeNavigationItem) -> Bool {
        switch (lhs, rhs) {
        case (.none, .none),
            (.settings, .settings),
            (.capture, .capture):
            return true
        case(.review(let leftID), .review(let rightID)):
            return leftID == rightID
        default:
            return false
        }
    }
}

struct HomeNavigationRootKey: EnvironmentKey {
    static var defaultValue: Binding<HomeNavigationItem> { .constant(.none) }
}

extension EnvironmentValues {
    var homeNavigationRoot: Binding<HomeNavigationItem> {
        get { self[HomeNavigationRootKey.self] }
        set { self[HomeNavigationRootKey.self] = newValue }
    }
}

extension View {
    func homeNavigationRoot(_ item: Binding<HomeNavigationItem>) -> some View {
        environment(\.homeNavigationRoot, item)
    }
}

extension Binding where Value == HomeNavigationItem {
    func isActive(for item: HomeNavigationItem) -> Binding<Bool> {
        .init(
            get: { wrappedValue == item },
            set: { wrappedValue = $0 ? item : .none } )
    }
}
