//
//  VideoThumbnailView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/31/21.
//

import SwiftUI
import AVFoundation

final class ThumbnailGenerator: ObservableObject {
    @Published public private(set) var image: UIImage? = .none
    
    func loadImage(for url: URL?, for size: CGSize) {
        DispatchQueue.global(qos: .background).async {
            guard let url = url else { return }
            
            let asset: AVAsset = AVAsset(url: url)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            
            do {
                let thumbnailImage = try imageGenerator.copyCGImage(
                    at: .init(seconds: 1.0, preferredTimescale: .max),
                    actualTime: .none
                )
                
                let image = UIImage(cgImage: thumbnailImage)
                
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.image = image
                }
            } catch let error {
                Log.error("[ThumbnailGenerator] url:\(url.absoluteString) \(error)")
            }
        }
    }
}

struct VideoThumbnailView: View {
    @StateObject private var generator = ThumbnailGenerator()
    let url: URL?
    
    var body: some View {
        GeometryReader { proxy in
            ZStack {
                if let image = generator.image {
                    Image(uiImage: image)
                        .resizable()
                        .scaledToFill()
                        .frame(width: proxy.size.width, height: proxy.size.height)
                        .clipped()
                } else {
                    ZStack {
                        Rectangle()
                            .themeModifier(.reflectGradient)
                        
                    }
                    .foregroundColor(.white)
                    .frame(width: proxy.size.width, height: proxy.size.height)
                }
            }
            .onChange(of: url) { url in
                generator.loadImage(for: url, for: proxy.size)
            }
            .onAppear {
                generator.loadImage(for: url, for: proxy.size)
            }
        }
    }
}

struct VideoThumbnailView_Previews: PreviewProvider {
    static let urlString = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
    
    static var previews: some View {
        VStack {
            VideoThumbnailView(url: URL(string: urlString))
        }
    }
}
