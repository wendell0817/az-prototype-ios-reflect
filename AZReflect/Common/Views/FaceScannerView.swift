//
//  FaceScannerView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/26/21.
//

import SwiftUI

struct FaceScannerView: View {
    private var cornerLength: CGFloat = 40.0
    
    var body: some View {
        GeometryReader { proxy in
            ZStack {
                Rectangle().borderStroke()
                FaceCornersShape().cornerStroke()
            }
        }
    }
}

fileprivate extension Shape {
    func borderStroke() -> some View {
        stroke(
            Color.accentColor,
            style: .init(
                lineWidth: 2.0,
                lineCap: .round,
                lineJoin: .bevel,
                dash: [16.0],
                dashPhase: 16.0
            )
        )
    }
    
    func cornerStroke() -> some View {
        stroke(
            Color.accentColor,
            style: .init(
                lineWidth: 8.0,
                lineCap: .round,
                lineJoin: .round,
                dash: [],
                dashPhase: 0.0
            )
        )
    }
}

#if DEBUG

struct FaceScannerView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader { _ in
            FaceScannerView()
                .accentColor(Theme.color(.warning))
                .padding()
        }
        .background(
            Image("capture.image.test")
                .resizable()
                .scaledToFill()
                .ignoresSafeArea()
        )
    }
}

#endif
