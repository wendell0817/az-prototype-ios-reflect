//
//  BlurView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 12/7/21.
//

import SwiftUI
import UIKit

struct BlurView: UIViewRepresentable {
    enum Style {
        case light
        case dark
    }
    
    let style: BlurView.Style
    
    func makeUIView(context: Context) -> UIVisualEffectView {
        let view = UIVisualEffectView(effect: style.effect)
        return view
    }
    
    func updateUIView(_ uiView: UIVisualEffectView, context: Context) {
        uiView.effect = style.effect
    }
}

extension BlurView.Style {
    var effect: UIBlurEffect {
        switch self {
        case .light:
            return .init(style: .light)
        case .dark:
            return .init(style: .dark)
        }
    }
}

#if DEBUG

extension BlurView.Style {
    @ViewBuilder func previewContent() -> some View {
        switch self {
        case .light:
            Text("Light").foregroundColor(.black)
        case .dark:
            Text("Dark").foregroundColor(.white)
        }
    }
}

struct BlurView_Previews: PreviewProvider {
    static let styles: [BlurView.Style] = [.light, .dark]
    
    static var previews: some View {
        GeometryReader { proxy in
            VStack(spacing: 16.0) {
                ForEach(Self.styles, id: \.self) { style in
                    BlurView(style: style)
                        .overlay(style.previewContent())
                        .frame(height: 100.0)
                }
            }
            .padding(.horizontal, 20.0)
            .frame(size: proxy.size)
        }
        .background(
            Image("capture.image.test")
                .resizable()
                .scaledToFill()
                .ignoresSafeArea()
        )
    }
}

#endif
