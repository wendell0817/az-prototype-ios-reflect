//
//  BackNavigationView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import SwiftUI

struct BackNavigationView: View {
    @Environment(\.presentationMode) private var presentationMode
    
    var body: some View {
        HStack {
            Button(
                action: { presentationMode.wrappedValue.dismiss() },
                label: {
                    Image(systemName: "arrow.left")
                        .themeFont(.title)
                }
            )
            
            Spacer()
        }
        .buttonStyle(PlainButtonStyle())
    }
}

#if DEBUG

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        BackNavigationView()
    }
}

#endif
