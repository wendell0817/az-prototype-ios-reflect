//
//  ExpandingView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/24/21.
//

import SwiftUI

struct ExpandingView<Content: View>: View {
    @Binding var isExpanded: Bool
    @ViewBuilder let content: Content
    
    var header: some View {
        HStack {
            Spacer()
            
            RoundedRectangle(cornerRadius: 2.0)
                .fill(Theme.color(.backgroundCard))
                .frame(width: 44.0, height: 4.0)
            
            Spacer()
        }
        .frame(height: 20.0)
        .contentShape(Rectangle())
    }
    
    var body: some View {
        VStack(spacing: 0.0) {
            header
                .onTapGesture { isExpanded.toggle() }
                .zIndex(1.0)
            
            content
                .fixedSize(horizontal: false, vertical: true)
                .frame(height: .none)
                .frame(height: isExpanded ? .none : 0.0, alignment: .bottom)
                .clipped()
                .zIndex(0.0)
        }
    }
}

#if DEBUG

struct ExpandingContainerView: View {
    @State private var isExpanded = true
    var body: some View {
        VStack {
            ExpandingView(isExpanded: $isExpanded) {
                Text("Expanding View")
            }
        }
    }
}

struct ExpandingView_Previews: PreviewProvider {
    static var previews: some View {
        ExpandingContainerView()
    }
}

#endif
