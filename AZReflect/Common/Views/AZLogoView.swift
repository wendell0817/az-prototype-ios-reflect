//
//  AZLogoView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 11/30/21.
//

import SwiftUI

struct AZLogoView: View {
    var body: some View {
        Image("az.logo")
            .resizable()
            .scaledToFit()
            .foregroundColor(Color.accentColor)
            .frame(width: 180.0, height: 50.0)
    }
}

struct AZLogoView_Previews: PreviewProvider {
    static var previews: some View {
        AZLogoView()
    }
}
