//
//  ImageView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/9/21.
//

import SwiftUI

struct ImageView: UIViewRepresentable {
    let uiImage: UIImage?
    
    func makeUIView(context: Context) -> UIImageView {
        let imageView = UIImageView()
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageView.contentMode = .scaleAspectFill
        imageView.image = uiImage
        return imageView
    }
    
    func updateUIView(_ uiImageView: UIImageView, context: Context) {
        uiImageView.image = uiImage
        guard let size = uiImage?.size else { return }
        uiImageView.contentMode = ((size.width < size.height) ? .scaleAspectFit : .scaleAspectFill)
    }
}

struct ImageView_Previews: PreviewProvider {
    static var previews: some View {
        ImageView(uiImage: .init(named: "capture.image.test"))
    }
}
