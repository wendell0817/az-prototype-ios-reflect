//
//  VideoPreviewView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/9/21.
//

import SwiftUI
import AVFoundation

class PreviewView: UIView {
    var videoPreviewLayer: AVCaptureVideoPreviewLayer {
        guard let layer = layer as? AVCaptureVideoPreviewLayer else {
            fatalError("Expected `AVCaptureVideoPreviewLayer` type for layer. Check PreviewView.layerClass implementation.")
        }
        return layer
    }
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.bounds = layer.bounds
        videoPreviewLayer.position = CGPoint(x: layer.bounds.midX, y: layer.bounds.midY)
    }
    
    var session: AVCaptureSession? {
        get { videoPreviewLayer.session }
        set { videoPreviewLayer.session = newValue }
    }
    
    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
}

struct VideoPreviewView: UIViewRepresentable {
    typealias UIViewType = PreviewView
    
    let session: AVCaptureSession?
    let orientation: AVCaptureVideoOrientation
    
    init(
        session: AVCaptureSession?,
        orientation: AVCaptureVideoOrientation = .portrait
    ) {
        self.session = session
        self.orientation = orientation
    }
    
    func makeUIView(context: Context) -> PreviewView {
        let view = PreviewView()
        view.session = session
        if let connection = view.videoPreviewLayer.connection {
            Log.verbose("[VideoPreviewView] videoMaxScaleAndCropFactor:\(connection.videoMaxScaleAndCropFactor)")
            connection.videoScaleAndCropFactor = connection.videoMaxScaleAndCropFactor
            connection.videoOrientation = orientation
        }
        return view
    }
    
    func updateUIView(_ uiView: PreviewView, context: Context) {
        uiView.session = session
        if let connection = uiView.videoPreviewLayer.connection {
            connection.videoScaleAndCropFactor = connection.videoMaxScaleAndCropFactor
            connection.videoOrientation = orientation
        }
    }
}

extension AVCaptureVideoOrientation: CustomStringConvertible {
    public var description: String {
        switch self {
        case .landscapeLeft:
            return "landscapeLeft"
        case .landscapeRight:
            return "landscapeRight"
        case .portrait:
            return "portrait"
        case .portraitUpsideDown:
            return "portraitUpsideDown"
        default:
            return "unknown"
        }
    }
}

struct VideoPreviewView_Previews: PreviewProvider {
    static var previews: some View {
        VideoPreviewView(session: .none)
    }
}
