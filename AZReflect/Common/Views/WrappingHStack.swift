//
//  WrappingHStack.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 10/20/21.
//

import SwiftUI

// SOURCE:
// https://stackoverflow.com/questions/62102647/swiftui-hstack-with-wrap-and-dynamic-height/62103264#62103264

struct WrappingHStack<Data: RandomAccessCollection, ID: Hashable, Content: View>: View {
    let data: Data
    let id: KeyPath<Data.Element, ID>
    let spacing: CGFloat
    let content: (Data.Element) -> Content
    
    @State private var height: CGFloat = 0.0
    @State private var animation: Animation? = .none
    
    init(
        data: Data,
        id: KeyPath<Data.Element, ID>,
        spacing: CGFloat = 0.0,
        content: @escaping (Data.Element) -> Content
    ) {
        self.data = data
        self.id = id
        self.spacing = spacing
        self.content = content
    }
    
    var body: some View {
        GeometryReader { proxy in
            wrappedView(for: proxy.size)
        }
        .frame(height: height)
    }
    
    private func wrappedView(for size: CGSize) -> some View {
        var offset: CGSize = .zero
        
        return ZStack(alignment: .topLeading) {
            ForEach(data, id: id) { element in
                content(element)
                    .alignmentGuide(.leading) { dimension in
                        let spacedWidth = dimension.width + spacing
                        let spacedHeight = dimension.height + spacing
                        
                        if (abs(offset.width - spacedWidth) > size.width) {
                            offset.width = 0.0
                            offset.height -= spacedHeight
                        }
                        let result = offset.width
                        if element[keyPath: id] == data.last?[keyPath: id] {
                            offset.width = 0.0
                        } else {
                            offset.width -= spacedWidth
                        }
                        
                        return result
                    }
                    .alignmentGuide(.top) { dimension in
                        let result = offset.height
                        if element[keyPath: id] == data.last?[keyPath: id] {
                            offset.height = 0 // last item
                        }
                        return result
                    }
            }
        }
        .background(
            GeometryReader { geometry -> Color in
                let rect = geometry.frame(in: .local)
                DispatchQueue.main.async {
                    withAnimation(animation) {
                        height = rect.size.height
                    }
                }
                return .clear
            }
        )
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                animation = .default
            }
        }
    }
}

extension WrappingHStack where Data.Element: Identifiable, Data.Element.ID == ID {
    init(data: Data, spacing: CGFloat = 0.0, content: @escaping (Data.Element) -> Content) {
        self.data = data
        self.id = \Data.Element.id
        self.spacing = spacing
        self.content = content
    }
}

struct WrappingHStack_Previews: PreviewProvider {
    struct PreviewItem: Identifiable {
        let id: String
        let value: String
    }
    
    static let previewItems: [PreviewItem] = [
        .init(id: "1", value: "One"),
        .init(id: "2", value: "Two"),
        .init(id: "3", value: "Three"),
        .init(id: "4", value: "Four"),
        .init(id: "5", value: "Five"),
        .init(id: "6", value: "Six"),
        .init(id: "7", value: "Seven"),
        .init(id: "8", value: "Eight"),
        .init(id: "9", value: "Nine"),
        .init(id: "10", value: "Ten"),
    ]
    
    static var previews: some View {
        ScrollView {
            WrappingHStack(data: previewItems, spacing: 16.0) { item in
                Text(item.value)
                    .foregroundColor(.white)
                    .padding(.horizontal, 16.0)
                    .frame(height: 32.0)
                    .background(
                        RoundedRectangle(cornerRadius: 8.0)
                            .fill(Color.blue)
                    )
                    .font(.title)
            }
            .background(Color.gray)
        }
    }
}
