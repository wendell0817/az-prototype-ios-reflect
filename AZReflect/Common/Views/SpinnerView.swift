//
//  SpinnerView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 12/1/21.
//

import SwiftUI

struct SpinnerView: View {
    struct ViewState {
        var spinState: SpinState = .start
        var timer: Timer? = .none
    }
    
    let style: Style
    @State private var state = ViewState()
    private let animationInterval = 1.0
    
    private var spinValues: (start: Double, end: Double) {
        let orientation: Double = 270.0
        
        switch state.spinState {
        case .start:
            return (orientation, orientation)
        case .expand:
            return (orientation + 360.0, orientation)
        case .collapse:
            return (orientation + 360.0, orientation + 360.0)
        }
    }
    
    var body: some View {
        GeometryReader { proxy in
            SpinnerShape(
                start: spinValues.start,
                end: spinValues.end
            )
            .stroke(
                gradient(for: spinValues),
                lineWidth: style.lineWidth
            )
            .padding(style.lineWidth * 0.5)
        }
        .frame(size: style.size)
        .animation(.linear(duration: animationInterval), value: state.spinState)
        .onAppear(perform: startTimer)
        .onDisappear(perform: stopTimer)
    }
    
    private func gradient(for angles: (start: Double, end: Double)) -> AngularGradient {
        return AngularGradient(
            colors: [
                Theme.color(.accentDark),
                Theme.color(.accentLight)
            ],
            center: .center,
            startAngle: .degrees(angles.start),
            endAngle: .degrees(angles.end)
        )
    }
    
    private func startTimer() {
        state.timer?.invalidate()
        state.timer = Timer.scheduledTimer(
            withTimeInterval: animationInterval,
            repeats: true,
            block: { _ in state.spinState.next() }
        )
    }
    
    private func stopTimer() {
        state.timer?.invalidate()
    }
}

extension SpinnerView {
    enum Style {
        case large
        case small
        
        var size: CGSize {
            switch self {
            case .large:
                return .init(width: 32.0, height: 32.0)
            case .small:
                return .init(width: 16.0, height: 16.0)
            }
        }
        
        var lineWidth: CGFloat {
            switch self {
            case .large:
                return 4.0
            case .small:
                return 2.0
            }
        }
    }
    
    enum SpinState {
        case start
        case expand
        case collapse
        
        mutating func next() {
            switch self {
            case .start:
                self = .expand
            case .expand:
                self = .collapse
            case .collapse:
                self = .start
            }
        }
    }
}

extension SpinnerView {
    struct SpinnerShape: Shape {
        var start: Double
        var end: Double
        
        var animatableData: AnimatablePair<Double, Double> {
            get { AnimatablePair(start, end) }
            set {
                start = newValue.first
                end = newValue.second
            }
        }
        
        func path(in rect: CGRect) -> Path {
            Path { path in
                path.addArc(
                    center: .init(
                        x: rect.size.width * 0.5,
                        y: rect.size.height * 0.5
                    ),
                    radius: (min(rect.width, rect.height) * 0.5),
                    startAngle: .degrees(start),
                    endAngle: .degrees(end),
                    clockwise: true
                )
            }
        }
    }
}

#if DEBUG

extension SpinnerView {
    init(style: SpinnerView.Style, spinState: SpinnerView.SpinState) {
        self.style = style
        let initialState = SpinnerView.ViewState(spinState: spinState, timer: .none)
        self._state = .init(initialValue: initialState)
    }
}

struct SpinnerView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SpinnerView(style: .large, spinState: .expand)
            SpinnerView(style: .small, spinState: .expand)
        }
        .padding(16.0)
        .previewLayout(.sizeThatFits)
    }
}

#endif
