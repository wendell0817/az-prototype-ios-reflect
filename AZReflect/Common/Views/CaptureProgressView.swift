//
//  CaptureProgressView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 11/30/21.
//

import SwiftUI

struct CaptureProgressView: View {
    let style: Style
    let elapsedTime: TimeInterval
    let progress: Double
    
    static var secondsFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = 2
        return formatter
    }
    
    var secondsText: String {
        Self.secondsFormatter.string(for: elapsedTime.clockTime.seconds) ?? ""
    }
    
    var body: some View {
        HStack(spacing: 16.0) {
            Text(style.title)
                .themeFont(.captionSemiBold)
            
            Text("0:\(secondsText)")
                .themeFont(.captureCountdownBodyBold)
                .frame(width: 50.0)
            
            CaptureProgressView.ProgressBarView(value: progress)
        }
        .foregroundColor(style.foregroundColor)
        .padding(.horizontal, 16.0)
        .frame(height: 32.0)
        .background(style.backgroundView())
    }
}

extension CaptureProgressView {
    enum Style {
        case scan
        case record
        
        var title: String {
            switch self {
            case .scan: return "SCANNING"
            case .record: return "RECORDING"
            }
        }
        
        var foregroundColor: Color {
            switch self {
            case .scan: return Color.blue
            case .record: return Color.white
            }
        }
        
        @ViewBuilder func backgroundView() -> some View {
            switch self {
            case .scan:
                BlurView(style: .light)
                    .mask(RoundedRectangle(cornerRadius: 8.0))
            case .record:
                RoundedRectangle(cornerRadius: 8.0)
                    .fill(Theme.color(.alert))
            }
        }
    }
}

extension CaptureProgressView {
    struct ProgressBarView: View {
        let value: Double
        
        var body: some View {
            GeometryReader { proxy in
                ZStack {
                    RoundedRectangle(cornerRadius: 3.0)
                        .fill(Color.black.opacity(0.15))
                    RoundedRectangle(cornerRadius: 3.0)
                        .frame(width: proxy.size.width * value)
                        .frame(width: proxy.size.width, alignment: .leading)
                }
            }
            .frame(height: 6.0)
        }
    }
}

struct CaptureProgressView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CaptureProgressView(
                style: .scan,
                elapsedTime: 30.0,
                progress: 0.5
            )
            
            CaptureProgressView(
                style: .record,
                elapsedTime: 30.0,
                progress: 0.5
            )
        }
        .padding(16.0)
        .previewLayout(.sizeThatFits)
    }
}
