//
//  ReflectLogoView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/3/21.
//

import SwiftUI

struct ReflectLogoView<S: ShapeStyle>: View {
    enum Style {
        case large
        case small
    }
    
    let fill: S
    let style: Style
    
    private let logoSizeRatio = SizeRatio(width: 775.0, height: 170.0)

    var body: some View {
        viewForStyle()
    }
    
    @ViewBuilder func viewForStyle() -> some View {
        switch style {
        case .large:
            Rectangle()
                .fill(fill)
                .mask(
                    Image("reflect.logo.black")
                        .resizable()
                        .scaledToFit()
                )
        case .small:
            HStack(spacing: 8.0) {
                Circle()
                    .fill(
                        LinearGradient(
                            gradient: .init(
                                colors: [
                                    Theme.color(.accentLight),
                                    Theme.color(.accentDark),
                                ]
                            ),
                            startPoint: .leading,
                            endPoint: .trailing
                        )
                    )
                    .frame(width: 20.0, height: 20.0)
                
                Rectangle()
                    .fill(fill)
                    .mask(
                        Image("reflect.logo.black")
                            .resizable()
                            .scaledToFit()
                    )
                    .frame(width: logoSizeRatio.width(for: 24.0))
            }
            .frame(height: 24.0)
        }
    }
}

extension ReflectLogoView where S == Color {
    init(_ fill: S, style: Style = .large) {
        self.fill = fill
        self.style = style
    }
}

extension ReflectLogoView where S == LinearGradient {
    init(style: Style = .large) {
        self.fill = LinearGradient(
            gradient: .init(
                colors: [
                    Theme.color(.accentLight),
                    Theme.color(.accentDark),
                ]
            ),
            startPoint: .leading,
            endPoint: .trailing
        )
        self.style = style
    }
    
    init(_ fill: S, style: Style = .large) {
        self.fill = fill
        self.style = style
    }
}

#if DEBUG

struct ReflectLogoView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            ReflectLogoView(style: .large)
                .frame(height: 120.0)
            ReflectLogoView(fill: Color.black, style: .small)
        }
        .padding(16.0)
        .previewLayout(.sizeThatFits)
    }
}

#endif
