//
//  ProfileImageView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 10/21/21.
//

import SwiftUI

struct ProfileImageView: View {
    let name: String
    
    private var textSymbol: String {
        guard let firstCharacter = name.first else { return "" }
        return "\(firstCharacter.uppercased())"
    }
    
    var body: some View {
        GeometryReader { proxy in
            Text(textSymbol)
                .frame(size: proxy.size)
                .background(
                    Theme.color(.accentDark)
                )
                .mask(Circle())
        }
    }
}

extension ProfileImageView {
    init(member: ShareGroup.Member) {
        self.name = member.name
    }
}

#if DEBUG

struct ProfileImageView_Previews: PreviewProvider {
    static let member = ShareGroup.Member(
        id: "100",
        name: "Wendell",
        email: "wendell.thompson@astrazeneca.com"
    )
    
    static var previews: some View {
        VStack {
            ProfileImageView(member: member)
                .themeFont(.title)
                .foregroundColor(.white)
                .frame(width: 44.0, height: 44.0)
        }
    }
}

#endif
