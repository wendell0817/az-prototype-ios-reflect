//
//  ContactSearchView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 10/25/21.
//

import SwiftUI
import Contacts

struct ContactSearchView: View {
    struct ViewState {
        var searchText = ""
        var animation: Animation? = .none
    }
    
    let contactSelected: (ContactsState.Contact) -> Void
    
    @StateObject private var contactsStore: ContactsStore
    @State private var viewState = ViewState()
    
    init(
        _ state: ContactsState = .init(),
        contactSelected: @escaping (ContactsState.Contact) -> Void = { _ in }
    ) {
        let store = ContactsStore(state)
        self._contactsStore = .init(wrappedValue: store)
        self.contactSelected = contactSelected
    }
    
    var body: some View {
        VStack {
            TextField.init("Search Contacts", text: $viewState.searchText)
                .themeFont(.actionCircle)
                .textFieldStyle(.roundedBorder)
                .padding(.horizontal, 20.0)
            
            List {
                Section {
                    ForEach(contactsStore.state.contacts) { contact in
                        Button(
                            action: { contactSelected(contact) },
                            label: { ContactSearchView.ContactRowView(contact: contact) }
                        )
                    }
                }
            }
        }
        .listStyle(.plain)
        .animation(viewState.animation, value: contactsStore.state.contacts)
        .padding(.top, 40.0)
        .background(Theme.color(.background).edgesIgnoringSafeArea(.all))
        .onChange(of: viewState.searchText) {
            contactsStore.loadContacts(for: $0)
        }
        .onAppear {
            contactsStore.loadContacts(for: viewState.searchText)
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                viewState.animation = .default
            }
        }
    }
}

extension ContactSearchView {
    struct ContactRowView: View {
        let contact: ContactsState.Contact
        
        var displayName: String {
            return [
                contact.givenName,
                contact.familyName
            ].joined(separator: " ")
        }
        
        var body: some View {
            HStack {
                ZStack {
                    if let image = contact.image {
                        Image(uiImage: image)
                            .resizable()
                            .scaledToFill()
                    } else {
                        ProfileImageView(name: contact.givenName)
                            .foregroundColor(.white)
                    }
                }
                .frame(width: 32.0, height: 32.0)
                .mask(Circle())
                
                VStack(alignment: .leading) {
                    Text(displayName)
                        .themeFont(.body)
                    
                    if let email = contact.email {
                        Text(email)
                            .themeFont(.caption)
                    }
                }
                
                Spacer()
            }
            .frame(height: 44.0)
        }
    }
}

struct ContactSearchView_Previews: PreviewProvider {
    static var previewState = ContactsState(
        contacts: [
            .init(
                id: "100",
                imageData: .none,
                givenName: "Wendell",
                familyName: "Thompson",
                email: "wendell.thompson@astrazeneca.com"
            ),
            .init(
                id: "101",
                imageData: .none,
                givenName: "Jeremy",
                familyName: "Wilt",
                email: "jeremy.wilt@astrazeneca.com"
            )
        ]
    )
    
    static var previews: some View {
        ContactSearchView(previewState, contactSelected: { _ in })
            .environment(\.colorScheme, .dark)
    }
}
