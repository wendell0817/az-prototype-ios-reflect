//
//  SymptomSelectorView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 11/16/21.
//

import SwiftUI

struct SymptomSelectorView: View {
    struct ViewState {
        var searchText = ""
    }
    
    @StateObject private var store: SymptomFilterStore
    @State private var viewState = ViewState()
    
    let symptomSelected: (Symptom) -> Void
    
    init(excludedSymptoms: [Symptom], symptomSelected: @escaping (Symptom) -> Void) {
        let store = SymptomFilterStore(.init(), excludedSymptoms: excludedSymptoms)
        self._store = .init(wrappedValue: store)
        self.symptomSelected = symptomSelected
    }
    
    var body: some View {
        VStack(spacing: 32.0) {
            RoundedRectangle(cornerRadius: 1.0)
                .fill(Theme.color(.textCaption))
                .frame(width: 24.0, height: 2.0)
            
            Text("Did we miss a symptom?")
                .themeFont(.captureCountdownBodyBold)
                .foregroundColor(Theme.color(.textCaption))
            
            VStack {
                TextField(
                    "Search Symptoms",
                    text: $viewState.searchText
                )
                .themeFont(.body)
            }
            .padding(8.0)
            .background(
                RoundedRectangle(cornerRadius: 8.0)
                    .stroke(Theme.color(.accentDark), lineWidth: 2.0)
            )
            
            GeometryReader { proxy in
                ScrollView {
                    VStack(spacing: 16.0) {
                        ForEach(store.state.symptoms) { symptom in
                            Button(
                                action: { symptomSelected(symptom) },
                                label: { SymptomSelectorView.SymptomView(symptom: symptom) }
                            )
                        }
                    }
                    .animation(.easeInOut, value: store.state.symptoms)
                    .frame(width: proxy.size.width)
                }
            }
        }
        .padding(.top, 8.0)
        .padding(.horizontal, 20.0)
        .background(Theme.color(.backgroundInstruction).ignoresSafeArea())
        .onChange(of: viewState.searchText, perform: store.search)
    }
}

extension SymptomSelectorView {
    struct SymptomView: View {
        let symptom: Symptom
        
        var body: some View {
            HStack {
                HStack {
                    Text(symptom.title)
                        .themeFont(.action)
                        .foregroundColor(Theme.color(.accentDark))
                    
                    Image(systemName: "plus")
                        .themeFont(.action)
                        .foregroundColor(Theme.color(.accept))
                }
                .padding(.vertical, 8.0)
                .padding(.horizontal, 16.0)
                .background(
                    RoundedRectangle(cornerRadius: 8.0)
                        .stroke(Theme.color(.accentDark), lineWidth: 2)
                        .padding(1.0)
                )
                
                Spacer()
            }
        }
    }
}

#if DEBUG

// MARK: Previews

struct SymptomSelectorView_Previews: PreviewProvider {
    static var previews: some View {
        SymptomSelectorView(
            excludedSymptoms: [],
            symptomSelected: { _ in }
        )
    }
}

#endif
