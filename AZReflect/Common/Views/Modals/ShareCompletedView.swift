//
//  ShareCompletedView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 10/27/21.
//

import SwiftUI

struct ShareCompletedView: View {
    let finishTapped: () -> Void
    
    @State private var present = false
    
    init(finishTapped: @escaping () -> Void = {}) {
        self.finishTapped = finishTapped
    }
    
    var body: some View {
        VStack(spacing: 0.0) {
            Image(systemName: "checkmark")
                .font(.system(size: 44.0))
                .scaleEffect(present ? 1.0 : 0.001, anchor: .center)
                .rotationEffect(present ? Angle(degrees: 0.0) : Angle(degrees: -90.0), anchor: .center)
                .animation(.spring(), value: present)
                .padding(.top, 96.0)
            
            Text("Recording Shared!")
                .themeFont(.titleBold)
                .padding(.top, 48.0)
            
            Text("The people you invited will be notified that they can view your reflection. We’ll let you know when they open it!"
            )
            .themeFont(.body)
            .padding(.top, 16.0)
            
            Spacer()
            
            Button("Finish", action: finishTapped)
                .themeButtonStyle(.cta)
                .disabled(!present)
        }
        .foregroundColor(.white)
        .multilineTextAlignment(.center)
        .padding(.horizontal, 20.0)
        .padding(.bottom, 32.0)
        .background(
            Color.white.overlay(
                Image("reflection.card.background")
                    .resizable()
                    .scaledToFill()
                    .modifier(Theme.ViewModifier.reflectGradient)
                    .blendMode(.multiply)
            )
            .edgesIgnoringSafeArea(.all)
        )
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                present = true
            }
        }
    }
}

struct ShareCompletedView_Previews: PreviewProvider {
    static var previews: some View {
        ShareCompletedView()
            .environment(\.colorScheme, .dark)
    }
}
