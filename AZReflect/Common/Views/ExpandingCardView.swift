//
//  ExpandingCardView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 9/1/21.
//

import SwiftUI

struct ExpandingCardView<Content: View, Background: View, Accessory: View>: View {
    let heightRange: ClosedRange<CGFloat>
    let content: () -> Content
    let background: () -> Background
    let accessory: () -> Accessory
    @GestureState private var dragState: DragState = .none
    @State private var contentHeight: CGFloat
    
    private static var gripperHeight: CGFloat { 20.0 }
    private static var coordinateSpaceName: String { "ExpandingCardView" }
    private static var swipeTolerance: CGFloat { 500.0 }
    private static var accessoryHeight: CGFloat { 40.0 }
    
    private var animation: Animation? {
        switch dragState {
        case .none:
            return .spring()
        case .dragging:
            return .none
        }
    }
    
    init(
        heightRange: ClosedRange<CGFloat>,
        content: @escaping () -> Content,
        background: @escaping () -> Background,
        accessory: @escaping () -> Accessory
    ) {
        let offsetRangeLower = heightRange.lowerBound + Self.gripperHeight
        let offsetRangeUpper = heightRange.upperBound + Self.gripperHeight
        let offsetRange = offsetRangeLower ... offsetRangeUpper
        
        self.heightRange = offsetRange
        self.content = content
        self.background = background
        self.accessory = accessory
        self._contentHeight = .init(initialValue: offsetRange.upperBound)
    }
    
    var gripperView: some View {
        HStack {
            Spacer()
            RoundedRectangle(cornerRadius: 2.0)
                .frame(width: 44.0, height: 4.0)
            Spacer()
        }
        .frame(height: Self.gripperHeight)
        .contentShape(Rectangle())
    }
    
    var body: some View {
        GeometryReader { proxy in
            VStack(spacing: 0.0) {
                accessory()
                    .frame(height: Self.accessoryHeight)
                    .offset(y: -Self.accessoryHeight)
                    .padding(.bottom, -Self.accessoryHeight)
                
                gripperView
                    .gesture(gripperGesture(maxHeight: proxy.size.height))
                    .zIndex(2.0)
                
                GeometryReader { contentProxy in
                    content()
                        .frame(height: contentProxy.size.height, alignment: .bottom)
                }
                .clipped()
                .zIndex(1.0)
            }
            .frame(height: contentHeight)
            .background(background())
            .animation(animation, value: contentHeight)
            .frame(height: proxy.size.height, alignment: .bottom)
        }
        .coordinateSpace(name: Self.coordinateSpaceName)
    }
    
    private func gripperGesture(maxHeight: CGFloat) -> some Gesture {
        let dragGesture = DragGesture(minimumDistance: 0.0, coordinateSpace: .named(Self.coordinateSpaceName))
            .updating($dragState) { value, state, transaction in
                state = .dragging
            }
            .onChanged { value in
                let offsetLocationY = value.location.y - (Self.gripperHeight * 0.5)
                let targetHeight = maxHeight - offsetLocationY
                contentHeight = heightRange.clamp(targetHeight)
            }
            .onEnded { value in
                let startLocationY = value.location.y - (Self.gripperHeight * 0.5)
                let endLocationY = value.predictedEndLocation.y - (Self.gripperHeight * 0.5)
                let velocity = endLocationY - startLocationY
                let swipeDetected = abs(velocity) > Self.swipeTolerance
                let isExpanded: Bool
                
                switch swipeDetected {
                case true:
                    isExpanded = velocity < 0.0
                case false:
                    let targetHeight = maxHeight - endLocationY
                    isExpanded = targetHeight >= heightRange.middle
                }
                
                switch isExpanded {
                case true:
                    contentHeight = heightRange.upperBound
                default:
                    contentHeight = heightRange.lowerBound
                }
            }
        
        let tapGesture = TapGesture(count: 1)
            .onEnded {
                switch contentHeight >= heightRange.middle {
                case true:
                    contentHeight = heightRange.lowerBound
                default:
                    contentHeight = heightRange.upperBound
                }
            }
        
        return tapGesture.exclusively(before: dragGesture)
    }
}
extension ExpandingCardView where Accessory == EmptyView {
    init(
        heightRange: ClosedRange<CGFloat>,
        content: @escaping () -> Content,
        background: @escaping () -> Background
    ) {
        self = .init(
            heightRange: heightRange,
            content: content,
            background: background,
            accessory: { EmptyView() }
        )
    }
}

extension ExpandingCardView {
    enum DragState {
        case none
        case dragging
    }
}

struct ExpandingCardView_Previews: PreviewProvider {
    static var previews: some View {
        ExpandingCardView(
            heightRange: 100.0 ... 500.0,
            content: {
                Button("Expanding Card View", action: {})
                    .themeButtonStyle(.cta)
                    .padding(.horizontal, 20.0)
                    .frame(height: 100.0)
            },
            background: {
                CardShape(cornerRadius: 28.0)
                    .fill(Theme.color(.captureBackgroundCard))
                    .edgesIgnoringSafeArea(.bottom)
            }
        )
        .background(Theme.color(.accentDark).ignoresSafeArea())
    }
}
