//
//  FieldView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import SwiftUI

struct FieldView: View {
    let isSecure: Bool
    let prompt: String
    let title: String
    @Binding var text: String
    let isValid: Bool
    
    init(
        isSecure: Bool = false,
        prompt: String,
        title: String? = .none,
        text: Binding<String>,
        isValid: Bool
    ) {
        self.isSecure = isSecure
        self.prompt = prompt
        self.title = title ?? prompt
        self._text = text
        self.isValid = isValid
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(prompt)
                .themeFont(.action)
            
            HStack {
                textView()
                    .font(Theme.Font.custom(size: 14.0, weight: .regular))
                    .foregroundColor(Theme.color(.textBody))
                
                Image(systemName: "checkmark")
                    .foregroundColor(Theme.color(.accept))
                    .frame(width: 22.0)
                    .opacity(isValid ? 1.0 : 0.0)
            }
            .padding(.horizontal, 16.0)
            .frame(height: 52.0)
            .background(
                RoundedRectangle(cornerRadius: 12.0)
                    .fill(Theme.color(.textBodyInverse))
                    .overlay(
                        RoundedRectangle(cornerRadius: 12.0)
                            .stroke(Theme.color(.backgroundBorder), lineWidth: 1.0)
                    )
                    .shadow(color: Color.black.opacity(0.04), radius: 4.0, x: 0.0, y: 4.0)
            )
        }
    }
    
    @ViewBuilder private func textView() -> some View {
        switch isSecure {
        case true:
            SecureField(title, text: $text)
        default:
            TextField(title, text: $text)
        }
    }
}

#if DEBUG

struct FieldView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Spacer()
            
            FieldView(
                prompt: "What should we call you?",
                title: "Preferred Name",
                text: .constant("Preferred Name"),
                isValid: true
            )
            
            FieldView(
                prompt: "What should we call you?",
                title: "Preferred Name",
                text: .constant(""),
                isValid: false
            )
            
            FieldView(
                isSecure: true,
                prompt: "What should we call you?",
                title: "Preferred Name",
                text: .constant("Password"),
                isValid: true
            )
            
            Spacer()
        }
        .padding()
        .background(Theme.color(.background).ignoresSafeArea())
    }
}

#endif
