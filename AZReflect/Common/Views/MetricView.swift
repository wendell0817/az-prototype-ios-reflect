//
//  MetricView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 9/1/21.
//

import SwiftUI
import Combine

struct MetricView: View {
    let symbol: String
    let value: Double?
    let unitText: String
    let foregroundColor: Color
    let animated: Bool
    
    @State private var subscription: AnyCancellable? = .none
    @State private var animatedDot = 0
    private let dotCount = 3
    
    init(
        symbol: String,
        value: Double?,
        unitText: String,
        foregroundColor: Color = Theme.color(.textBody),
        animated: Bool = true
    ) {
        self.symbol = symbol
        self.value = value
        self.unitText = unitText
        self.foregroundColor = foregroundColor
        self.animated = animated
    }
    
    var body: some View {
        HStack(alignment: .lastTextBaseline, spacing: 0.0) {
            Image(systemName: symbol)
                .themeFont(.bodyBold)
                .foregroundColor(Color.accentColor)
                .frame(width: 20.0)
            
            ZStack {
                if animated && value == .none {
                    ellipseView()
                        .frame(width: 24.0, height: 16.0)
                        .opacity(value == .none ? 1.0 : 0.0)
                        .onAppear(perform: startTimer)
                        .onDisappear(perform: stopTimer)
                }
                
                Text(value: value, format: .metricWholeNumber)
                    .foregroundColor(foregroundColor)
                    .animation(.none)
                    .opacity(value == .none ? 0.0 : 1.0)
                    .frame(height: 24.0, alignment: .bottom)
            }
            .animation(.easeInOut, value: value)
            .themeFont(.bodyBold)
            .frame(width: 38.0)
            
            Text(unitText)
                .foregroundColor(foregroundColor)
                .opacity(0.5)
                .themeFont(.subtitle)
                .frame(width: 38.0, alignment: .bottomLeading)
        }
        .frame(height: 24.0)
    }
    
    private func ellipseView() -> some View {
        GeometryReader { proxy in
            HStack(spacing: 0.0) {
                ForEach(0 ..< dotCount, id: \.self) { dot in
                    Circle()
                        .fill(foregroundColor)
                        .frame(width: 4.0, height: 4.0)
                        .offset(y: dot == animatedDot ? -(proxy.size.height - 8.0) : 0.0)
                        .animation(Animation.easeOut(duration: 0.5), value: animatedDot)
                        .frame(
                            width: proxy.size.width / CGFloat(dotCount),
                            height: proxy.size.height,
                            alignment: .bottom
                        )
                }
            }
            .frame(width: proxy.size.width)
        }
    }
    
    private func startTimer() {
        subscription = Timer.publish(
            every: Double(dotCount) * 1.0,
            on: .main,
            in: .default
        )
        .autoconnect()
        .sink { _ in
            animatedDot = -1
            (0 ..< dotCount + 1).forEach { dot in
                let delay = Double(dot) * 0.25
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(delay)) {
                    animatedDot += 1
                }
            }
        }
    }
    
    private func stopTimer() {
        subscription?.cancel()
    }
}

struct MetricView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            HStack {
                MetricView(symbol: "heart", value: .none, unitText: "bpm")
                Spacer()
                MetricView(symbol: "drop", value: 888, unitText: "sp02")
                Spacer()
                MetricView(symbol: "wind", value: 88, unitText: "rpm")
            }
            .padding(20.0)
            .background(Theme.color(.backgroundInstruction))
            .environment(\.colorScheme, .light)
            
            HStack {
                MetricView(symbol: "heart", value: .none, unitText: "bpm")
                Spacer()
                MetricView(symbol: "drop", value: 888, unitText: "sp02")
                Spacer()
                MetricView(symbol: "wind", value: 88, unitText: "rpm")
            }
            .padding(20.0)
            .background(Theme.color(.backgroundInstruction))
            .environment(\.colorScheme, .dark)
        }
        .accentColor(Theme.color(.accept))
        .previewLayout(.sizeThatFits)
        
    }
}
