//
//  ReflectSegmentedView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/4/21.
//

import SwiftUI

struct ReflectSegmentedView<Data: RandomAccessCollection, ID: Hashable, Content: View>: View {
    let data: Data
    let id: KeyPath<Data.Element, ID>
    @Binding var selection: Data.Element
    let content: (Data.Element) -> Content
    
    var body: some View {
        HStack(spacing: 0.0) {
            ForEach(data, id: id) { element in
                Button(
                    action: { selection = element },
                    label: {
                        GeometryReader { proxy in
                            content(element)
                                .themeFont(.action)
                                .foregroundColor(
                                    isSelected(element)
                                        ? Color.white
                                        : Theme.color(.backgroundInverse)
                                )
                                .frame(size: proxy.size)
                        }
                        .background(background(for: element))
                    }
                )
            }
            .buttonStyle(PlainButtonStyle())
        }
        .frame(height: 52.0)
        .mask(RoundedRectangle(cornerRadius: 12.0))
        .shadow(color: Color.black.opacity(0.04), radius: 6.0, x: 0.0, y: 4.0)
    }
    
    private func isSelected(_ element: Data.Element) -> Bool {
        element[keyPath: id] == selection[keyPath: id]
    }
    
    @ViewBuilder private func background(for element: Data.Element) -> some View {
        let isSelected = isSelected(element)
        switch isSelected {
        case true:
            LinearGradient(
                gradient: .init(
                    colors: [
                        Theme.color(.accentLight),
                        Theme.color(.accentDark)
                    ]
                ),
                startPoint: .leading,
                endPoint: .trailing
            )
        default:
            Theme.color(.background)
        }
    }
}

extension ReflectSegmentedView where Data.Element: Identifiable, Data.Element.ID == ID {
    init(data: Data, selection: Binding<Data.Element>, content: @escaping (Data.Element) -> Content) {
        self.data = data
        self.id = \.id
        self._selection = selection
        self.content = content
    }
}

#if DEBUG

struct ReflectSegmentedView_Previews: PreviewProvider {
    static let previewData = [0, 1]
    @State static var selectedData = 1
    
    static var previews: some View {
        ReflectSegmentedView(
            data: previewData,
            id: \.self,
            selection: $selectedData
        ) { data in
            Text("\(data)")
        }
        .padding()
        .background(Theme.color(.backgroundInstruction))
    }
}

#endif
