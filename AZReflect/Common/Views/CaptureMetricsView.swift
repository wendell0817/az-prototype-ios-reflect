//
//  CaptureMetricsView.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 12/1/21.
//

import SwiftUI

struct CaptureMetricsView: View {
    let metrics: CaptureState.MetricsDisplay
    private let spacing: CGFloat = 16.0
    
    var body: some View {
        VStack(spacing: spacing) {
            HStack(spacing: 0.0) {
                MetricView(
                    style: .large,
                    title: "Heart Rate",
                    value: metrics.heartRate,
                    subtitle: "BPM"
                )
                
                Divider()
                
                MetricView(
                    style: .large,
                    title: "Respiration",
                    value: metrics.breathingRate,
                    subtitle: "RPM"
                )
                
                Divider()
                
                MetricView(
                    style: .large,
                    title: "Blood Oxygen",
                    value: metrics.oxygenSaturation,
                    subtitle: "SPO2%"
                )
            }
            .frame(height: 112.0)
            .modifier(CaptureMetricsView.CardbackgroundModifier())
        }
    }
}

extension CaptureMetricsView {
    struct MetricView: View {
        enum Style {
            case large
            case small
        }
        
        let style: Style
        let title: String
        let value: Double?
        let subtitle: String?
        
        init(style: Style, title: String, value: Double?, subtitle: String? = .none) {
            self.style = style
            self.title = title
            self.value = value
            self.subtitle = subtitle
        }
        
        var body: some View {
            GeometryReader { proxy in
                styledView()
                    .frame(size: proxy.size)
            }
        }
        
        @ViewBuilder private func styledView() -> some View {
            switch style {
            case .large:
                VStack(spacing: 0.0) {
                    Text(title)
                        .themeFont(.action)
                        .foregroundColor(Theme.color(.accentDark))
                    
                    CaptureMetricsView.MetricValueView(
                        value: value,
                        spinStyle: .large
                    )
                    .themeFont(.captureMetricText)
                    .frame(height: 50.0)
                    
                    if let subtitle = subtitle {
                        Text(subtitle)
                            .kerning(2.0)
                            .foregroundColor(.black)
                            .themeFont(.captionBold)
                    }
                }
            case .small:
                HStack(spacing: 8.0) {
                    Text(title)
                        .themeFont(.action)
                        .foregroundColor(Theme.color(.accentDark))
                    
                    CaptureMetricsView.MetricValueView(
                        value: value,
                        spinStyle: .small
                    )
                    .frame(height: 16.0)
                }
            }
        }
    }
}

extension CaptureMetricsView {
    struct MetricValueView: View {
        let value: Double?
        let spinStyle: SpinnerView.Style
        
        var body: some View {
            ZStack {
                if let value = value {
                    Text(value: value, format: .metricWholeNumber)
                        .foregroundColor(.black)
                } else {
                    SpinnerView(style: spinStyle)
                }
            }
        }
    }
}

extension CaptureMetricsView {
    struct CardbackgroundModifier: ViewModifier {
        func body(content: Content) -> some View {
            content.background(
                BlurView(style: .light)
                    .mask(RoundedRectangle(cornerRadius: 8.0))
            )
        }
    }
}

#if DEBUG

struct CaptureMetricsView_Previews: PreviewProvider {
    static let metrics = CaptureState.MetricsDisplay(
        heartRate: 52.5,
        oxygenSaturation: .none,
        breathingRate: .none
    )
    
    static var previews: some View {
        CaptureMetricsView(metrics: metrics)
            .previewLayout(.sizeThatFits)
    }
}

#endif
