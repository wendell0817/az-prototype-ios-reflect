//
//  Theme.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import Foundation
import SwiftUI

struct Theme {
    static func color(_ themeColor: Theme.Color) -> SwiftUI.Color {
        SwiftUI.Color(themeColor.rawValue)
    }
}

// MARK: Colors

extension Theme {
    enum Color: String, CaseIterable {
        case background = "background"
        case backgroundBorder = "background.border"
        case backgroundButton = "background.button"
        case backgroundCard = "background.card"
        case backgroundHomeCard = "background.home.card"
        case backgroundInstruction = "background.instruction"
        case backgroundInverse = "background.inverse"
        case backgroundRecording = "background.recording"
        case accentLight = "accent.light"
        case accentDark = "accent.dark"
        case textBody = "text.body"
        case textBodyInverse = "text.body.inverse"
        case textCaption = "text.caption"
        case accept = "accept"
        case alert = "alert"
        case warning = "warning"
        case captureBackgroundButton = "capture.background.button"
        case captureBackgroundCard = "capture.background.card"
        case captureText = "capture.text"
        case destructiveLight = "destructive.light"
        case destructiveDark = "destructive.dark"
        case settingsGroup = "settings.group"
        case settingsPrivacy = "settings.privacy"
        case headerBackground = "header.background"
        case headerText = "header.text"
        case progressScan = "progress.scan"
    }
}

// MARK: Fonts

extension Theme {
    enum Font: CaseIterable {
        case action
        case actionCircle
        case body
        case bodyBold
        case title
        case titleBold
        case subtitle
        case caption
        case captionBold
        case captionSemiBold
        case countdown
        case timerTime
        case captureCountdownBody
        case captureCountdownBodyBold
        case captureMetricText
        
        var fontName: String {
            switch self {
            default:
                return "Inter"
            }
        }
        
        var size: CGFloat {
            switch self {
            case .countdown:
                return 200.0
            case .captureMetricText:
                return 42.0
            case .title,
                 .titleBold,
                 .actionCircle:
                return 24.0
            case .timerTime,
                 .captureCountdownBody,
                 .captureCountdownBodyBold:
                return 20.0
            case .body,
                 .bodyBold:
                return 16.0
            case .action,
                 .subtitle:
                return 14.0
            case .caption,
                 .captionBold,
                 .captionSemiBold:
                return 12.0
            }
        }
        
        var weight: SwiftUI.Font.Weight {
            switch self {
            case .action,
                 .titleBold,
                 .bodyBold,
                 .captionBold,
                 .captureCountdownBodyBold,
                 .captureMetricText:
                return .bold
            case .captionSemiBold:
                return .semibold
            case .countdown:
                return .ultraLight
            default:
                return .regular
            }
        }
        
        var swiftUIFont: SwiftUI.Font {
            switch self {
            default:
                return SwiftUI.Font
                    .custom(fontName, size: size)
                    .weight(weight)
            }
        }
    }
}

extension Theme.Font {
    // TODO: Make all referencees to this part of the Theme
    static func custom(size: CGFloat, weight: SwiftUI.Font.Weight) -> SwiftUI.Font {
        return SwiftUI.Font.custom("Inter", size: size).weight(weight)
    }
}

extension View {
    func themeFont(_ themeFont: Theme.Font) -> some View {
        font(themeFont.swiftUIFont)
    }
}

extension Text {
    func themeFont(_ themeFont: Theme.Font) -> Text {
        font(themeFont.swiftUIFont)
    }
}

// MARK: Button Styles

extension Theme {
    enum ButtonStyle: CaseIterable, SwiftUI.ButtonStyle {
        case standard
        case standardLight
        case standardCircle
        case cta
        case ctaCircle
        case destructive
        case captureStandardGradient
        case captureStandardDestructive
        case capturePlay
        case shareAdd
        
        var width: CGFloat? {
            switch self {
            case .ctaCircle,
                 .standardCircle:
                return 60.0
            case .capturePlay:
                return 42.0
            default:
                return .none
            }
        }
        
        var height: CGFloat? {
            switch self {
            case .capturePlay:
                return 42.0
            case .shareAdd:
                return 52.0
            default:
                return 60.0
            }
        }
        
        var foregroundColor: SwiftUI.Color {
            switch self {
            case .cta,
                 .ctaCircle,
                 .destructive,
                 .capturePlay:
                return .white
            case .standardLight:
                return Theme.color(.captureText)
            default:
                return Theme.color(.textBody)
            }
        }
        
        var font: Theme.Font {
            switch self {
            case .standardCircle,
                 .ctaCircle,
                 .capturePlay:
                return .actionCircle
            default:
                return .action
            }
        }
        
        @ViewBuilder private func backgroundContent() -> some View {
            switch self {
            case .standard:
                RoundedRectangle(cornerRadius: 12.0)
                    .fill(Theme.color(.backgroundButton))
            case .standardLight:
                RoundedRectangle(cornerRadius: 12.0)
                    .fill(Theme.color(.captureBackgroundButton))
            case .standardCircle:
                Circle()
                    .fill(Theme.color(.backgroundButton))
            case .cta:
                RoundedRectangle(cornerRadius: 12.0)
                    .fill(
                        LinearGradient(
                            gradient: .init(
                                colors: [
                                    SwiftUI.Color(Theme.Color.accentLight.rawValue),
                                    SwiftUI.Color(Theme.Color.accentDark.rawValue)
                                ]
                            ),
                            startPoint: .leading,
                            endPoint: .trailing
                        )
                    )
            case .ctaCircle:
                Circle()
                    .fill(
                        LinearGradient(
                            gradient: .init(
                                colors: [
                                    SwiftUI.Color(Theme.Color.accentLight.rawValue),
                                    SwiftUI.Color(Theme.Color.accentDark.rawValue)
                                ]
                            ),
                            startPoint: .leading,
                            endPoint: .trailing
                        )
                    )
            case .destructive:
                RoundedRectangle(cornerRadius: 12.0).fill(Theme.color(.alert))
            case .captureStandardGradient,
                 .captureStandardDestructive:
                RoundedRectangle(cornerRadius: 12.0)
                    .fill(Theme.color(.captureBackgroundButton))
            case .capturePlay:
                RoundedRectangle(cornerRadius: 4.0)
                    .fill(SwiftUI.Color.white.opacity(0.5))
            case .shareAdd:
                RoundedRectangle(cornerRadius: 8.0)
                    .fill(Theme.color(.backgroundInstruction))
                    .overlay(
                        RoundedRectangle(cornerRadius: 8.0)
                            .stroke(Theme.color(.backgroundBorder), lineWidth: 1.0)
                    )
            }
        }
        
        @ViewBuilder func labelView(
            for label: ButtonStyleConfiguration.Label
        ) -> some View {
            switch self {
            case .captureStandardGradient:
                label.themeModifier(.reflectGradient)
            case .captureStandardDestructive:
                label.themeModifier(.destructiveGradient)
            case .shareAdd:
                HStack {
                    label.foregroundColor(foregroundColor)
                        .foregroundColor(Theme.color(.textCaption))
                        .themeFont(.body)
                    Spacer()
                    
                    Image(systemName: "plus")
                        .font(.system(size: 20.0))
                }
                .padding(.horizontal, 16.0)
            default:
                label.foregroundColor(foregroundColor)
            }
        }
        
        func makeBody(configuration: Configuration) -> some View {
            ButtonStyleView(config: configuration, style: self)
        }
    }
}

extension View {
    func themeButtonStyle(_ themeButtonStyle: Theme.ButtonStyle) -> some View {
        buttonStyle(themeButtonStyle)
    }
}

extension Theme.ButtonStyle {
    struct ButtonStyleView: View {
        let config: Configuration
        let style: Theme.ButtonStyle
        @Environment(\.isEnabled) private var isEnabled
        
        var body: some View {
            GeometryReader { proxy in
                style.labelView(for: config.label)
                    .font(style.font.swiftUIFont)
                    .frame(size: proxy.size)
            }
            .frame(width: style.width, height: style.height)
            .background(
                style.backgroundContent()
                    .shadow(
                        color: SwiftUI.Color.black.opacity(0.05),
                        radius: 4.0, x: 0.0, y: 4.0
                    )
            )
            .scaleEffect(config.isPressed ? 0.99 : 1.0, anchor: .center)
            .opacity(config.isPressed ? 0.5 : 1.0)
            .opacity(isEnabled ? 1.0 : 0.5)
        }
    }
}

// MARK: View Modifiers

extension Theme {
    enum ViewModifier: SwiftUI.ViewModifier, CaseIterable {
        case reflectGradient
        case destructiveGradient
        case symptomPill
        
        func body(content: Content) -> some View {
            modifiedView(for: content)
        }
        
        @ViewBuilder private func modifiedView(for content: Content) -> some View {
            switch self {
            case .reflectGradient:
                content
                    .foregroundColor(.clear)
                    .overlay(
                        Rectangle()
                            .fill(
                                LinearGradient(
                                    gradient: .init(
                                        colors: [
                                            Theme.color(.accentLight),
                                            Theme.color(.accentDark),
                                        ]
                                    ),
                                    startPoint: .leading,
                                    endPoint: .trailing
                                )
                            )
                            .mask(content)
                    )
            case .destructiveGradient:
                content
                    .foregroundColor(.clear)
                    .overlay(
                        Rectangle()
                            .fill(
                                LinearGradient(
                                    gradient: .init(
                                        colors: [
                                            Theme.color(.destructiveLight),
                                            Theme.color(.destructiveDark),
                                        ]
                                    ),
                                    startPoint: .leading,
                                    endPoint: .trailing
                                )
                            )
                            .mask(content)
                    )
            case .symptomPill:
                content
                    .background(
                        GeometryReader { proxy in
                            RoundedRectangle(cornerRadius: proxy.size.height * 0.5)
                                .themeModifier(.reflectGradient)
                                .overlay(
                                    RoundedRectangle(cornerRadius: proxy.size.height * 0.45)
                                        .fill(SwiftUI.Color.black.opacity(0.75))
                                        .padding(2.0)
                                )
                        }
                    )
            }
        }
    }
}

extension View {
    func themeModifier(_ themeModifier: Theme.ViewModifier) -> some View {
        modifier(themeModifier)
    }
}

// MARK: Previews

#if DEBUG

extension Theme.Font {
    var previewText: String {
        switch self {
        case .action: return "Action"
        case .actionCircle: return "Action Circle"
        case .body: return "Body"
        case .bodyBold: return "Body Bold"
        case .title: return "Title"
        case .titleBold: return "Title Bold"
        case .subtitle: return "Subtitle"
        case .caption: return "Caption"
        case .captionBold: return "Caption Bold"
        case .captionSemiBold: return "Caption Semi Bold"
        case .countdown: return "99"
        case .timerTime: return "Timer Time 00:00"
        case .captureCountdownBody: return "Capture Countdown Body"
        case .captureCountdownBodyBold: return "Capture Countdown Body Bold"
        case .captureMetricText: return "Capture Metric"
        }
    }
}

extension Theme.ButtonStyle {
    @ViewBuilder func previewContent() -> some View {
        switch self {
        case .standard:
            Text("Standard")
        case .standardLight:
            Text("Standard Light")
        case .standardCircle:
            Image(systemName: "arrow.left")
        case .cta:
            Text("Call To Action")
        case .ctaCircle:
            Image(systemName: "checkmark")
        case .destructive:
            Text("Destructive")
        case .captureStandardGradient:
            Text("Capture Standard Gradient")
        case .captureStandardDestructive:
            Text("Capture Standard Destructive")
        case .capturePlay:
            Image(systemName: "play")
        case .shareAdd:
            Text("Add Contact")
        }
    }
}

extension Theme.ViewModifier {
    @ViewBuilder func previewContent() -> some View {
        switch self {
        case .reflectGradient:
            Text("Reflect Gradient").themeFont(.titleBold)
        case .destructiveGradient:
            Text("Destructive Gradient").themeFont(.titleBold)
        case .symptomPill:
            Text("Symptom Pill")
                .foregroundColor(SwiftUI.Color.white)
                .padding(.horizontal, 16.0)
                .frame(height: 32.0)
        }
    }
}

struct Theme_Previews: PreviewProvider {
    enum PreviewSection: Identifiable {
        case colors(ColorScheme)
        case fonts
        case buttonStyles
        case modifiers
        
        var id: String { title }
        
        var title: String {
            switch self {
            case .colors(let colorScheme):
                switch colorScheme {
                case .dark: return "Colors - Dark"
                case .light: return "Colors - Light"
                default: return ""
                }
            case .fonts: return "Fonts"
            case .buttonStyles: return "Button Styles"
            case .modifiers: return "Modifiers"
            }
        }
    }
    
    static let sections: [PreviewSection] = [
        .colors(.light),
        .colors(.dark),
        .fonts,
        .buttonStyles,
        .modifiers
    ]
    
    static var previews: some View {
        Group {
            ForEach(sections) { section in
                content(for: section)
                    .previewDisplayName(section.title)
            }
        }
        .previewLayout(.sizeThatFits)
    }
    
    @ViewBuilder static func content(for section: PreviewSection) -> some View {
        switch section {
        case .colors(let colorScheme):
            VStack {
                ForEach(Theme.Color.allCases, id: \.self) { themeColor in
                    HStack {
                        Circle().fill(Theme.color(themeColor))
                            .frame(width: 40.0, height: 40.0)
                            .overlay(
                                Circle()
                                    .stroke(Color.gray.opacity(0.5), lineWidth: 2)
                            )
                        Text("\(themeColor.rawValue)")
                        Spacer()
                    }
                    .frame(height: 44.0)
                }
            }
            .padding(16.0)
            .background(Theme.color(.background))
            .environment(\.colorScheme, colorScheme)
        case .fonts:
            VStack {
                ForEach(Theme.Font.allCases, id: \.self) { themeFont in
                    HStack {
                        Text(themeFont.previewText)
                        Spacer()
                    }
                    .themeFont(themeFont)
                }
            }
            .padding(16.0)
        case .buttonStyles:
            VStack(spacing: 16.0) {
                ForEach(Theme.ButtonStyle.allCases, id:\.self) { themeButtonStyle in
                    Button(
                        action: { },
                        label: { themeButtonStyle.previewContent() }
                    )
                    .themeButtonStyle(themeButtonStyle)
                }
            }
            .padding(16.0)
            .background(Theme.color(.backgroundInstruction))
        case .modifiers:
            VStack(spacing: 16.0) {
                ForEach(Theme.ViewModifier.allCases, id: \.self) { themeViewModifier in
                    HStack {
                        Spacer()
                        themeViewModifier.previewContent()
                            .themeModifier(themeViewModifier)
                        Spacer()
                    }
                }
            }
            .padding(16.0)
            .background(Theme.color(.backgroundInstruction))
        }
    }
}

#endif
