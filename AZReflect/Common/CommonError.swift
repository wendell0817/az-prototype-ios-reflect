//
//  CommonError.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/23/21.
//

import Foundation

enum CommonError: Error {
    case message(String)
}
