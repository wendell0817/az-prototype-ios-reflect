//
//  Config.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/23/21.
//

import Foundation

struct Config {
    enum Key: String {
        case binahLicenseKey = "BINAH_LICENSE_KEY"
    }
    
    static func value(for key: Key) throws -> String {
        guard let stringValue = Bundle.main.object(forInfoDictionaryKey: key.rawValue) as? String
        else { throw CommonError.message("[CONFIG] key not found:\(key.rawValue)") }
        
        return stringValue
    }
}
