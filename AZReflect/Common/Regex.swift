//
//  Regex.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/3/21.
//

import Foundation

enum Regex: Hashable {
    case email
    case url
    
    static var cache: [Regex: NSRegularExpression] = [:]
}

extension Regex {
    var pattern: String {
        switch self {
        case .email:
            /*
             ^
             == USER NAME ==
             ([a-zA-Z0-9]{1,64}|[a-zA-Z0-9]{1}[a-zA-Z0-9+\.-/!%_]{1,62}[a-zA-Z0-9]{1})
             == AT ==
             (@)
             == DOMAIN NAME ==
             ([a-zA-Z0-9]{1,64}|[a-zA-Z0-9]{1}[a-zA-Z0-9+\.-]{1,62}[a-zA-Z0-9]{1})
             == DOT ==
             (\.)
             == DOMAIN ==
             ([a-zA-Z]{2,7})
             $
             */
            return "^([a-zA-Z0-9]{1,64}|[a-zA-Z0-9]{1}[a-zA-Z0-9+\\.-/!%_]{1,62}[a-zA-Z0-9]{1})(@)([a-zA-Z0-9]{1,64}|[a-zA-Z0-9]{1}[a-zA-Z0-9+\\.-]{1,62}[a-zA-Z0-9]{1})(\\.)([a-zA-Z]{2,7})$"
        case .url:
            return "[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)"
        }
    }
    
    var options: NSRegularExpression.Options {
        switch self {
        case .email,
             .url:
            return [.caseInsensitive]
        }
    }
    
    func nsRegex() throws -> NSRegularExpression {
        return try .init(pattern: pattern, options: options)
    }
    
    func cachedRegex() throws -> NSRegularExpression {
        guard let regex = Self.cache[self] else {
            let newRegex = try nsRegex()
            Log.verbose("[REGEX] built new regex for \(self)")
            Self.cache[self] = newRegex
            return newRegex
        }
        return regex
    }
    
    func matches(string: String) -> Bool {
        do {
            let regex = try cachedRegex()
            return regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != .none
        } catch {
            Log.error(error)
            return false
        }
    }
}

extension String {
    var isEmail: Bool {
        Regex.email.matches(string: self)
    }
    
    var isURL: Bool {
        Regex.url.matches(string: self)
    }
}
