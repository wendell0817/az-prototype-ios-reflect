//
//  NumberFormatterUtility.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/4/21.
//

import Foundation
import SwiftUI

struct NumberFormatterUtility {
    enum Format: Hashable {
        case metric
        case metricWholeNumber
        
        fileprivate func formatter() -> NumberFormatter {
            switch self {
            case .metric:
                let formatter = NumberFormatter()
                formatter.maximumFractionDigits = 1
                formatter.minimumFractionDigits = 1
                return formatter
            case .metricWholeNumber:
                let formatter = NumberFormatter()
                formatter.maximumFractionDigits = 0
                formatter.minimumFractionDigits = 0
                return formatter
            }
        }
    }
    
    static var formatters: [Format: NumberFormatter] = [:]
    
    static func formatter(for format: Format) -> NumberFormatter {
        guard let formatter = formatters[format] else {
            let newFormatter = format.formatter()
            formatters[format] = newFormatter
            return newFormatter
        }
        
        return formatter
    }
}

extension Text {
    init(value: Any?, format: NumberFormatterUtility.Format) {
        self = Text(NumberFormatterUtility.formatter(for: format).string(for: value) ?? "")
    }
}
