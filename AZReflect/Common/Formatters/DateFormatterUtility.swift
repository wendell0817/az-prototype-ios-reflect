//
//  DateFormatterUtility.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/4/21.
//

import Foundation
import SwiftUI

struct DateFormatterUtility {
    enum Format: Hashable {
        case short
        case timer
        case custom(String)
        
        var dateFormat: String {
            switch self {
            case .short:
                return "MM/dd/yyyy"
            case .timer:
                return "m:ss"
            case .custom(let formatString):
                return formatString
            }
        }
    }
    
    static var formatters: [Format: DateFormatter] = [:]
    
    static func formatter(for format: Format) -> DateFormatter {
        guard let formatter = formatters[format] else {
            let newFormatter = DateFormatter()
            newFormatter.dateFormat = format.dateFormat
            formatters[format] = newFormatter
            return newFormatter
        }
        
        return formatter
    }
}

extension Text {
    init(date: Date, format: DateFormatterUtility.Format) {
        self = Text(DateFormatterUtility.formatter(for: format).string(from: date))
    }
}
