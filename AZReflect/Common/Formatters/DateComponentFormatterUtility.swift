//
//  DateComponentFormatterUtility.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/11/21.
//

import Foundation
import SwiftUI

struct DateComponentFormatterUtility {
    enum Format: Hashable {
        case timer
        
        var allowedUnits: NSCalendar.Unit {
            switch self {
            case .timer:
                return [.minute, .second]
            }
        }
        
        var unitsStyle: DateComponentsFormatter.UnitsStyle {
            switch self {
            case .timer:
                return .positional
            }
        }
        
        var zeroFormattingBehavior: DateComponentsFormatter.ZeroFormattingBehavior {
            switch self {
            case .timer:
                return .pad
            }
        }
        
        func createFormatter() -> DateComponentsFormatter {
            let formatter = DateComponentsFormatter()
            formatter.allowedUnits = allowedUnits
            formatter.unitsStyle = unitsStyle
            formatter.zeroFormattingBehavior = zeroFormattingBehavior
            return formatter
        }
    }
    
    static var formatters: [Format: DateComponentsFormatter] = [:]
    
    static func formatter(for format: Format) -> DateComponentsFormatter {
        guard let existingFormatter = Self.formatters[format] else {
            let newFormatter = format.createFormatter()
            Self.formatters[format] = newFormatter
            return newFormatter
        }
        
        return existingFormatter
    }
}

extension Text {
    init(timeInterval: TimeInterval, format: DateComponentFormatterUtility.Format) {
        self = Text(DateComponentFormatterUtility.formatter(for: format).string(from: timeInterval) ?? "")
    }
}
