//
//  SizeRatio.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/4/21.
//

import Foundation
import SwiftUI

struct SizeRatio {
    let width: CGFloat
    let height: CGFloat
    
    init(size: CGSize) {
        self.width = size.width
        self.height = size.height
    }
    
    init(width: CGFloat, height: CGFloat) {
        self.width = width
        self.height = height
    }
    
    func width(for height: CGFloat) -> CGFloat {
        (height / self.height) * self.width
    }
    
    func height(for width: CGFloat) -> CGFloat {
        (width / self.width) * self.height
    }
}
