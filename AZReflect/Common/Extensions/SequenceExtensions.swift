//
//  SequenceExtensions.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/10/21.
//

import Foundation

extension Sequence where Element: Hashable {
    public func unique() -> [Element] {
        var uniqueValues: [Element: Void] = [:]
        forEach { uniqueValues[$0] = () }
        return [Element].init(uniqueValues.keys)
    }
    
    public func uniqueOrdered() -> [Element] {
        var uniqueValues: [Element: Void] = [:]
        var orderedValues: [Element] = []
        forEach { element in
            switch !uniqueValues.keys.contains(element) {
            case true:
                uniqueValues[element] = ()
                orderedValues.append(element)
            default:
                break
            }
        }
        
        return orderedValues
    }
}

extension Sequence {
    func unique<H: Hashable>(by keyPath: KeyPath<Element, H>) -> [Element] {
        var uniqueValues: [H: Element] = [:]
        forEach { uniqueValues[$0[keyPath: keyPath]] = $0 }
        return [Element].init(uniqueValues.values)
    }
    
    func uniqueOrdered<H: Hashable>(by keyPath: KeyPath<Element, H>) -> [Element] {
        var uniqueValues: [H: ()] = [:]
        var orderedValues: [Element] = []
        
        forEach { element in
            let key = element[keyPath: keyPath]
            switch !uniqueValues.keys.contains(key) {
            case true:
                uniqueValues[key] = ()
                orderedValues.append(element)
            default:
                break
            }
        }
        
        return orderedValues
    }
}

extension Array where Element: Comparable {
    func sorted(direction sort: (Element, Element) -> Bool) -> Array {
        sorted(by: { sort($0, $1) })
    }
}

extension Array {
    func sorted<Value: Comparable>(
        by keyPath: KeyPath<Element, Value>,
        _ sort: (Value, Value) -> Bool
    ) -> Array {
        sorted(by: { sort($0[keyPath: keyPath], $1[keyPath: keyPath]) })
    }
}
