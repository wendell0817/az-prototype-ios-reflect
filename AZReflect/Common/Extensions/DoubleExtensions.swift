//
//  DoubleExtensions.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/25/21.
//

import Foundation

extension Array where Element == Double {
    var average: Double? {
        guard !isEmpty else { return .none }
        let sum = self.reduce(0.0, +)
        guard sum > 0.0 else { return 0.0 }
        return sum / Double(count)
    }
}
