//
//  MockModels.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 10/21/21.
//

import Foundation

#if DEBUG

extension Reflection {
    static var mock: Reflection {
        Reflection(
            id: "100",
            date: Date(),
            title: "Reflection",
            metrics: .init(
                heartRate: 85.9,
                oxygenSaturation: 99.1,
                breathingRate: 12.5
            ),
            transcription: "I had a headache today",
            symptoms: [.headache, .nausea, .vomiting],
            videoFileName: .none,
            audioFileName: .none
        )
    }
}

extension ShareGroup {
    static var mock: ShareGroup {
        .init(
            members: [
                .init(id: "101", name: "Jeremy", email: "jeremy.wilt@astrzeneca.com"),
                .init(id: "102", name: "Roy", email: "roy.grossberg@astrzeneca.com"),
                .init(id: "103", name: "Mike", email: "mike.dyer@astrzeneca.com")
            ],
            outgoingInvites: [],
            incomingInvites: []
        )
    }
}

#endif
