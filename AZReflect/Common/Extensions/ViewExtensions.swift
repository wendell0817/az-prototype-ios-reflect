//
//  ViewExtensions.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import Foundation
import SwiftUI

extension View {
    func frame(size: CGSize, alignment: Alignment = .center) -> some View {
        frame(width: size.width, height: size.height, alignment: alignment)
    }
    
    func navigationLink<Dest: View>(destination: Dest, isActive: Binding<Bool>) -> some View {
        background(
            NavigationLink(
                destination: destination,
                isActive: isActive,
                label: { EmptyView() }
            )
        )
    }
}
