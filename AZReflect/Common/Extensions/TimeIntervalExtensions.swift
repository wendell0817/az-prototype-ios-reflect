//
//  TimeIntervalExtensions.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/11/21.
//

import Foundation

extension TimeInterval {
    static func seconds(_ value: Double) -> TimeInterval {
        .init(value)
    }
    
    static func minutes(_ value: Double) -> TimeInterval {
        .init(value * 60.0)
    }
}

extension TimeInterval {
    private var milliseconds: Int {
        return Int((truncatingRemainder(dividingBy: 1)) * 1000)
    }

    private var seconds: Int {
        return Int(self) % 60
    }

    private var minutes: Int {
        return (Int(self) / 60 ) % 60
    }

    private var hours: Int {
        return Int(self) / 3600
    }
}

extension TimeInterval {
    struct ClockTime {
        let hours: Double
        let minutes: Double
        let seconds: Double
    }
    
    var clockTime: ClockTime {
        let seconds = self.truncatingRemainder(dividingBy: 60.0)
        let minutes = (self / 60.0).truncatingRemainder(dividingBy: 60.0)
        let hours = floor(self / 3_600.0)
        
        return .init(hours: hours, minutes: minutes, seconds: seconds)
    }
    
    init(hours: Int = 0, minutes: Int = 0, seconds: Int = 0) {
        let seconds = seconds
        let minuteSeconds = minutes * 60
        let hourSeconds = hours * 3600
        self = TimeInterval(seconds + minuteSeconds + hourSeconds)
    }
}
