//
//  RangeExtensions.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 9/1/21.
//

import Foundation
import SwiftUI

extension ClosedRange where Bound: BinaryFloatingPoint {
    func clamp(_ bound: Bound) -> Bound {
        guard contains(bound) else {
            return bound < lowerBound ? lowerBound : upperBound
        }
        return bound
    }
    
    var middle: Bound {
        let difference = upperBound - lowerBound
        return lowerBound + (difference * 0.5)
    }
}
