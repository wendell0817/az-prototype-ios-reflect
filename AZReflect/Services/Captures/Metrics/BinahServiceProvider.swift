//
//  BinahServiceProvider.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/25/21.
//

import Foundation
import Combine

extension MetricsServiceProvider {
    static var binah: MetricsServiceProvider {
        BinahServiceProvider()
    }
}

final class BinahServiceProvider: NSObject, MetricsServiceProvider {
    private let imageSubject: PassthroughSubject<MetricsService.Image, Never> = .init()
    private let metricsSubject: CurrentValueSubject<MetricsService.Metrics, Never> = .init(.init())
    private var healthManager: HealthMonitorManager? = .none
    private var healthSession: HealthMonitorSession? = .none
    
    public var imagePublisher: AnyPublisher<MetricsService.Image, Never> {
        imageSubject.eraseToAnyPublisher()
    }
    
    public var metricsPublisher: AnyPublisher<MetricsService.Metrics, Never> {
        metricsSubject.eraseToAnyPublisher()
    }
    
    public var currentMetrics: MetricsService.Metrics {
        metricsSubject.value
    }
    
    func startRecording(for length: TimeInterval) {
        do {
            metricsSubject.value = .init()
            try initialize()
            
            let processingTime = NSNumber(value: length)
            let newSession = try healthManager?.createFaceSessionBuilder(processingTime: processingTime)
                .listener(self)
                .stateListener(self)
                .build()
            
            switch newSession {
            case .some(let session):
                try session.start()
                self.healthSession = session
            default:
                Log.error("[BinahServiceProvider] failed to created new health session")
            }
        } catch let error {
            Log.error(error)
        }
    }
    
    func stopRecording() {
        switch healthSession {
        case .some(let session):
            try? session.stop()
        default:
            break
        }
    }
    
    private func initialize() throws {
        let licenseData = UserLicenseData()
        licenseData.licenseKey = try Config.value(for: .binahLicenseKey)
        healthManager = try HealthMonitorManager(licenseData: licenseData, with: self)
    }
}

extension BinahServiceProvider {
    enum BinahError: Error, LocalizedError {
        case monitorError(HealthMonitorCode)
        case generic(Swift.Error)
        
        var errorDescription: String? {
            switch self {
            case .monitorError(let code):
                return code.description
            case .generic(let error):
                return "\(error)"
            }
        }
        
        init(_ error: Swift.Error) {
            let code = (error as NSError).code
            switch HealthMonitorCode(rawValue: code) {
            case .some(let monitorCode):
                self = .monitorError(monitorCode)
            default:
                self = .generic(error)
            }
        }
    }
}

// MARK: HealthMonitorManagerListener

extension BinahServiceProvider: HealthMonitorManagerListener {
    func healthMonitorManagerReady() {
        Log.verbose("[BinahServiceProvider] healthMonitorManagerReady")
    }
    
    func healthMonitorManagerError(_ error: Error) {
        Log.error("[BinahServiceProvider] healthMonitorManagerError \(BinahError(error))")
    }
}

// MARK: HealthMonitorFaceSessionListener

extension StressLevelType {
    var metricsStressLevel: MetricsService.StressLevel {
        let metricsStressLevel = MetricsService.StressLevel.init(rawValue: rawValue)
        switch metricsStressLevel {
        case .some(let stressLevel):
            return stressLevel
        default:
            return .unknown
        }
    }
}

extension BinahServiceProvider: HealthMonitorFaceSessionListener {
    func newImage(_ image: UIImage!, withFaceAt faceRect: CGRect) {
        imageSubject.send(.init(image: image, faceRect: faceRect))
    }
    
    func healthMonitorSessionMessage(_ messageType: HealthMonitorMessageType, withValue value: Any) {
        switch (messageType, value) {
        case (.breathingRate, let nsNumber as NSNumber):
            metricsSubject.value.breathingRates.append(nsNumber.doubleValue)
        case (.heartRate, let nsNumber as NSNumber):
            metricsSubject.value.heartRates.append(nsNumber.doubleValue)
        case (.oxygenSaturation, let nsNumber as NSNumber):
            metricsSubject.value.oxygenSaturations.append(nsNumber.doubleValue)
        case (.report, let report as HealthMonitorReport):
            metricsSubject.value.report = report.metricsReport
            Log.verbose("[BinahServiceProvider] received report:\(report.metricsReport)")
        default:
            Log.warning("[BinahServiceProvider] unknown messageType:\(messageType) value:\(value)")
        }
    }
    
    func healthMonitorSessionError(_ error: Error) {
        Log.error("[BinahServiceProvider] healthMonitorSessionError \(BinahError(error))")
    }
}

// MARK: HealthMonitorSessionStateListener

extension BinahServiceProvider: HealthMonitorSessionStateListener {
    func healthMonitorSession(_ session: HealthMonitorSession!, change state: HealthMonitorSessionState) {
        switch state {
        case .active:
            Log.verbose("[BinahServiceProvider] active")
        case .measuring:
            Log.verbose("[BinahServiceProvider] measuring")
        case .stopped:
            Log.verbose("[BinahServiceProvider] stopped")
        case .terminated:
            Log.verbose("[BinahServiceProvider] terminated")
        default:
            Log.warning("[BinahServiceProvider] unknown session state:\(state)")
        }
    }
}

// MARK: HealthMonitorCode: CustomStringConvertible

extension HealthMonitorCode: CustomStringConvertible {
    public var description: String {
        switch self {
        case .app_IllegalStartCallError:
            return "[\(rawValue)] app_IllegalStartCallError"
        case .app_IllegalStartSessionCallError:
            return "[\(rawValue)] app_IllegalStartSessionCallError"
        case .app_IllegalStopCallError:
            return "[\(rawValue)] app_IllegalStopCallError"
        case .camera_CameraMissingPermissionsError:
            return "[\(rawValue)] camera_CameraMissingPermissionsError"
        case .camera_CameraOpenError:
            return "[\(rawValue)] camera_CameraOpenError"
        case .camera_NoCameraError:
            return "[\(rawValue)] camera_NoCameraError"
        case .device_InternalError1,
             .device_InternalError2,
             .device_InternalError3,
             .device_InternalError4:
            return "[\(rawValue)] device_InternalError"
        case .device_LowPowerModeEnabledError:
            return "[\(rawValue)] device_LowPowerModeEnabledError"
        case .device_MinimumBatteryLevelError:
            return "[\(rawValue)] device_MinimumBatteryLevelError"
        case .device_MinimumIOSVersionError:
            return "[\(rawValue)] device_MinimumIOSVersionError"
        case .device_TorchShutDownError:
            return "[\(rawValue)] device_TorchShutDownError"
        case .device_TorchUnavailableError:
            return "[\(rawValue)] device_TorchUnavailableError"
        case .device_UnsupportedDeviceError:
            return "[\(rawValue)] device_UnsupportedDeviceError"
        case .initialization_InternalError1:
            return "[\(rawValue)] initialization_InternalError1"
        case .initialization_InvalidProcessingTimeError:
            return "[\(rawValue)] initialization_InvalidProcessingTimeError"
        case .license_ActivationLimitReachedError:
            return "[\(rawValue)] license_ActivationLimitReachedError"
        case .license_InternalError1,
             .license_InternalError2,
             .license_InternalError3,
             .license_InternalError4,
             .license_InternalError5,
             .license_InternalError7,
             .license_InternalError8,
             .license_InternalError9,
             .license_InternalError10,
             .license_InternalError11,
             .license_InternalError16,
             .license_InternalError18:
            return "[\(rawValue)] license_InternalError"
        case .license_AuthenticationFailedError:
            return "[\(rawValue)] license_AuthenticationFailedError"
        case .license_CannotOpenFileForReadError:
            return "[\(rawValue)] license_CannotOpenFileForReadError"
        case .license_DeviceDeactivatedError:
            return "[\(rawValue)] license_DeviceDeactivatedError"
        case .license_InputFingerprintEmptyError:
            return "[\(rawValue)] license_InputFingerprintEmptyError"
        case .license_InputLicenseKeyEmptyError:
            return "[\(rawValue)] license_InputLicenseKeyEmptyError"
        case .license_InputProductIdIllegalError:
            return "[\(rawValue)] license_InputProductIdIllegalError"
        case .license_InvalidLicenseKeyError:
            return "[\(rawValue)] license_InvalidLicenseKeyError"
        case .license_LicenseExpiredError:
            return "[\(rawValue)] license_LicenseExpiredError"
        case .license_LicenseSuspendedError:
            return "[\(rawValue)] license_LicenseSuspendedError"
        case .license_MeterAttributeUsesLimitReachedError:
            return "[\(rawValue)] license_MeterAttributeUsesLimitReachedError"
        case .license_MinSdkError:
            return "[\(rawValue)] license_MinSdkError"
        case .license_MonthlyUsageTrackingRequiresSyncError:
            return "[\(rawValue)] license_MonthlyUsageTrackingRequiresSyncError"
        case .license_NetworkIssuesError:
            return "[\(rawValue)] license_NetworkIssuesError"
        case .license_RevokedLicenseError:
            return "[\(rawValue)] license_RevokedLicenseError"
        case .license_SslHandShakeCertificateExpiredError:
            return "[\(rawValue)] license_SslHandShakeCertificateExpiredError"
        case .license_SslHandShakeDeviceDateError:
            return "[\(rawValue)] license_SslHandShakeDeviceDateError"
        case .license_SslHandShakeError:
            return "[\(rawValue)] license_SslHandShakeError"
        case .license_TokenExpiredError:
            return "[\(rawValue)] license_TokenExpiredError"
        case .measurement_InternalError1,
             .measurement_InternalError2:
            return "[\(rawValue)] measurement_InternalError"
        case .measurement_InvalidMeasurementAverageDetectionRateError:
            return "[\(rawValue)] measurement_InvalidMeasurementAverageDetectionRateError"
        case .measurement_InvalidRecentDetectionRateError:
            return "[\(rawValue)] measurement_InvalidRecentDetectionRateError"
        case .measurement_InvalidRecentFPSRateWarning:
            return "[\(rawValue)] measurement_InvalidRecentFPSRateWarning"
        case .measurement_LicenseActivationFailedError:
            return "[\(rawValue)] measurement_LicenseActivationFailedError"
        case .measurement_MisdetectionDurationExceedsLimitError:
            return "[\(rawValue)] measurement_MisdetectionDurationExceedsLimitError"
        case .measurement_MisdetectionDurationExceedsLimitWarning:
            return "[\(rawValue)] measurement_MisdetectionDurationExceedsLimitWarning"
        case .measurement_UnsupportedOrientationWarning:
            return "[\(rawValue)] measurement_UnsupportedOrientationWarning"
        case .vitalSign_InternalError1,
             .vitalSign_InternalError2:
            return "[\(rawValue)] vitalSign_InternalError"
        default:
            return "[\(rawValue)] uknown"
        }
    }
}

extension RRIValue {
    var metricsRRIValue: MetricsService.RRIValue {
        return .init(timestamp: timestamp, interval: interval)
    }
}

extension HealthMonitorReport {
    var metricsReport: MetricsService.Report {
        return .init(
            heartRate: heartRate?.doubleValue,
            respirationRate: respirationRate?.doubleValue,
            oxygenSaturation: oxygenSaturation?.doubleValue,
            standardDeviationNN: standardDeviationNN?.doubleValue,
            stressLevel: stressLevel.rawValue,
            rrInterval: rrInterval?.map(\.metricsRRIValue),
            stressIndex: stressIndex?.doubleValue
        )
    }
}
