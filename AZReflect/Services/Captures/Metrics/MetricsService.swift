//
//  MetricsService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/25/21.
//

import Foundation
import Combine

protocol MetricsServiceProvider {
    var imagePublisher: AnyPublisher<MetricsService.Image, Never> { get }
    var metricsPublisher: AnyPublisher<MetricsService.Metrics, Never> { get }
    var currentMetrics: MetricsService.Metrics { get }
    func startRecording(for length: TimeInterval)
    func stopRecording()
}

struct MetricsService: MetricsServiceProvider {
    private let provider: MetricsServiceProvider
    
    var imagePublisher: AnyPublisher<Image, Never> { provider.imagePublisher }
    var metricsPublisher: AnyPublisher<Metrics, Never> { provider.metricsPublisher }
    var currentMetrics: Metrics { provider.currentMetrics }
    
    init() {
        #if targetEnvironment(simulator)
        self.provider = MockMetricsProvider()
        #else
        self.provider = BinahServiceProvider()
        #endif
    }
    
    func startRecording(for length: TimeInterval) {
        provider.startRecording(for: length)
    }
    
    func stopRecording() {
        provider.stopRecording()
    }
}

extension MetricsService {
    struct Image: Equatable {
        let image: UIImage?
        let faceRect: CGRect
    }
    
    enum StressLevel: UInt32 {
        case unknown = 0
        case low = 1
        case normal = 2
        case mild = 3
        case high = 4
        case extreme = 5
    }
    
    struct RRIValue: Equatable {
        let timestamp: Double
        let interval: Double
    }
    
    struct Report: Equatable {
        let heartRate: Double?
        let respirationRate: Double?
        let oxygenSaturation: Double?
        let standardDeviationNN: Double?
        let stressLevel: UInt32
        let rrInterval: [RRIValue]?
        let stressIndex: Double?
    }
    
    struct Metrics: Equatable {
        var breathingRates: [Double] = []
        var heartRates: [Double] = []
        var oxygenSaturations: [Double] = []
        var report: Report? = .none
    }
}
