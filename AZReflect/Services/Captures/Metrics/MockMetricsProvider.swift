//
//  MockMetricsProvider.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/25/21.
//

import Foundation
import Combine

final class MockMetricsProvider: MetricsServiceProvider {
    private let imageSubject: PassthroughSubject<MetricsService.Image, Never> = .init()
    private let metricsSubject: CurrentValueSubject<MetricsService.Metrics, Never> = .init(.init())
    private var subscriptions: [AnyCancellable] = []
    private let heartRateRange = 40.0 ... 60.0
    private let oxygenSaturationRange = 95.0 ... 100.0
    private let respirationRateRange = 12.0 ... 16.0
    
    var imagePublisher: AnyPublisher<MetricsService.Image, Never> {
        imageSubject.eraseToAnyPublisher()
    }
    
    var metricsPublisher: AnyPublisher<MetricsService.Metrics, Never> {
        metricsSubject.eraseToAnyPublisher()
    }
    
    var currentMetrics: MetricsService.Metrics {
        metricsSubject.value
    }
    
    func startRecording(for length: TimeInterval) {
        imageSubject.send(
            .init(
                image: UIImage(named: "capture.image.test"),
                faceRect: CGRect(x: 0.0, y: 0.0, width: 200.0, height: 400.0)
            )
        )
        
        randomValuePublisher(for: heartRateRange, delay: .seconds(2), every: 2.0)
            .sink {  self.setValue($0, for: \.heartRates) }
            .store(in: &subscriptions)
        
        randomValuePublisher(for: oxygenSaturationRange, delay: .seconds(4), every: 3.0)
            .sink {  self.setValue($0, for: \.oxygenSaturations) }
            .store(in: &subscriptions)
        
        randomValuePublisher(for: respirationRateRange, delay: .seconds(6), every: 4.0)
            .sink {  self.setValue($0, for: \.breathingRates) }
            .store(in: &subscriptions)
    }
    
    func stopRecording() {
        metricsSubject.value.report = .init(
            heartRate: Double.random(in: heartRateRange),
            respirationRate: Double.random(in: respirationRateRange),
            oxygenSaturation: Double.random(in: oxygenSaturationRange),
            standardDeviationNN: Double.random(in: 90.0 ..< 153.0),
            stressLevel: UInt32.random(in: 1 ... 5),
            rrInterval: .none,
            stressIndex: .none
        )
        
        subscriptions.forEach { $0.cancel() }
        subscriptions = []
    }
    
    private func randomValuePublisher(
        for range: ClosedRange<Double>,
        delay: DispatchQueue.SchedulerTimeType.Stride,
        every interval: TimeInterval
    ) -> AnyPublisher<Double, Never> {
        Just(())
            .delay(for: delay, scheduler: DispatchQueue.main)
            .flatMap { _ in Timer.publish(every: interval, on: .main, in: .default).autoconnect() }
            .map { _ in range }
            .map(Double.random(in:))
            .eraseToAnyPublisher()
    }
    
    private func setValue(_ value: Double, for keyPath: KeyPath<MetricsService.Metrics, [Double]>) {
        switch keyPath {
        case \.heartRates:
            metricsSubject.value.heartRates.append(value)
        case \.oxygenSaturations:
            metricsSubject.value.oxygenSaturations.append(value)
        case \.breathingRates:
            metricsSubject.value.breathingRates.append(value)
        default:
            break
        }
    }
}
