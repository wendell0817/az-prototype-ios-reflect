//
//  TranscriptionService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/9/21.
//

import Foundation
import AVFoundation
import Speech
import Combine

final class AVTranscriptionProvider: NSObject, TranscriptionServiceProvider {
    private let speechSubject = CurrentValueSubject<String, Never>("")
    private let outputSubject = PassthroughSubject<URL, Never>()
    private var session = AVCaptureSession()
    private var tempDirUrl: URL { FileManager.default.temporaryDirectory }
    private var speechRequest = SFSpeechAudioBufferRecognitionRequest()
    private var writer: AVAssetWriter? = .none
    private var writerInput: AVAssetWriterInput? = .none
    private var transcribeQueue = DispatchQueue(
        label: "AZReflect.TranscriptionService.transcribeQueue",
        qos: .background,
        target: .none
    )
    
    private let audioFileType: AVFileType = .m4a
    
    public var speechPublisher: AnyPublisher<String, Never> {
        speechSubject.eraseToAnyPublisher()
    }
    
    public var outputPublisher: AnyPublisher<URL, Never> {
        outputSubject.eraseToAnyPublisher()
    }
    
    func startRecording(to id: String) {
        isAuthorized { [weak self] granted in
            guard let self = self, granted else { return }
            self.configure()
            self.setupTranscription()
            self.startAudioRecording(to: self.tempDirUrl.appendingPathComponent(id + ".m4a"))
        }
    }
    
    func stopRecording() {
        speechRequest.endAudio()
        session.stopRunning()
        writerInput = .none
        
        switch writer {
        case .some(let writer):
            writer.finishWriting {
                Log.debug("[TranscriptionService] finished writing to \(writer.outputURL.lastPathComponent)")
                self.writer = .none
                self.outputSubject.send(writer.outputURL)
            }
        default:
            break
        }
    }
    
    private func configure() {
        do {
            guard let audioDevice = AVCaptureDevice.default(for: .audio) else {
                Log.debug("[TranscriptionService] could not find audio device")
                return
            }
            
            let audioInput = try AVCaptureDeviceInput(device: audioDevice)
            guard session.canAddInput(audioInput) else {
                Log.debug("[TranscriptionService] could not add audio input device")
                return
            }
            
            let audioOutput = AVCaptureAudioDataOutput()
            audioOutput.setSampleBufferDelegate(self, queue: self.transcribeQueue)
            guard session.canAddOutput(audioOutput) else {
                Log.debug("[TranscriptionService] could not add audio output device")
                return
            }
            
            session.beginConfiguration()
            session.inputs.forEach { session.removeInput($0) }
            session.addInput(audioInput)
            session.addOutput(audioOutput)
            session.commitConfiguration()
            
        } catch let error {
            Log.error("[TranscriptionService] \(error)")
        }
    }
    
    private func setupTranscription() {
        let newSpeechRequest = SFSpeechAudioBufferRecognitionRequest()
        guard let speechRecognizer = SFSpeechRecognizer(locale: Locale.current) else {
            Log.error("[TranscriptionService] could not create speech recognizer")
            return
        }
        
        speechRecognizer.recognitionTask(
            with: newSpeechRequest,
            resultHandler: { [weak self] result, error in
                guard let self = self else { return }
                
                switch (result, error) {
                case (_, .some(let error)):
                    Log.error("[TranscriptionService] \(error)")
                case (.some(let result), _):
                    self.speechSubject.value = result.bestTranscription.formattedString
                default:
                    Log.error("[TranscriptionService] unknown result")
                }
            }
        )
        
        self.speechRequest = newSpeechRequest
    }
    
    private func startAudioRecording(to fileURL: URL) {
        guard let audioOutput = session.outputs.compactMap({ $0 as? AVCaptureAudioDataOutput}).first else {
            Log.error("[TranscriptionService] session has no audio outputs")
            return
        }
        
        do {
            let settings = audioOutput.recommendedAudioSettingsForAssetWriter(writingTo: self.audioFileType) ?? [:]
            let writerInput = AVAssetWriterInput(mediaType: .audio, outputSettings: settings)
            writerInput.marksOutputTrackAsEnabled = true
            let writer = try AVAssetWriter(outputURL: fileURL, fileType: self.audioFileType)
            writer.add(writerInput)
            
            self.writer = writer
            self.writerInput = writerInput
            Log.debug("[TranscriptionService] started writing to \(fileURL.lastPathComponent)")
        } catch let error {
            Log.error("[TranscriptionService] \(error)")
        }
        
        session.startRunning()
    }
    
    private func isAuthorized(_ isAuthorized: @escaping (Bool) -> Void) {
        SFSpeechRecognizer.requestAuthorization { status in
            switch status {
            case .authorized:
                AVAudioSession.sharedInstance().requestRecordPermission { authorized in
                    isAuthorized(authorized)
                }
            default:
                isAuthorized(false)
            }
        }
    }
}

extension AVTranscriptionProvider: AVCaptureAudioDataOutputSampleBufferDelegate {
    func captureOutput(
        _ output: AVCaptureOutput,
        didOutput sampleBuffer: CMSampleBuffer,
        from connection: AVCaptureConnection
    ) {
        guard let writer = writer, let writerInput = writerInput else { return }
        
        switch writer.status {
        case .unknown:
            writer.startWriting()
            writer.startSession(atSourceTime: CMSampleBufferGetPresentationTimeStamp(sampleBuffer))
            writerInput.append(sampleBuffer)
        case .writing:
            writerInput.append(sampleBuffer)
        default:
            break
        }
        
        speechRequest.appendAudioSampleBuffer(sampleBuffer)
    }
}
