//
//  MockTranscriptionProvider.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/31/21.
//

import Foundation
import Combine

final class MockTranscriptionProvider: TranscriptionServiceProvider {
    private let speechSubject = CurrentValueSubject<String, Never>("")
    private let outputSubject = PassthroughSubject<URL, Never>()
    private var subscriptions: [AnyCancellable] = []
    
    var speechPublisher: AnyPublisher<String, Never> {
        speechSubject.eraseToAnyPublisher()
    }
    
    public var outputPublisher: AnyPublisher<URL, Never> {
        outputSubject.eraseToAnyPublisher()
    }
    
    func startRecording(to id: String) {
        speechSubject.value = ""
        let mockSymptoms: [Symptom] = [.headache, .nausea, .acne]
        Just(mockSymptoms)
            .flatMap { Publishers.Sequence(sequence: $0) }
            .map(\.title)
            .delay(for: .seconds(2), scheduler: DispatchQueue.main)
            .log(.debug, prefix: "[MockTranscriptionProvider] symptom")
            .sink { [weak self] symptomText in
                guard let self = self else { return }
                self.speechSubject.value = self.speechSubject.value + " \(symptomText)"
            }
            .store(in: &subscriptions)
    }
    
    func stopRecording() {
        subscriptions.forEach { $0.cancel() }
        subscriptions = []
    }
}
