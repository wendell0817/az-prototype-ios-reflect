//
//  TranscriptionService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/31/21.
//

import Foundation
import Combine

protocol TranscriptionServiceProvider {
    var speechPublisher: AnyPublisher<String, Never> { get }
    var outputPublisher: AnyPublisher<URL, Never> { get }
    func startRecording(to id: String)
    func stopRecording()
}

struct TranscriptionService: TranscriptionServiceProvider {
    private let provider: TranscriptionServiceProvider
    
    var speechPublisher: AnyPublisher<String, Never> {
        provider.speechPublisher
    }
    
    var outputPublisher: AnyPublisher<URL, Never> {
        provider.outputPublisher
    }
    
    init() {
        #if targetEnvironment(simulator)
        self.provider = MockTranscriptionProvider()
        #else
        self.provider = AVTranscriptionProvider()
        #endif
    }
    
    func startRecording(to id: String) {
        provider.startRecording(to: id)
    }
    
    func stopRecording() {
        provider.stopRecording()
    }
}
