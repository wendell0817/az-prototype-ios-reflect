//
//  VideoMergeService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/31/21.
//

import Foundation
import AVFoundation

struct VideoMergeService {
    private var documentsDirURL: URL {
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    func merge(
        _ data: VideoMergeService.Data,
        _ completion: @escaping (Result<URL, VideoMergeService.MergeError>) -> Void
    ) {
        let composition = AVMutableComposition()
        
        guard let compVideoTrack = composition.addMutableTrack(
            withMediaType: .video,
            preferredTrackID: kCMPersistentTrackID_Invalid
        ) else {
            completion(.failure(.couldNotAddVideoTrack))
            return
        }
        
        guard let compAudioTrack = composition.addMutableTrack(
            withMediaType: .audio,
            preferredTrackID: kCMPersistentTrackID_Invalid
        ) else {
            completion(.failure(.couldNotAddAudioTrack))
            return
        }
        
        let videoAsset = AVAsset(url: data.videoURL)
        let audioAsset = AVAsset(url: data.audioURL)
        
        guard let firstVideoAssetTrack = videoAsset.tracks(withMediaType: .video).first else {
            completion(.failure(.videoTrackNotFound))
            return
        }
        
        // Default must have tranformation
        compVideoTrack.preferredTransform = firstVideoAssetTrack.preferredTransform
        
        guard let firstAudioAssetTrack = audioAsset.tracks(withMediaType: .audio).first else {
            completion(.failure(.audioTrackNotFound))
            return
        }
        
        do {
            try compVideoTrack.insertTimeRange(
                .init(
                    start: .zero,
                    duration: firstVideoAssetTrack.timeRange.duration
                ),
                of: firstVideoAssetTrack,
                at: .zero
            )
            
            try compAudioTrack.insertTimeRange(
                .init(
                    start: firstVideoAssetTrack.timeRange.start,
                    duration: firstVideoAssetTrack.timeRange.start + firstAudioAssetTrack.timeRange.duration
                ),
                of: firstAudioAssetTrack,
                at: .zero
            )
            
        } catch let error {
            completion(.failure(.generic(error)))
            return
        }
        
        guard let exportSession = AVAssetExportSession(
                asset: composition,
                presetName: AVAssetExportPresetHighestQuality
        ) else {
            completion(.failure(.failedToCreateExportSession))
            return
        }
        
        let outputURL = documentsDirURL.appendingPathComponent(data.id + ".mp4")
        exportSession.outputFileType = .mp4
        exportSession.outputURL = outputURL
        exportSession.shouldOptimizeForNetworkUse = false
        
        exportSession.exportAsynchronously {
            switch (exportSession.error, exportSession.status) {
            case (.some(let error), _):
                completion(.failure(.generic(error)))
            case (.none, .completed):
                completion(.success(outputURL))
            default:
                completion(.failure(.unknown))
            }
        }
    }
}

extension VideoMergeService {
    enum MergeError: Error {
        case couldNotAddVideoTrack
        case couldNotAddAudioTrack
        case videoTrackNotFound
        case audioTrackNotFound
        case failedToCreateExportSession
        case generic(Error)
        case unknown
    }
    
    struct Data: Equatable {
        let id: String
        let videoURL: URL
        let audioURL: URL
    }
}
