//
//  MockVideoCaptureServiceProvider.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/31/21.
//

import Foundation
import Combine

final class MockVideoCaptureServiceProvider: VideoCaptureProvider {
    public private(set) var session = AVCaptureSession()
    private let outputSubject = PassthroughSubject<URL, Never>()
    
    var outputPublisher: AnyPublisher<URL, Never> {
        outputSubject.eraseToAnyPublisher()
    }
    
    func startRecording(to id: String) {
        
    }
    
    func stopRecording() {
        
    }
}
