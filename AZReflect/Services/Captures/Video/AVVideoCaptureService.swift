//
//  VideoCaptureService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/9/21.
//

import Foundation
import Combine
import AVFoundation
import UIKit

class AVVideoCaptureServiceProvider: NSObject, VideoCaptureProvider {
    private let outputSubject = PassthroughSubject<URL, Never>()
    public private(set) var session = AVCaptureSession()
    private var movieFileOutput: AVCaptureMovieFileOutput? = .none
    private var tempDirUrl: URL { FileManager.default.temporaryDirectory }
    
    var outputPublisher: AnyPublisher<URL, Never> {
        outputSubject.eraseToAnyPublisher()
    }
    
    func startRecording(to id: String) {
        isVideoAuthorized { [weak self] granted in
            guard let self = self, granted else { return }
            self.configure()
            self.startVideoRecording(for: id)
        }
    }
    
    func stopRecording() {
        guard let movieFileOutput = movieFileOutput else { return }
        movieFileOutput.stopRecording()
    }
    
    private func configure() {
        guard let cameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else {
            Log.error("[VideoCaptureService] could not created input device")
            return
        }
        
        guard let inputDevice = try? AVCaptureDeviceInput(device: cameraDevice) else {
            Log.error("[VideoCaptureService] could not create input device")
            return
        }
        
        guard session.canAddInput(inputDevice) else {
            Log.error("[VideoCaptureService] could not add input device")
            return
        }
        
        let newOutput = AVCaptureMovieFileOutput()
        guard session.canAddOutput(newOutput) else {
            Log.error("[VideoCaptureService] could not add input device")
            return
        }
        
        let outputConnection = newOutput.connection(with: .video)
        switch outputConnection {
        case .some(let connection):
            connection.videoOrientation = UIDevice.current.orientation.avOrientation
            switch newOutput.availableVideoCodecTypes.contains(.hevc) {
            case true:
                newOutput.setOutputSettings([AVVideoCodecKey: AVVideoCodecType.hevc], for: connection)
            default:
                break
            }
        default:
            break
        }
        
        session.beginConfiguration()

        session.sessionPreset = .high
        session.inputs.forEach { session.removeInput($0) }
        session.addInput(inputDevice)
        session.addOutput(newOutput)

        session.commitConfiguration()
        
        self.movieFileOutput = newOutput
    }
    
    private func startVideoRecording(for id: String) {
        guard let output = session.outputs.compactMap({ $0 as? AVCaptureMovieFileOutput }).first else {
            Log.error("[VideoCaptureStore] no AVCaptureMovieFileOutput found")
            return
        }
        
        let outputUrl = tempDirUrl.appendingPathComponent(id + ".mp4")

        session.startRunning()
        output.startRecording(to: outputUrl, recordingDelegate: self)
    }
    
    private func isVideoAuthorized(_ isAuthorized: @escaping (Bool) -> Void) {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        switch status {
        case .authorized:
            isAuthorized(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                isAuthorized(granted)
            }
        default:
            Log.warning("[VideoCaptureStore] unknown status:\(status)")
            isAuthorized(false)
        }
    }
}

// MARK: AVCaptureFileOutputRecordingDelegate

extension AVVideoCaptureServiceProvider: AVCaptureFileOutputRecordingDelegate {
    func fileOutput(
        _ output: AVCaptureFileOutput,
        didStartRecordingTo fileURL: URL,
        from connections: [AVCaptureConnection]
    ) {
        Log.debug("[VideoService] didStartRecordingTo \(fileURL.lastPathComponent)")
    }
    
    func fileOutput(
        _ output: AVCaptureFileOutput,
        didFinishRecordingTo outputFileURL: URL,
        from connections: [AVCaptureConnection],
        error: Error?
    ) {
        switch error {
        case .some(let error):
            Log.debug("[VideoService] didFinishRecordingTo error:\(error)")
        default:
            Log.debug("[VideoService] didFinishRecordingTo \(outputFileURL.lastPathComponent)")
            outputSubject.send(outputFileURL)
        }
    }
}

// MARK: Extensions

extension UIDeviceOrientation {
    var avOrientation: AVCaptureVideoOrientation {
        switch self {
        case .landscapeRight:
            return .landscapeLeft
        case .landscapeLeft:
            return .landscapeRight
        default:
            return .portrait
        }
    }
}
