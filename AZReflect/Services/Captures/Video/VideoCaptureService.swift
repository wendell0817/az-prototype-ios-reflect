//
//  VideoCaptureService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/31/21.
//

import Foundation
import Combine
import AVFoundation

protocol VideoCaptureProvider {
    var outputPublisher: AnyPublisher<URL, Never> { get }
    var session: AVCaptureSession { get }
    func startRecording(to id: String)
    func stopRecording()
}

struct VideoCaptureService: VideoCaptureProvider {
    public var outputPublisher: AnyPublisher<URL, Never> {
        provider.outputPublisher
    }
    
    public var session: AVCaptureSession {
        provider.session
    }
    
    private let provider: VideoCaptureProvider
    
    init() {
        #if targetEnvironment(simulator)
        self.provider = MockVideoCaptureServiceProvider()
        #else
        self.provider = AVVideoCaptureServiceProvider()
        #endif
    }
    
    func startRecording(to id: String) {
        provider.startRecording(to: id)
    }
    
    func stopRecording() {
        provider.stopRecording()
    }
}


