//
//  SymptomDetectionService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/10/21.
//

import Foundation
import Combine

struct SymptomDetectionService {
    static let allSymptoms = Symptom.allCases
    
    func detectSymptoms(in text: String) -> [Symptom] {
        guard !text.isEmpty else { return [] }
        
        return Self.allSymptoms
            .compactMap { symptom -> SymptomRange? in
                var symptomRange: SymptomRange? = .none
                for desc in symptom.descriptors {
                    switch text.range(of: desc, options: .caseInsensitive) {
                    case .some(let range):
                        symptomRange = .init(index: range.lowerBound, symptom: symptom)
                        break
                    default:
                        continue
                    }
                }
                
                return symptomRange
            }
            .sorted(by: \.index, <)
            .map(\.symptom)
    }
}

fileprivate extension SymptomDetectionService {
    struct SymptomRange {
        let index: String.Index
        let symptom: Symptom
    }
}
