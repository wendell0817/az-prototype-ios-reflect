//
//  FileService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/2/21.
//

import Foundation

struct FileService {
    private var docsUrl: URL { FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] }
    private var manager: FileManager { FileManager.default }
    
    private var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }()
    
    private var encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        return encoder
    }()
    
    func read<File: Decodable>(from fileName: String) throws -> File {
        let fileUrl = docsUrl.appendingPathComponent(fileName)
        let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
        let file = try decoder.decode(File.self, from: data)
        Log.debug("[FileService] read \(type(of: file)) from \(fileUrl.absoluteString)")
        return file
    }
    
    func write<File: Encodable>(_ file: File, to fileName: String) throws {
        let fileUrl = docsUrl.appendingPathComponent(fileName)
        let data = try encoder.encode(file)
        try data.write(to: fileUrl, options: .atomicWrite)
        Log.debug("[FileService] wrote \(type(of: file)) to \(fileUrl.absoluteString)")
    }
    
    func createDirectory(in parent: FileManager.SearchPathDirectory, dirName: String) throws -> URL {
        guard let parentDirURL = manager.urls(for: parent, in: .userDomainMask).first
        else { throw CommonError.message("[FileService] could not find parent directory for \(parent)") }
        
        let fullPath = parentDirURL.appendingPathComponent(dirName)
        try manager.createDirectory(atPath: fullPath.path, withIntermediateDirectories: true, attributes: .none)
        return fullPath
    }
    
    func move(_ sourceFileURL: URL, to destFileURL: URL) throws {
        try? manager.removeItem(atPath: destFileURL.path)
        try manager.moveItem(atPath: sourceFileURL.path, toPath: destFileURL.path)
    }
    
    func delete(_ fileName: String) throws {
        let fileUrl = docsUrl.appendingPathComponent(fileName)
        try manager.removeItem(atPath: fileUrl.path)
        Log.debug("[FileService] deleted at \(fileUrl.absoluteString)")
    }
}
