//
//  NetworkService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/5/21.
//

import Foundation

protocol NetworkServiceProvider {
    func get<Model: Decodable>(_ query: NetworkService.GetQuery) throws -> Model
    func status(_ query: NetworkService.StatusQuery) throws -> Int
    func videoURL(for reflection: Reflection) -> URL?
}

struct NetworkService: NetworkServiceProvider {
    let provider: NetworkServiceProvider
    
    init() {
        #if targetEnvironment(simulator)
        self.provider = MockNetworkService()
        #else
        self.provider = MockNetworkService()
        #endif
    }
    
    func get<Model: Decodable>(_ query: NetworkService.GetQuery) throws -> Model {
        try provider.get(query)
    }
    
    func status(_ query: NetworkService.StatusQuery) throws -> Int {
        try provider.status(query)
    }
    
    func videoURL(for reflection: Reflection) -> URL? {
        provider.videoURL(for: reflection)
    }
}

extension NetworkService {
    enum Error: Swift.Error {
        case invalidResponseString(String)
        case invalidResponse(URLResponse)
    }
}

extension NetworkService {
    enum StatusQuery {
        case create(reflection: Reflection, userID: String)
        case delete(reflectionID: String, userID: String)
        case update(reflection: Reflection, userID: String)
        case invite(newMemberEmail: String, userID: String)
        case removeGroupMember(id: String, userID: String)
        
        var decoder: JSONDecoder {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            return decoder
        }
    }
    
    enum GetQuery {
        case login(email: String, password: String)
        case feed(userID: String)
        case upload(videoURL: URL, audioURL: URL, reflectionID: String, userID: String)
        case group(userID: String)
        
        var decoder: JSONDecoder {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            return decoder
        }
    }
}
