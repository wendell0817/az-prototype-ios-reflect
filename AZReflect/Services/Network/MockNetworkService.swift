//
//  MockNetworkService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 8/5/21.
//

import Foundation

final class MockNetworkService: NetworkServiceProvider {
    private var cacheData = CacheData()
    private let feedsFileName = "mock.network.feeds.json"
    private let groupsFileName = "mock.network.groups.json"
    private var docsURL: URL { FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] }
    private var feedsFileURL: URL { docsURL.appendingPathComponent(feedsFileName) }
    private var groupsFileURL: URL { docsURL.appendingPathComponent(groupsFileName) }
    
    private var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }()
    
    private var encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        return encoder
    }()
    
    init() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.cacheData = self.loadCacheData()
        }
    }
    
    func get<Model>(_ query: NetworkService.GetQuery) throws -> Model where Model: Decodable {
        let data = try data(for: query)
        return try query.decoder.decode(Model.self, from: data)
    }
    
    func status(_ query: NetworkService.StatusQuery) throws -> Int {
        switch query {
        case .create(let reflection, let userID):
            try add(reflection, for: userID)
            return 200
        case .delete(let reflectionID, let userID):
            try delete(reflectionID: reflectionID, for: userID)
            return 200
        case .update(let reflection, let userID):
            try add(reflection, for: userID)
            return 200
        case .invite(let newMemberEmail, let userID):
            try invite(memberEmail: newMemberEmail, userID: userID)
            return 200
        case .removeGroupMember(let memberID, let userID):
            try removeMember(memberID: memberID, userID: userID)
            return 200
        }
    }
    
    func videoURL(for reflection: Reflection) -> URL? {
        guard let videoFileName = reflection.videoFileName else { return .none }
        return docsURL
            .appendingPathComponent(reflection.id)
            .appendingPathComponent(videoFileName)
    }
}

extension MockNetworkService {
    struct CacheData {
        var knownUsers: [User] = []
        var feeds: [String: Feed] = [:]
        var groups: [String: ShareGroup] = [:]
    }
    
    private func loadCacheData() -> CacheData {
        let knownUsers = loadKnownUsers()
        let groups = loadGroups()
        return .init(knownUsers: knownUsers, groups: groups)
    }
    
    private func loadKnownUsers() -> [User] {
        guard let knownUsersURL = Bundle.main.url(forResource: "mock.network.users", withExtension: "json") else {
            return .init()
        }
        
        do {
            let knownUsersData = try Data(contentsOf: knownUsersURL)
            return try decoder.decode([User].self, from: knownUsersData)
        } catch let error {
            Log.error(error)
            return []
        }
    }
    
    private func loadGroups() -> [String: ShareGroup] {
        do {
            let data = try Data(contentsOf: groupsFileURL)
            let existingGroups = try decoder.decode([String: ShareGroup].self, from: data)
            let updatedEntries = existingGroups.map { entry -> (String, ShareGroup) in
                let group = ShareGroup(
                    members: entry.value.members + entry.value.outgoingInvites,
                    outgoingInvites: [],
                    incomingInvites: entry.value.incomingInvites
                )
                return (entry.key, group)
            }
            return Dictionary(updatedEntries, uniquingKeysWith: { $1 })
        } catch let error {
            Log.error(error)
            return [:]
        }
    }
}
    
extension MockNetworkService {
    private func data(for query: NetworkService.GetQuery) throws -> Data {
        switch query {
        case .login(let email, _):
            return try loginData(for: email)
        case .feed(let userID):
            return try feedData(for: userID)
        case .upload(let videoURL, let audioURL, let reflectionID, let userID):
            return try upload(videoURL: videoURL, audioURL: audioURL, reflectionID: reflectionID, userID: userID)
        case .group(let userID):
            return try groupData(for: userID)
        }
    }
    
    private func loginData(for email: String) throws -> Data {
        guard let knownUser = cacheData.knownUsers.first(where: { $0.email == email.lowercased() }) else {
            throw NetworkService.Error.invalidResponseString("User not found")
        }
        
        return try encoder.encode(knownUser)
    }
    
    private func feedData(for userID: String) throws -> Data {
        switch cacheData.feeds[userID] {
        case .some(let feed):
            return try encoder.encode(feed)
        default:
            let feeds: [String: Feed]
            
            do {
                let feedData = try Data(contentsOf: feedsFileURL)
                feeds = try decoder.decode([String: Feed].self, from: feedData)
                cacheData.feeds = feeds
                Log.verbose("[MockNetworkService] loaded cached feeds from \(feedsFileURL)")
            } catch {
                feeds = [:]
            }

            switch feeds[userID] {
            case .some(let existingFeed):
                return try encoder.encode(existingFeed)
            default:
                return try encoder.encode(Feed())
            }
        }
    }
    
    private func add(_ reflection: Reflection, for userID: String) throws {
        switch cacheData.feeds[userID] {
        case .some(let feed):
            cacheData.feeds[userID] = feed.adding(reflection)
        default:
            cacheData.feeds[userID] = Feed(reflections: [reflection.id: reflection])
        }
        
        let data = try encoder.encode(cacheData.feeds)
        try data.write(to: feedsFileURL)
    }
    
    private func delete(reflectionID: String, for userID: String) throws {
        switch cacheData.feeds[userID] {
        case .some(let feed):
            let fileService = FileService()
            try? fileService.delete(reflectionID)
            
            cacheData.feeds[userID] = feed.removing(reflectionID)
            let data = try encoder.encode(cacheData.feeds)
            try data.write(to: feedsFileURL)
        default:
            break
        }
    }
    
    private func upload(videoURL: URL, audioURL: URL, reflectionID: String, userID: String) throws -> Data {
        guard let feed = cacheData.feeds[userID],
              let reflection = feed.reflections[reflectionID]
        else {
            throw CommonError.message("[MockNetworkService] upload failed for reflection:\(reflectionID)")
        }
        
        let fileService = FileService()
        let destDirectoryURL = try fileService.createDirectory(in: .documentDirectory, dirName: reflectionID)
        let destVideoURL = destDirectoryURL.appendingPathComponent(videoURL.lastPathComponent)
        let destAudioURL = destDirectoryURL.appendingPathComponent(audioURL.lastPathComponent)
        try fileService.move(videoURL, to: destVideoURL)
        try fileService.move(audioURL, to: destAudioURL)
        
        let updatedReflection = reflection.with(
            videoFileName: destVideoURL.lastPathComponent,
            audioFileName: destAudioURL.lastPathComponent
        )
        
        cacheData.feeds[userID] = feed.adding(updatedReflection)
        let feedData = try encoder.encode(cacheData.feeds)
        try feedData.write(to: feedsFileURL)
        
        Log.debug("[MockNetworkService] moved assets for \(reflectionID) to \(destDirectoryURL.absoluteString)")
        return try encoder.encode(updatedReflection)
    }
    
    private func groupData(for userID: String) throws -> Data {
        switch cacheData.groups[userID] {
        case .some(let shareGroup):
            return try encoder.encode(shareGroup)
        default:
            throw CommonError.message("[MockNetworkService] no group found for user \(userID)")
        }
    }
    
    private func invite(memberEmail: String, userID: String) throws {
        let user = cacheData.knownUsers.first(where: { $0.email == memberEmail.lowercased() })
        let newMember: ShareGroup.Member
        switch user {
        case .some(let user):
            newMember = .init(id: user.id, name: user.name, email: user.email)
        default:
            newMember = .init(id: UUID().uuidString, name: memberEmail, email: memberEmail)
        }
        
        switch cacheData.groups[userID] {
        case .some(let shareGroup):
            guard !shareGroup.memberExists(newMember) else {
                throw CommonError.message("[MockNetworkService] member \(newMember.email) already exists")
            }
            
            let invitedMembers = shareGroup.outgoingInvites + [newMember]
            let updatedGroup = ShareGroup(
                members: shareGroup.members,
                outgoingInvites: invitedMembers,
                incomingInvites: shareGroup.incomingInvites
            )
            cacheData.groups[userID] = updatedGroup
        default:
            let group = ShareGroup(members: [], outgoingInvites: [newMember], incomingInvites: [])
            cacheData.groups[userID] = group
        }
        
        let groupData = try encoder.encode(cacheData.groups)
        try groupData.write(to: groupsFileURL)
    }
    
    private func removeMember(memberID: String, userID: String) throws {
        let group = cacheData.groups[userID]
        switch group {
        case .some(let group):
            let updatedGroup = ShareGroup(
                members: group.members.filter { $0.id != memberID },
                outgoingInvites: group.outgoingInvites.filter { $0.id != memberID },
                incomingInvites: group.incomingInvites
            )
            
            cacheData.groups[userID] = updatedGroup
            let groupData = try encoder.encode(cacheData.groups)
            try groupData.write(to: groupsFileURL)
        default:
            break
        }
    }
}

extension Feed {
    func adding(_ reflection: Reflection) -> Feed {
        var updatedReflection = reflections
        updatedReflection[reflection.id] = reflection
        
        return .init(
            reflections: updatedReflection
        )
    }
    
    func removing(_ reflectionID: String) -> Feed {
        var updatedReflection = reflections
        updatedReflection[reflectionID] = .none
        
        return .init(
            reflections: updatedReflection
        )
    }
}

extension ShareGroup {
    func memberExists(_ member: Member) -> Bool {
        [members, outgoingInvites]
            .flatMap { $0 }
            .contains(where: { $0.email.lowercased() == member.email.lowercased() })
    }
}
