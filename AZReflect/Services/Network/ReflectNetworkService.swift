//
//  ReflectNetworkService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 9/21/21.
//

import Foundation

struct ReflectNetworkService: NetworkServiceProvider {
    func get<Model>(_ query: NetworkService.GetQuery) throws -> Model where Model: Decodable {
        throw CommonError.message("Under Consruction!")
    }
    
    func status(_ query: NetworkService.StatusQuery) throws -> Int {
        throw CommonError.message("Under Consruction!")
    }
    
    func videoURL(for reflection: Reflection) -> URL? {
        .none
    }
}
