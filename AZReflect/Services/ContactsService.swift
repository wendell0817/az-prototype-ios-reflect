//
//  ContactsService.swift
//  AZReflect
//
//  Created by Thompson, Wendell (About Objects, Inc.) on 12/8/21.
//

import Foundation
import Contacts

struct ContactsService {
    private let cnContactStore = CNContactStore()
    
    var currentAuthStatus: CNAuthorizationStatus {
        CNContactStore.authorizationStatus(for: .contacts)
    }
    
    func requestAuthorization() async -> Bool {
        switch currentAuthStatus {
        case .authorized:
            return true
        case .notDetermined:
            return await withCheckedContinuation { cont in
                cnContactStore.requestAccess(
                    for: .contacts,
                    completionHandler: { isAccessGranted, error in
                        cont.resume(returning: isAccessGranted)
                    }
                )
            }
        default:
            return false
        }
    }
    
    func searchContacts(for searchString: String) async throws -> [CNContact] {
        guard await requestAuthorization() else { throw ContactsService.Error.accessDenied }
        
        let keysToFetch = [
            CNContactImageDataKey as CNKeyDescriptor,
            CNContactIdentifierKey as CNKeyDescriptor,
            CNContactGivenNameKey as CNKeyDescriptor,
            CNContactFamilyNameKey as CNKeyDescriptor,
            CNContactEmailAddressesKey as CNKeyDescriptor
        ]
        
        let request = CNContactFetchRequest(keysToFetch: keysToFetch)
        request.unifyResults = true
        request.mutableObjects = false
        request.sortOrder = .userDefault
        
        var contacts: [CNContact] = []
        
        try cnContactStore.enumerateContacts(
            with: request,
            usingBlock: { cnContact, _ in
                guard cnContact.matchingSearchString(searchString) else { return }
                contacts.append(cnContact)
            }
        )
        
        return contacts
    }
}

extension ContactsService {
    enum Error: Swift.Error {
        case none
        case accessDenied
    }
}

extension CNContact {
    func matchingSearchString(_ text: String) -> Bool {
        guard !emailAddresses.isEmpty else { return false }
        guard !text.isEmpty else { return true }
        let searchValues = [givenName, familyName] + emailAddresses.map(\.value).map(String.init)
        return searchValues.contains { value in
            return value.range(of: text, options: .caseInsensitive) != .none
        }
    }
}
